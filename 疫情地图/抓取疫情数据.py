# 药材名
#  //mat-cell[2]/button/span

# 药材名和药材英文名
# //mat-cell/button/span
# //mat-cell/button/span/text()


# 化合物
# //mat-table/mat-row/mat-cell[2]

# todo 先获取药材 化合物英文名


import os
import json
import requests
import collections


class Test(object):
    def __init__(self, url):
        # 构建列表页面
        self.url = url
        # 构建headers
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        }

    def get_data(self, url):
        response = requests.get(url, headers=self.headers)
        return response.content

    def parse_list_page(self, data):
        dict_data = json.loads(data)
        data_title = dict_data['data_title']
        yq_data = dict_data['data']
        return data_title, yq_data

    def second_parse_list_page(self, data):
        dict_data = json.loads(data)
        return dict_data['compound']

    def run(self):
        # 循环
        # 发起列表页面请求
        data = self.get_data(self.url)

        with open('my_self_test.html', 'wb')as f:
            f.write(data)
        # quit()

        data_title, yq_data = self.parse_list_page(data)

        print(data_title)
        print(yq_data)
        quit()
        f = open("./Herb.txt", 'w', encoding='utf-8')
        f1 = open("./compound.txt", 'w', encoding='utf-8')
        # Chinese name 	English name	Phonetic name	Latin name	Medicinal part	Category	Effect	Indication
        f.write("Chinese_name" + "\t" +
                "English_name" + "\t" +
                "Phonetic_name" + "\t" +
                "Latin_name" + "\t" +
                "Medicinal_part" + "\t" +
                "Category" + "\t" +
                "Effect" + "\t" +
                "Indication" + "\n")
        # English name
        # Chinese name
        # Formula
        # Smiles
        # CID
        # CAS
        # First Category
        # Second Category
        # Mol Weight
        # Alogp
        # PSA
        # HBA
        # HBD
        # RTB
        # DL
        # Related
        f1.write(
            "English_name" + "\t" +
            "Chinese_name" + "\t" +
            "Formula" + "\t" +
            "Smiles" + "\t" +
            "CID" + "\t" +
            "CAS" + "\t" +
            "First_Category" + "\t" +
            "Second_Category" + "\t" +
            "Mol_Weight" + "\t" +
            "Alogp" + "\t" +
            "PSA" + "\t" +
            "HBA" + "\t" +
            "HBD" + "\t" +
            "RTB" + "\t" +
            "DL" + "\t" +
            "Related_Herbs" + "\t" +
            "mol_file" + "\t" +
            "find_id" + "\n"
        )

        set_herb_id = collections.defaultdict(set)
        yaocai_huahewu_dict = collections.defaultdict(list)
        for dict_data_herb in dict_data_herbs:
            # first_catagory_chinese first_catagory_english
            Category = (dict_data_herb['first_catagory_chinese'] if dict_data_herb[
                'first_catagory_chinese'] else "null") + "(" + (
                           dict_data_herb['first_catagory_english'] if dict_data_herb[
                               'first_catagory_english'] else "null") + ")"
            f.write(
                (dict_data_herb['Chinese_name'] if dict_data_herb['Chinese_name'] else "null") + "\t" +
                (dict_data_herb['English_name'] if dict_data_herb['English_name'] else "null") + "\t" +
                (dict_data_herb['pinyin_name'] if dict_data_herb['pinyin_name'] else "null") + "\t" +
                (dict_data_herb['latin_name'] if dict_data_herb['latin_name'] else "null") + "\t" +
                (dict_data_herb['use_part'] if dict_data_herb['use_part'] else "null") + "\t" +
                Category + "\t" +
                (dict_data_herb['effect'] if dict_data_herb['effect'] else "null") + "\t" +
                (dict_data_herb['indication'] if dict_data_herb['indication'] else "null") + "\n"
            )
            if dict_data_herb['compounds']:
                for herb_id in dict_data_herb['compounds']:
                    set_herb_id["set_id"].add(herb_id["id"])
                    yaocai_huahewu_dict[dict_data_herb['Chinese_name']].append(str(herb_id["id"]))
                    # print(dict_data_herb['Chinese_name'])
            else:
                yaocai_huahewu_dict[dict_data_herb['Chinese_name']].append("null")

        f.close()

        for Compound_set_id in set_herb_id["set_id"]:
            url_id = "http://cadd.pharmacy.nankai.edu.cn/yatcm_api/compounds/{}/?include[]=herb_set.id&include[]=herb_set.Chinese_name&include[]=herb_set.English_name&exclude[]=herb_set.*&include[]=cid.*&include[]=cas.*&include[]=compoundfirstcatagory_set.*&include[]=compoundsecondcatagory_set&include[]=tcmid_herbs_set.*".format(
                Compound_set_id)
            data = self.get_data(url_id)

            compound_data = self.second_parse_list_page(data)

            cid = ""
            for i in compound_data['cid']:
                cid += "," + str(i['cid'])

            cas = ""
            for i in compound_data['cas']:
                cas += "," + str(i['cas'])

            First_Category = ""
            if compound_data['compoundfirstcatagory_set']:
                for i in compound_data['compoundfirstcatagory_set']:
                    First_Category += "," + str(i['Chinese_first_catagory']) + "(" + str(
                        i['English_first_catagory']) + ")"
            else:
                First_Category = "null"
            # Second_Category
            Second_Category = ""
            for i in compound_data['compoundsecondcatagory_set']:
                Second_Category += "," + i['Chinese_second_catagory'] + "(" + i['English_second_catagory'] + ")"
            # Related_Herbs
            Related_Herbs = ""
            for i in compound_data['herb_set']:
                Related_Herbs += "," + str(i['Chinese_name']) + "(" + str(i['English_name']) + ")"

            f1.write(
                compound_data['english_name'] + "\t" +
                (compound_data['chinese_name'] if compound_data['chinese_name'] else "null") + "\t" +
                (compound_data['formula'] if compound_data['formula'] else "null") + "\t" +
                (str(compound_data['smiles']) if compound_data['smiles'] else "null") + "\t" +
                (cid.strip(',') if cid else "null") + "\t" +
                (cas.strip(',') if cas else "null") + "\t" +
                (First_Category.strip(',') if First_Category else "null") + "\t" +
                (Second_Category.strip(',') if Second_Category else "null") + "\t" +
                (str(compound_data['mol_weight']) if compound_data['mol_weight'] else "null") + "\t" +
                (str(compound_data['alogp']) if compound_data['alogp'] else "null") + "\t" +
                (str(compound_data['psa']) if compound_data['psa'] else "null") + "\t" +
                (str(compound_data['hba']) if compound_data['hba'] else "null") + "\t" +
                (str(compound_data['hbd']) if compound_data['hbd'] else "null") + "\t" +
                (str(compound_data['rtb']) if compound_data['rtb'] else "null") + "\t" +
                (str(compound_data['DL']) if compound_data['DL'] else "null") + "\t" +
                (Related_Herbs.strip(',') if Related_Herbs else "null") + "\t" +
                (compound_data['mol_file'] if compound_data['mol_file'] else "null") + "\t" +
                str(Compound_set_id) + "\n"

            )

            print(compound_data['english_name'])
        f1.close()
        # print(yaocai_huahewu_dict)
        f2 = open("./yaocai_huahewu.txt", 'w', encoding='utf-8')
        for i in yaocai_huahewu_dict:
            f2.write(
                i + "\t" + ",".join(yaocai_huahewu_dict[i]) + "\n"
            )
        f2.close()


if __name__ == '__main__':
    test = Test('https://interface.sina.cn/news/wap/fymap2020_data.d.json')
    test.run()
