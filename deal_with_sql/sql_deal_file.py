import os
import sys
from pathlib import Path


def deal_file(file_path):
    path = Path(file_path)
    paths = [p for p in path.iterdir() if p.is_file()]

    new_files = [p for p in path.glob("*_New.txt")]
    if new_files:
        return False
    # 去除某文件的第一行并添加第一列为序号
    # tail -n +2 expr_table.xls | sed = | sed 'N;s/\n/\t/' | less
    # awk 'BEGIN{OFS=FS="\t"}FNR>1{print FNR-1,$0}' file1>newfile
    for path in paths:
        os.system(''' awk 'BEGIN{OFS=FS="\t"}FNR>1{print FNR-1,$0}' %s>%s_New.txt ''' % (path, path))


def create_sql(file_path):
    path = Path(file_path)
    # load data local infile "/home/zhaoqiming/221mers_sars_metadata.final.txt" into table cell_samples;
    new_files = [p for p in path.glob("*_New.txt")]
    # print(new_files)
    # 获取当前文件路径
    utils_root_dir = os.path.dirname(os.path.abspath(__file__))
    sql_name = os.sep.join([utils_root_dir, 'zqm.sql'])
    with open(sql_name, "w") as f:
        for file in new_files:
            sql_load = "load data local infile '{}' into table *** ;".format(file)
            f.write(sql_load + "\n\r")
            print(sql_load)


sys_path = sys.argv[1]
deal_file(sys_path)
create_sql(sys_path)
