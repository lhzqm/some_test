import pandas as pd

file_name = "sample_table.txt"
# 读文件
data = pd.read_csv(file_name, sep="\t", index_col=None, header=0)
# df.columns.values
li = list(data.columns.values)
li.insert(0, "id")
print(li)
row, col = data.shape
_id = range(1, row + 1)
data["id"] = _id

print()

# data = data[field]
