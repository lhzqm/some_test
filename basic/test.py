def multiply(*numbers):
    return numbers


print(multiply(1, 2, 3, 4))

items = [
    ("product1", 10),
    ("product2", 9),
    ("product3", 12),
]


def sort_item(item):
    return item[1]


# items.sort(key=sort_item)
items.sort(key=lambda item: item[1])

print(items)

prices = list(map(lambda item: item[1], items))

filterde = list(filter(lambda item: item[1] >= 10, items))
print(prices)
print(filterde)

a = [1, 2, 3, 4]
b = [1, 5]
print(set(a) | set(b))

char_frequency = {}

from pprint import pprint

sentence = "This is a common interview question"
for char in sentence:
    if char in char_frequency:
        char_frequency[char] += 1
    else:
        char_frequency[char] = 1

char_frequency_sorted = sorted(char_frequency.items(),
                               key=lambda kv: kv[1],
                               reverse=True)
print(char_frequency_sorted[0])

# print(char_frequency)
# pprint(char_frequency, width=1)

