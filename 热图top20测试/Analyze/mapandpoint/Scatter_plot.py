import re

import numpy as np
import pandas as pd
import plotly.express as px
from plotly.offline import plot


def scatter_plot(data_file_name, data_file):
    try:
        # data = pd.read_csv("./huoshan.xls", sep="\t", header=0, index_col=0)
        data = pd.read_csv("{}".format(data_file), sep="\t", header=0, index_col=0)
        data['padj'] = np.log10(data['padj']) * (-1)

        fig = px.scatter(data, x="log2FoldChange", y="padj", color="level")
        # fig.show()
        plot_div = plot(fig, output_type='div', include_plotlyjs='directory')

        html = """
                        <!DOCTYPE html>
                        <html lang="en">
                        <head>
                            <meta charset="UTF-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>Document</title>
                        </head>
                            <body>
                            {}
                            </body>
                        </html>
                        """.format(plot_div)
        with open('{}.html'.format(data_file), 'w', encoding='utf-8') as test:
            test.write(html)

        #  替换js链接
        plot_div = re.sub("plotly.min.js", "./static/js/plotly.min.js", plot_div, 1)

        with open('{}.txt'.format(data_file_name), 'w', encoding='utf-8') as f:
            f.write(plot_div)
    except Exception as e:
        with open('error.txt', 'w', encoding='utf-8') as f:
            f.write(str(e))
