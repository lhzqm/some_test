import pandas as pd
# 生成 meta.tsv
# from disease.ehbioUtility import DEgeneAnalysis
# from disease.ehbioUtility import DEenrichment
from ehbioUtility import DEgeneAnalysis, transferLongToWideSameSpecies, DEenrichment
# from mapandpoint.Scatter_plot import scatter_plot
# from mapandpoint.Scatter_plot import scatter_plot
from mapandpoint.heatmap_div import heatmap

"""
source_data = ['AMS.show.txt', 'HAPC.show.txt', 'HPH.show.txt']

data = pd.read_csv('./AMS.show.txt', sep="\t")
data[['Sample', 'Group']].to_csv('./AMS.meta.tsv', sep='\t', index=None)

data = pd.read_csv('./HAPC.show.txt', sep="\t")
data[['Sample', 'Group']].to_csv('./HAPC.meta.tsv', sep='\t', index=None)

data = pd.read_csv('./HPH.show.txt', sep="\t")
data[['Sample', 'Group']].to_csv('./HPH.meta.tsv', sep='\t', index=None)

# 生成 merger.txt
data = pd.read_csv('./AMS.meta.tsv', sep="\t")
sample = data['Sample'].tolist()

f1 = open('./AMS.merger.txt', 'w')
for i in data['Sample'].tolist():
    f2 = open("../readscount/{}.gene.readscount.txt".format(i), 'r')
    for j in f2:
        f1.write(j)

data = pd.read_csv('./HPH.meta.tsv', sep="\t")
sample = data['Sample'].tolist()

f1 = open('./HPH.merger.txt', 'w')
for i in data['Sample'].tolist():
    f2 = open("../readscount/{}.gene.readscount.txt".format(i), 'r')
    for j in f2:
        f1.write(j)

data = pd.read_csv('./HAPC.meta.tsv', sep="\t")
sample = data['Sample'].tolist()

f1 = open('./HAPC.merger.txt', 'w')
for i in data['Sample'].tolist():
    f2 = open("../readscount/{}.gene.readscount.txt".format(i), 'r')
    for j in f2:
        f1.write(j)

# 生成:expr_mat.tsv
transferLongToWideSameSpecies('./AMS.merger.txt', fill_value=0, remove_all_zero=True, all_integer=True,
                              outputfile='./AMS.expr_mat.tsv')
transferLongToWideSameSpecies('./HPH.merger.txt', fill_value=0, remove_all_zero=True, all_integer=True,
                              outputfile='./HPH.expr_mat.tsv')
transferLongToWideSameSpecies('./HAPC.merger.txt', fill_value=0, remove_all_zero=True, all_integer=True,
                              outputfile='./HAPC.expr_mat.tsv')


DEgeneAnalysis(expression_type="reads_count", outputprefix='./AMS', fdr=0.1, log2fc=1)
DEgeneAnalysis(expression_type="reads_count", outputprefix='./HAPC', fdr=0.1, log2fc=1)
DEgeneAnalysis(expression_type="reads_count", outputprefix='./HPH', fdr=0.1, log2fc=1)

"""

# pd.read_csv(inputfile, sep="\t", header=header, index_col=index_col, usecols=usecols)
# middle_data
# for data in source_data:
#     pass

# scatter_plot("AMS.Non_AMS._vs_.AMS.results.xls", './AMS.Non_AMS._vs_.AMS.results.xls')

# hot_map(data_file_name, data_file, col_anno_file='', row_anno_file='', width=800, height=800, symmetric=True,
#             row_cluster=False, close=False)

# AMS.expr_mat.tsv
# AMS.Non_AMS._vs_.AMS.de_gene_expr.xls
# AMS.normalized.rlog.xls
# AMS.normalized.xls

heatmap("AMS.Non_AMS._vs_.AMS.de_gene_expr.xls", "AMS.Non_AMS._vs_.AMS.de_gene_expr.xls",
        col_anno_file='./AMS.middle_data.txt', symmetric=False)
#
# heatmap("HAPC.Non_HAPC._vs_.HAPC.de_gene_expr.xls", "HAPC.Non_HAPC._vs_.HAPC.de_gene_expr.xls",
#         col_anno_file='./HAPC.middle_data.txt', symmetric=False)
#
# scatter_plot("AMS.Non_AMS._vs_.AMS.results.xls", './AMS.Non_AMS._vs_.AMS.results.xls')
# scatter_plot("HAPC.Non_HAPC._vs_.HAPC.results.xls", 'HAPC.Non_HAPC._vs_.HAPC.results.xls')
# scatter_plot("HPH.Non_HPH._vs_.HPH.results.xls", 'HPH.Non_HPH._vs_.HPH.results.xls')

# heatmap("HAPC.Non_HAPC._vs_.HAPC.de_gene_expr.xls", "AMS.Non_AMS._vs_.AMS.de_gene_expr.xls",
#         col_anno_file='./AMS.middle_data.txt', symmetric=False)
# ehbioUtility.DEenrichment(output_prefix, str(species), analysis_type)
# 1.Biological_process  1.Cellular_component  1.Molecular_function  1.Reactome


# analysis_type = ['Biological_process', 'Cellular_component', 'Molecular_function', 'Reactome']
#
# DEenrichment('./HPH', "1", analysis_type)

# # 前缀
# prefix = ""
# for analysis_one in analysis_type:
#     filename = prefix + "." + analysis_one + "." +
# # cut --complement -f 3,4,5,8 AMS.show.txt
