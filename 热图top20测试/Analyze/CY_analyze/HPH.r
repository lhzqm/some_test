
if (FALSE){
	biocManager::install(c("DESeq2","BiocParallel"))
}

suppressPackageStartupMessages(library(DESeq2))
suppressPackageStartupMessages(library("RColorBrewer"))
suppressPackageStartupMessages(library("gplots"))
suppressPackageStartupMessages(library("amap"))
suppressPackageStartupMessages(library("reshape2"))
suppressPackageStartupMessages(library("ggplot2"))
suppressPackageStartupMessages(library("BiocParallel"))
suppressPackageStartupMessages(library("ggrepel"))
suppressPackageStartupMessages(library("pheatmap"))
suppressPackageStartupMessages(library("RColorBrewer"))
suppressPackageStartupMessages(library("factoextra"))

register(MulticoreParam(10))

data <- read.table("./HPH.expr_mat.tsv", header=T, row.names=1, com='', quote='',
	check.names=F, sep="\t")
data_sampleL = colnames(data)


sample <- read.table("./HPH.meta.tsv", header=T, row.names=1, com='',
	quote='', check.names=F, sep="\t", colClasses="factor")

sampleL = rownames(sample)

inter_sample = intersect(data_sampleL, sampleL)

sample <- sample[match(inter_sample, sampleL),, drop=F]
data <- data[,match(inter_sample, data_sampleL),drop=F]

minimumReads = ncol(data)

if (minimumReads > 20){
	minimumReads = 20
}

data <- data[rowSums(data)>minimumReads,]


sample_rowname <- rownames(sample)

sample <- data.frame(lapply(sample, function(x) factor(x, levels=unique(x))))
rownames(sample) <- sample_rowname

#paste0(sample $cell, sample$time)

if ("pairwise" == "pairwise") {
	print("Perform pairwise comparasion using <design=~Group>")
	ddsFullCountTable <- DESeqDataSetFromMatrix(countData = data,
		colData = sample,  design= ~ Group)
} else if ("pairwise" == "timeseries") {
	#Even for timeseries, pairwise is needed
	print("Perform pairwise comparasion using <design=~Group>")
	ddsFullCountTable <- DESeqDataSetFromMatrix(countData = data,
		colData = sample,  design= ~Group)
}

#dds <- DESeq(ddsFullCountTable)
dds <- ddsFullCountTable

if ("" != "") {
	#print("Using given sizeFactor")
	sizeFactorGiven = read.table("", header=T, row.names=1, 
		sep="\t")
	sizeFactorGiven2 = as.vector(unlist(sizeFactorGiven))
	names(sizeFactorGiven2) = rownames(sizeFactorGiven)
	sizeFactorGiven = sizeFactorGiven2
	sizeFactors(dds) <- sizeFactorGiven
} else {
	#print("estimate sizeFactor")
	dds <- estimateSizeFactors(dds, quiet=T)
	sizeFactorGenerate = sizeFactors(dds)	
	write.table(sizeFactorGenerate, "./HPH.sizeFactor.xls", sep="\t", 
		quote=F, row.names=T, col.names=T)
}

dds <- estimateDispersions(dds, quiet=T)
dds <- nbinomWaldTest(dds, quiet=T)

# Get normalized counts

#print("Output normalized counts")
normalized_counts <- counts(dds, normalized=TRUE)

normalized_counts_mad <- apply(normalized_counts, 1, mad)
normalized_counts <- normalized_counts[order(normalized_counts_mad, decreasing=T), ]

normalized_counts_output <- data.frame(ID=rownames(normalized_counts), round(normalized_counts,2))
write.table(normalized_counts_output, file="./HPH.normalized.xls",
quote=F, sep="\t", row.names=F, col.names=T)
#system(paste("sed -i '1 s/^/ID\t/'", "./HPH.normalized.xls"))

#rld <- rlog(dds, blind=FALSE)
#rlogMat <- assay(rld)
#rlogMat <- rlogMat[order(normalized_counts_mad, decreasing=T), ]
vsd <- varianceStabilizingTransformation(dds, blind=FALSE)
vstMat <- assay(vsd)
vstMat <- vstMat[order(normalized_counts_mad, decreasing=T), ]

#print("Output vst transformed normalized counts")
vstMat_output <- data.frame(ID=rownames(vstMat), round(vstMat,2))
write.table(vstMat_output, file="./HPH.normalized.rlog.xls",
quote=F, sep="\t", row.names=F, col.names=T)
#system(paste("sed -i '1 s/^/ID\t/'", "./HPH.normalized.rlog.xls"))

#print("Output vst transformed normalized ocunts")
#write.table(vstMat, file="./HPH.normalized.vst.xls",
#quote=F, sep="\t", row.names=T, col.names=T)
#system(paste("sed -i '1 s/^/ID\t/'", "./HPH.normalized.vst.xls"))


#formulaV <- c("Group")
#if ("pairwise" == "pairwise" & length(formulaV)>1) {
#	rlogMat_rmBatch <- limma::removeBatchEffect(rlogMat, sample[[formulaV[1]]])
#}

# Begin DE-gene compare

de_twosample <- function
(
dds, 
sampleV
){
	#print(sampleV)
	sampleA <- as.vector(sampleV$sampA)
	sampleB <- as.vector(sampleV$sampB)
	#print(paste("DE genes between", sampleA, sampleB, sep=" "))
	contrastV <- c("Group", sampleA, sampleB)
	res <- results(dds,  contrast=contrastV)
	baseA <- counts(dds, normalized=TRUE)[, colData(dds)$Group == sampleA]
	if (is.vector(baseA)){
		baseMeanA <- as.data.frame(baseA)
	} else {
		baseMeanA <- as.data.frame(rowMeans(baseA))
	}
	baseMeanA <- round(baseMeanA, 2)
	colnames(baseMeanA) <- sampleA
	baseB <- counts(dds, normalized=TRUE)[, colData(dds)$Group == sampleB]
	if (is.vector(baseB)){
		baseMeanB <- as.data.frame(baseB)
	} else {
		baseMeanB <- as.data.frame(rowMeans(baseB))
	}
	baseMeanB <- round(baseMeanB, 2)
	colnames(baseMeanB) <- sampleB
	res <- cbind(baseMeanA, baseMeanB, as.data.frame(res))
	res <- data.frame(ID=rownames(res), res)
	res$baseMean <- round(rowMeans(cbind(baseA, baseB)),2)
	res$padj[is.na(res$padj)] <- 1
	res$pvalue[is.na(res$pvalue)] <- 1
	res$log2FoldChange <- round(res$log2FoldChange,2)
	res$padj <- as.numeric(formatC(res$padj))
	res$pvalue <- as.numeric(formatC(res$pvalue))

	res <- res[order(res$padj),]

	comp314 <- paste(sampleA, "_vs_", sampleB, sep=".")

	file_base <- paste("./HPH", comp314, sep=".")
	file_base1 <- paste(file_base, "results.xls", sep=".")
	#write.table(as.data.frame(res), file=file_base1, sep="\t", quote=F, row.names=F)
	res_output <- as.data.frame(subset(res,select=c('ID',sampleA,sampleB,"baseMean",'log2FoldChange','pvalue', 'padj')))
	
	res_de <- subset(res, res$padj<0.1, select=c('ID', sampleA,
		sampleB, 'log2FoldChange', 'padj'))
	res_de_up <- subset(res_de, res_de$log2FoldChange>=1)
	file <- paste("./HPH",sampleA, "_higherThan_", sampleB, 'xls', sep=".") 
	write.table(as.data.frame(res_de_up), file=file, sep="\t", quote=F, row.names=F)
	res_de_up_id <- subset(res_de_up, select=c("ID"))
	#file <- paste(file_base, "DE_up_id", sep=".")
	file <- paste("./HPH",sampleA, "_higherThan_", sampleB,'id.xls', sep=".") 
	write.table(as.data.frame(res_de_up_id), file=file, sep="\t", 
		quote=F, row.names=F, col.names=F)
	
	if(dim(res_de_up_id)[1]>0) {
		res_de_up_id_l <- cbind(res_de_up_id, paste(sampleA, "_higherThan_",sampleB, sep="."))
		write.table(as.data.frame(res_de_up_id_l), file="./HPH.all.DE",
		sep="\t",quote=F, row.names=F, col.names=F, append=T)
	}
		
	res_de_dw <- subset(res_de, res_de$log2FoldChange<=(-1)*1)
	#file <- paste(file_base, "DE_dw", sep=".")
	file <- paste("./HPH",sampleA, "_lowerThan_", sampleB, 'xls', sep=".") 
	#file <- paste("./HPH",sampleB, "_higherThan_", sampleA, 'xls', sep=".") 
	write.table(as.data.frame(res_de_dw), file=file, sep="\t", quote=F, row.names=F)
	res_de_dw_id <- subset(res_de_dw, select=c("ID"))
	#file <- paste(file_base, "DE_dw_id", sep=".")
	#file <- paste("./HPH",sampleB, "_higherThan_", sampleA, 'id.xls', sep=".") 
	file <- paste("./HPH",sampleA, "_lowerThan_", sampleB, 'id.xls', sep=".") 
	write.table(as.data.frame(res_de_dw_id), file=file, sep="\t", 
		quote=F, row.names=F, col.names=F)

	if(dim(res_de_dw_id)[1]>0) {
		#res_de_dw_id_l <- cbind(res_de_dw_id, paste(sampleB, "_higherThan_",sampleA, sep="."))
		res_de_dw_id_l <- cbind(res_de_dw_id, paste(sampleA, "_lowerThan_",sampleB, sep="."))
		write.table(as.data.frame(res_de_dw_id_l), file="./HPH.all.DE",
		sep="\t",quote=F, row.names=F, col.names=F, append=T)
	}

	baseExpr <- cbind(baseA, baseB, baseMeanA, baseMeanB)
	if(length(rownames(res_de_up_id))>=1){
		baseExprUp <- baseExpr[match(rownames(res_de_up_id),rownames(baseExpr)),]
		baseExprUp <- baseExprUp[order(baseExprUp[sampleA],decreasing=T),]
	}else{
		baseExprUp <- NULL
	}
	if(length(rownames(res_de_dw_id))>=1){
		baseExprDw <- baseExpr[match(rownames(res_de_dw_id),rownames(baseExpr)),]
		baseExprDw <- baseExprDw[order(baseExprDw[sampleB]),]
	} else {
		baseExprDw <- NULL
	}
	
	if(!is.null(baseExprUp) || !is.null(baseExprDw)){
		baseExpr_de <- rbind(baseExprUp,baseExprDw)
		ncol = ncol(baseExpr_de)
		baseExpr_de <- baseExpr_de[,-c(ncol-1,ncol)]
		colnames_baseExpr_de <- colnames(baseExpr_de)
		rownames_baseExpr_de <- rownames(baseExpr_de)
		baseExpr_de <- t(apply(baseExpr_de,1,scale))
		rownames(baseExpr_de) <- rownames_baseExpr_de
		colnames(baseExpr_de) <- colnames_baseExpr_de
		baseExpr_de <- round(baseExpr_de,3)
		baseExpr_de <- data.frame(id=rownames(baseExpr_de), baseExpr_de)

		file497 <- paste("./HPH",sampleA, "_vs_", sampleB,'de_gene_expr.xls', sep=".") 
		write.table(as.data.frame(baseExpr_de), file=file497, sep="\t", 
			quote=F, row.names=F, col.names=T)
	}

	#logCounts <- log2(res$baseMean+1)
	#logFC <- res$log2FoldChange
	#FDR <- res$padj
	#svg(filename=paste(file_base, "MA.svg", sep="."))
	#plot(logCounts, logFC, col=ifelse(FDR<=0.01, "red", "black"),
	#xlab="logCounts", ylab="logFC", main="MA plot", pch='.')
	#dev.off()
	#png(filename=paste(file_base, "Volcano.png", sep="."))
	#plot(logFC, -1*log10(FDR), col=ifelse(FDR<=0.01, "red", "black"),
	#xlab="logFC", ylab="-1*log1o(FDR)", main="Volcano plot", pch=".")
	#dev.off()

	res_output$level <- ifelse(res_output$padj<=0.1, ifelse(res_output$log2FoldChange>=1, paste(sampleA,"UP"), ifelse(res_output$log2FoldChange<=1*(-1), paste(sampleB,"UP"), "NoDiff")) , "NoDiff")
	write.table(res_output, file=file_base1, sep="\t", quote=F, row.names=F)
	res_output$padj <- (-1) * log10(res_output$padj)
	res_output$padj <- replace(res_output$padj, res_output$padj>5, 5.005)
	
	boundary = ceiling(max(abs(res_output$log2FoldChange)))
	p = ggplot(res_output, aes(x=log2FoldChange,y=padj,color=level)) +
         #geom_point(aes(size=baseMean), alpha=0.5) + theme_classic() +
         geom_point(size=1, alpha=0.5) + theme_classic() +
	 	 xlab("Log2 transformed fold change") + ylab("Negative Log10 transformed FDR") +
		 xlim(-1 * boundary, boundary) + theme(legend.position="top", legend.title=element_blank()) +
		 geom_hline(yintercept=c((-1) * log10(0.1)), linetype="dashed", color="grey") +
		 geom_vline(xintercept=c((-1)*1, 1), linetype="dashed", color="grey")
	ggsave(p, filename=paste0(file_base1,".volcano.pdf"),width=13.5,height=15,units=c("cm"))
	ggsave(p, filename=paste0(file_base1,".volcano.png"),width=13.5,height=15,units=c("cm"))
	#system(paste0("sp_volcano.sh -x log2FoldChange -y padj -P TRUE -M 4 -L top -F '0.1,1' -f ", file_base1))
	#system(paste0("sp_volcano.sh -x log2FoldChange -y padj -P TRUE -M 4 -E png -L top -F '0.1,1' -f ", file_base1))
}



de_timeseries <- function
(
dds, 
sampleV, 
first_base, 
second_base
){
	#print(paste("Time series DE genes", sampleV, sep=" "))
	#----Extracting cell and time information----------------------
	baseV <- c(first_base, second_base)
	compV <- unlist(strsplit(		sub(".Group", "___",  sub("Group", '', sampleV)), "___"))
	
	first_inter <- c(baseV[1], compV[1])
	second_inter <- c(baseV[2], compV[2])

	conditionL <- c(paste0(first_inter[1], second_inter),
		paste0(first_inter[2], second_inter))

	base1 <- counts(dds, normalized=TRUE)[, colData(dds)$condition ==
		conditionL[1]]
	
	if (is.vector(base1)){
		baseMean1 <- as.data.frame(base1)
	} else {
		baseMean1 <- as.data.frame(rowMeans(base1))
	}

	colnames(baseMean1) <- conditionL[1]

	base2 <- counts(dds, normalized=TRUE)[, colData(dds)$condition ==
		conditionL[2]]

	if (is.vector(base2)){
		baseMean2 <- as.data.frame(base2)
	} else {
		baseMean2 <- as.data.frame(rowMeans(base2))
	}

	colnames(baseMean2) <- conditionL[2]

	base3 <- counts(dds, normalized=TRUE)[, colData(dds)$condition ==
		conditionL[3]]

	if (is.vector(base3)){
		baseMean3 <- as.data.frame(base3)
	} else {
		baseMean3 <- as.data.frame(rowMeans(base3))
	}

	colnames(baseMean3) <- conditionL[3]

	base4 <- counts(dds, normalized=TRUE)[, colData(dds)$condition ==
		conditionL[4]]

	if (is.vector(base4)){
		baseMean4 <- as.data.frame(base4)
	} else {
		baseMean4 <- as.data.frame(rowMeans(base4))
	}
	
	colnames(baseMean4) <- conditionL[4]

	#----Extracting cell and time information----------------------
	contrastV <- list(as.vector(sampleV))
	res <- results(dds,  contrast=contrastV, test="Wald")
	res$padj[is.na(res$padj)] <- 1
	res <- cbind(ID=rownames(res), baseMean1, baseMean2, baseMean3,
		baseMean4, as.data.frame(res))
	res <- subset(res, select=c('ID', conditionL, 'log2FoldChange', 'padj'))
	res <- res[order(res$padj),]
	
	comp314 <- paste0(sampleV, collapse="__")

	file_base <- paste("./HPH", paste0(sampleV, collapse="__"),"results", sep=".")
	write.table(as.data.frame(res), file=file_base, sep="\t", quote=F, row.names=F)
	
	res_de <- subset(res, res$padj<0.1,
		select=c('ID',conditionL, 'log2FoldChange','padj'))
	res_de_up <- subset(res_de, res_de$log2FoldChange>=1)
	file <- paste(file_base, "DE_up", sep=".")
	write.table(as.data.frame(res_de_up), file=file, sep="\t", quote=F, row.names=F)
	res_de_up_id <- subset(res_de_up, select=c("ID"))
	file <- paste(file_base, "DE_up_id", sep=".")
	write.table(as.data.frame(res_de_up_id), file=file, sep="\t", 
		quote=F, row.names=F, col.names=F)
	
	if(dim(res_de_up_id)[1]>0) {
		res_de_up_id_l <- cbind(res_de_up_id, paste(comp314, "up",sep="_"))
		write.table(as.data.frame(res_de_up_id_l), file="./HPH.all.DE",
		sep="\t",quote=F, row.names=F, col.names=F, append=T)
	}

	res_de_dw <- subset(res_de, res_de$log2FoldChange<=(-1)*1)
	file <- paste(file_base, "DE_dw", sep=".")
	write.table(as.data.frame(res_de_dw), file=file, sep="\t", quote=F, row.names=F)
	res_de_dw_id <- subset(res_de_dw, select=c("ID"))
	file <- paste(file_base, "DE_dw_id", sep=".")
	write.table(as.data.frame(res_de_dw_id), file=file, sep="\t", 
		quote=F, row.names=F, col.names=F)

	if(dim(res_de_dw_id)[1]>0) {
		res_de_dw_id_l <- cbind(res_de_dw_id, paste(comp314, "dw",sep="_"))
		write.table(as.data.frame(res_de_dw_id_l), file="./HPH.all.DE",
		sep="\t",quote=F, row.names=F, col.names=F, append=T)
	}

	logFC <- res$log2FoldChange
	FDR <- res$padj
	logFDR <- -1*log10(FDR)

	#png(filename=paste(file_base, "Volcano.png", sep="."))
	#plot(logFC, -1*log10(FDR), col=ifelse(FDR<=0.01, "red", "black"),
	#xlab="logFC", ylab="-1*log1o(FDR)", main="Volcano plot", pch=".")
	#dev.off()
}

if ("pairwise" == "pairwise" || "pairwise" == "timeseries") {
	if ("./HPH.compare_pair" == "FALSE") {
		compare_data <- as.vector(unique(sample$Group))
		#compare_combine <- as.matrix(combn(compare_data, 2))
		#for(i in compare_combine) {
		#	de_twosample(dds, i)
		#}
		len_compare_data <- length(compare_data)
		for(i in 1:(len_compare_data-1)) {
			for(j in (i+1):len_compare_data) {
				tmp_compare <- as.data.frame(
					cbind(sampA=compare_data[i],
					sampB=compare_data[j]))
				de_twosample(dds, tmp_compare)
			}
		}
	}else {
		compare_data <- read.table("./HPH.compare_pair", sep="\t",
		check.names=F, quote='', com='')
		colnames(compare_data) <- c("sampA", "sampB")
		unused <- by(compare_data, 1:nrow(compare_data), function (x)
		de_twosample(dds, x))
	}	
}
	
if ("pairwise" == "timeseries") {
	# Check the following links for time-serise reference
	# http://www.bioconductor.org/help/workflows/rnaseqGene/#count
	# https://support.bioconductor.org/p/65676/#66860
	# https://support.bioconductor.org/p/62357/#62368
	#print("Performing timeseries analysis using <design=~Group")
	ddsFullCountTable <- DESeqDataSetFromMatrix(countData = data,
		colData = sample,  design= ~ Group)

	# The following chunk performs a likelihood ratio test,  where we
	# remove the strain-specific differences over time. Genes with
	# small p values from this test are those which,  at one or more
	# time points after time 0 showed a strain-specific effect. Note
	# therefore that this will not give small p values to genes which
	# moved up or down over time in the same way in both strains.

	dds <- DESeq(ddsFullCountTable, test="LRT", reduced=~nouse)
	compareP <- resultsNames(dds)
	
	strain_specific <- compareP[grepl("\\.", compareP)]
	
	first_v_level <- levels(sample$Group)
	second_v_level <- levels(sample$Group)
	
	first_base <- first_v_level[1]
	second_base <- second_v_level[1]

	lapply(strain_specific, function(x) de_timeseries(dds, x,
	first_base, second_base))
	

	# results(dds, name=strainmut.minute60):
	# 	it is an interaction term because it contains the names of
	# 	both variables strain and minute. 
	#	So this term is a test for if the mut vs WT fold change is
	# 	different at minute 60 than at minute 0. 

	# results(dds, name="minute_60_vs_0")
	#	To generate the tables of log fold change of 60 minutes vs 0
	#   minutes for the WT strain would be:
	
	# results(dds, contrast=list(c("minute_60_vs_0", "strainmut.minute60")))
	#	To generate the tables of log fold change of 60 minutes vs 0
	#	minutes for the mut strain would be the sum of the WT term
	#	above and the interaction term which is an additional effect
	#	beyond the effect for the reference level (WT)

	rld <- rlog(dds)
}


#print("PCA analysis")
formulaV <- c("Group")
#pca_data <- plotPCA(rld, intgroup=formulaV, returnData=T, ntop=5000)
#percentVar <- round(100 * attr(pca_data, "percentVar"))
#pdf("./HPH.normalized.rlog.pca.pdf", pointsize=10)
#if (length(formulaV)==1) {
#  p <- ggplot(pca_data, aes(PC1, PC2, color=Group))
#} else if (length(formulaV==2)) {
#  p <- ggplot(pca_data, aes(PC1, PC2, color=Group,
#  shape=Group))
#}
#p + geom_point(size=3) + 
#	xlab(paste0("PC1: ", percentVar[1], "% variance")) +
#	ylab(paste0("PC2: ", percentVar[2], "% variance"))
##plotPCA(rld, intgroup=c("Group"))
#dev.off()
#
#png("./HPH.normalized.rlog.pca.png", pointsize=10)
#p + geom_point(size=3) + 
#	xlab(paste0("PC1: ", percentVar[1], "% variance")) +
#	ylab(paste0("PC2: ", percentVar[2], "% variance"))
##plotPCA(rld, intgroup=c("Group"))
#dev.off()

#print("Performing sample clustering")
hmcol <- colorRampPalette(brewer.pal(9, "GnBu"))(100)


topn = 5000
vstMat_nrow = nrow(vstMat)
if (topn > vstMat_nrow){
  topn = vstMat_nrow
}

pca_mat = vstMat[1:topn,]

pearson_cor <- round(as.matrix(cor(pca_mat, method="pearson")),3)


hc <- hcluster(t(pca_mat), method="pearson")

# Get lower triangle of the correlation matrix
get_lower_tri<-function(cormat){
	cormat[upper.tri(cormat)] <- NA
	return(cormat)
}
# Get upper triangle of the correlation matrix
get_upper_tri <- function(cormat){
	cormat[lower.tri(cormat)]<- NA
	return(cormat)
}

pearson_cor <- pearson_cor[hc$order, hc$order]

pearson_cor_output = data.frame(id=rownames(pearson_cor), pearson_cor)
write.table(pearson_cor_output, file="./HPH.pearson_cor.xls",
quote=F, sep="\t", row.names=F, col.names=T)

upper_tri <- get_upper_tri(pearson_cor)
# Melt the correlation matrix
melted_cormat <- melt(upper_tri, na.rm = TRUE)

col = colorRampPalette(rev(brewer.pal(n=7, name="RdYlBu")))(100)

# Create a ggheatmap
p <- ggplot(melted_cormat, aes(Var2, Var1, fill = value))+
 geom_tile(color = "white")+
 scale_fill_gradientn(colours=col, name="Pearson correlation") + theme_classic() +
  coord_fixed() + 
 theme(
  axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1),
  axis.title.x = element_blank(),
  axis.title.y = element_blank(),
  legend.justification = c(1, 0),
  legend.position = "top",
  legend.direction = "horizontal")+
  guides(fill = guide_colorbar(barwidth = 8, barheight = 1,
                title.position = "left"))

ggsave(p, filename="./HPH.normalized.rlog.pearson.pdf",width=18,height=20,units=c("cm"))
ggsave(p, filename="./HPH.normalized.rlog.pearson.png",width=18,height=20,units=c("cm"))

#pdf("./HPH.normalized.rlog.pearson.pdf", pointsize=10)
#heatmap.2(pearson_cor, Rowv=as.dendrogram(hc), symm=T, trace="none",
#col=hmcol, margins=c(5,5), main="The pearson correlation of each
#sample")
#dev.off()



#png("./HPH.normalized.rlog.pearson.png", units="cm", res=1000,width=20,height=18)
png("./HPH.normalized.rlog.pearson.png",width=620,height=600)
#heatmap.2(pearson_cor, Rowv=as.dendrogram(hc), symm=T, trace="none",
#col=hmcol, margins=c(1,1), main="Sample cluster", key.title="Pearson",key.xlab=NA,key.ylab=NA)
#pheatmap(pearson_cor, cluster_rows=hc, cluster_cols=hc, show_rownames=T, 
#        show_colnames=T, annotation_row=sample, annotation_col=sample)
#dev.off()


pca_mat <- pca_mat[apply(pca_mat,1,var)!=0,]
pca_mat <- as.data.frame(t(pca_mat))

pca <- prcomp(pca_mat, scale=T)

pca_x = pca$x
#
pca_individual = data.frame(samp=rownames(pca_x), pca_x, sample)
#
write.table(pca_individual, file="./HPH.pca_individuals.xls", sep="\t", quote=F, row.names=F, col.names=T)
#
pca_percentvar <- formatC(pca$sdev^2 * 100 / sum( pca$sdev^2))
#
##pdf("./HPH.normalized.rlog.pca.pdf", pointsize=10)
#if (length(formulaV)==1) {
#  p <- ggplot(pca_individual, aes(PC1, PC2, color=Group))
#} else if (length(formulaV==2)) {
#  p <- ggplot(pca_data, aes(PC1, PC2, color=Group,
#  shape=Group))
#}
#p = p + geom_point(size=3) + 
#	xlab(paste0("PC1: ", pca_percentvar[1], "% variance")) +
#	ylab(paste0("PC2: ", pca_percentvar[2], "% variance")) +
#	geom_text_repel(aes(label=samp), show.legend=F) +
#	theme_classic() +
#	theme(legend.position="top", legend.title=element_blank())

# https://mp.weixin.qq.com/s/4R14xJkQVPtaufaoXOcPIw
p <- fviz_pca_biplot(pca,
            fill.ind=sample$Group,
			#col.ind=sample$Group,
			palette="joo",
			addEllipses = T, 
			ellipse.type="confidence",
			ellipse.level=0.95,
			mean.point=F,
			col.var="contrib",
			gradient.cols = "RdYlBu",
			select.var = list(contrib = 10),
			repel=T, 
			ggtheme = theme_minimal()) + coord_fixed(1)

ggsave(p, filename="./HPH.normalized.rlog.pca.pdf",width=26,height=25,units=c("cm"))
ggsave(p, filename="./HPH.normalized.rlog.pca.png",width=26,height=25,units=c("cm"))

pca_percentvar <- data.frame(PC=colnames(pca_x), Variance=pca_percentvar)
write.table(pca_percentvar, file="./HPH.pca_pc_weights.xls", sep="\t", quote=F, row.names=F, col.names=T)

