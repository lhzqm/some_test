Sample	Treatment	Assay_type	Cell_type	Description	Hemoglobin(g/L)	Group	File
HAExp1Y_DT31	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	181	Non_HAPC	HAExp1Y_DT31.gene.readscount.txt
HAExp1Y_DK22	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	184	Non_HAPC	HAExp1Y_DK22.gene.readscount.txt
HAExp1Y_DT28	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	203	Non_HAPC	HAExp1Y_DT28.gene.readscount.txt
HAExp1Y_DK18	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	204	Non_HAPC	HAExp1Y_DK18.gene.readscount.txt
HAExp1Y_DT13	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	205	Non_HAPC	HAExp1Y_DT13.gene.readscount.txt
HAExp1Y_DT26	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	205	Non_HAPC	HAExp1Y_DT26.gene.readscount.txt
HAExp1Y_DK10	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	206	Non_HAPC	HAExp1Y_DK10.gene.readscount.txt
HAExp1Y_DT2	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	212	HAPC	HAExp1Y_DT2.gene.readscount.txt
HAExp1Y_DK24	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	214	HAPC	HAExp1Y_DK24.gene.readscount.txt
HAExp1Y_DK9	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	215	HAPC	HAExp1Y_DK9.gene.readscount.txt
HAExp1Y_DT7	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	216	HAPC	HAExp1Y_DT7.gene.readscount.txt
HAExp1Y_DT4	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	217	HAPC	HAExp1Y_DT4.gene.readscount.txt
HAExp1Y_DT1	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	223	HAPC	HAExp1Y_DT1.gene.readscount.txt
HAExp1Y_DT18	Exposed to high altitude 1 year	RNA-seq	Peripheral blood cell	Blood was drawn from a peripheral vein when people exposed to high altitude 1 year.	228	HAPC	HAExp1Y_DT18.gene.readscount.txt
