import pandas as pd
import sys
import numpy as np
from sklearn.decomposition import PCA
from scipy import stats
from itertools import combinations
import os
from statsmodels.stats.multitest import multipletests
from collections import defaultdict
from fisher import pvalue
import re

root_dir = os.path.dirname(os.path.realpath(__file__))
go_dir = root_dir + '/../../public/data/enrichment/'


# from sklearn.cluster import AgglomerativeClustering
# import importlib
# importlib.reload(ehbio_usage)    

# Ipython autoreload
# %load_ext autoreload
# %autoreload 1
# %aimport ehbioUtility
# 
# ehbioUtility._test_single_species()

# pd.options.display.float_format = '{:.3f}'.format

def _test_single_species():
    exprfile = "ehbio_trans.Count_matrix.3col"
    metadata = "ehbio_trans.Count_matrix.meta.tsv.input"
    outputprefix = "ehbio_trans.Count_matrix"
    expression_type = "reads_count"
    fill_value = 0
    remove_all_zero = True
    debug = False
    sampleClusterMatrixGenerate(exprfile, metadata, outputprefix="ehbio_trans.Count_matrix",
                                expression_type="reads_count")


def _test_double_species():
    exprfile = "readcount.twospecies"
    metadata = "sampleTable.twospecies"
    orthologfile = "ortholog.test"
    sampleClusterMatrixGenerate(exprfile, metadata, orthologfile, outputprefix="_test_double_species")


def sampleClusterMatrixGenerate(exprfile, metadata, orthologfile=None, outputprefix=None,
                                fill_value=0, remove_all_zero=True,
                                expression_type=False, debug=False):
    '''
    exprfile: 4 columns matrix file without header by default

                A1BG    9   HAExp3D_11  Human
                ADA 171 HAExp3D_11  Human
                CDH2    3   HAExp3D_11  Human
                AKT3    73  HAExp3D_11  Human
                MED6    74  HAExp3D_11  Human
    
    orthologfile: 2 column matrix file with first row as header line. 
                (Duplicate value in the second column will only be kept once.)
                (The program will transfer gene symbol in the second column to the first column.)

                Human   Mouse
                A1BG    A1BG
                AKT3    AKT3
    
    metadata: sample attribute table
        Species	Project_ID	Assay_type	Cell_type	Treatment	Sample
        Human	Altitude	RNA-seq	Peripheral blood cell	Pre-exposed to high altitude	PreExp_11
        Human	Altitude	RNA-seq	Peripheral blood cell	Pre-exposed to high altitude	PreExp_18
        Human	Altitude	RNA-seq	Peripheral blood cell	Pre-exposed to high altitude	PreExp_24

    outputprefix: Default None. Accept a filename. If None, the function will return a transferred matrix)

    fill_value: Default 0. Missed values will be replaced as 0.

    remove_all_zero: Default True. Remove a row for all zeros.

    all_integer: Default False. Set to True for reads count, then the numbers in matrix will all be integers.
    
    expression_type: Default TPM (accept reads_count).

    '''
    if debug:
        print("Read in ", metadata, file=sys.stderr)
    meta_mtx, multiple_species = checkMetadata(metadata)
    if expression_type == "TPM":
        all_integer = False
        normalized = True
    else:
        all_integer = True
        normalized = False
    if multiple_species:
        if debug:
            print("Multiple species transferLongToWideForTwoSpecies", file=sys.stderr)
        expr_mat = transferLongToWideForTwoSpecies(exprfile, orthologfile,
                                                   fill_value=fill_value, remove_all_zero=remove_all_zero,
                                                   all_integer=all_integer, debug=debug)
    else:
        if debug:
            print("Single specie transferLongToWideForTwoSpecies", file=sys.stderr)
        expr_mat = transferLongToWideSameSpecies(exprfile,
                                                 fill_value=fill_value, remove_all_zero=remove_all_zero,
                                                 all_integer=all_integer, debug=debug)
    # --------------------------------------------------------------------------------
    expr_mat.to_csv(outputprefix + '.expr_mat.tsv', sep="\t")
    # -------------------------------------------------
    if debug:
        print("matrixTransform", file=sys.stderr)
    expr_mat_log2, expr_mat_top = matrixTransform(expr_mat,
                                                  normalized=normalized, log_transfer=True, top=5000)
    expr_mat_log2 = expr_mat_log2.applymap(lambda x: "{:.2f}".format(x))
    expr_mat_log2.to_csv(outputprefix + '.expr_mat_log2_norm.tsv', sep="\t")
    expr_mat_top_output = expr_mat_top.applymap(lambda x: "{:.2f}".format(x))
    expr_mat_top_output.to_csv(outputprefix + '.expr_mat_log2_norm_top.tsv', sep="\t")
    if debug:
        print("correlationMatrix", file=sys.stderr)
    corr, outlierDF = correlationMatrix(expr_mat_top, cor_method="pearson", outputprefix=outputprefix)
    if debug:
        print("PCAmatrix", file=sys.stderr)
    PCAmatrix(expr_mat_top, n_components=2, outputprefix=outputprefix)
    meta_mtx.to_csv(outputprefix + '.meta.tsv', index=False, sep="\t")
    # print(outlierDF)
    meta_mtx.index = meta_mtx['Sample']
    meta_mtx = meta_mtx.join(outlierDF)
    # print(meta_mtx)
    meta_mtx['Suggest_remove'] = meta_mtx['Outlier'] & meta_mtx['Single_batch'] & meta_mtx['Single_group']
    meta_mtx.to_csv(outputprefix + '.meta_show.tsv', index=False, sep="\t")


# ---sampleClusterMatrixGenerate---------------------------------


def DEgeneAnalysis(expression_type, outputprefix, **kwargs):
    if expression_type == 'TPM':
        pass
    else:
        gene_expr_file = outputprefix + '.expr_mat.tsv'
        metadata = outputprefix + ".meta.tsv"
        status = DEgeneAnalysisReadscount(gene_expr_file=gene_expr_file,
                                          metadata=metadata, outputprefix=outputprefix,
                                          **kwargs)
        # if status == 'Success':
        #    randomSampleNoDiffGenes()


# ------------------------------------------------------------------------

def randomSampleNoDiffGenes(filename, samplecol_name="level", samplecol_value="NoDiff", sample_number=100):
    ''''''
    mtx = pd.read_csv(filename, sep="\t", header=0, index_col=None)
    mtx_skip_sample = mtx.loc[mtx[samplecol_name] != samplecol_value, :]
    mtx_sample = mtx.loc[mtx[samplecol_name] == samplecol_value, :].sample(sample_number)
    mtx_sample.to_csv(+'.random', index=False, sep="\t")
    return pd.concat(mtx_skip_sample, mtx_sample)


# -----------------------------------------------------------------------

def replace_nonillegel(x):
    if (isinstance(x, str)):
        return re.sub('[^0-9a-zA-Z_]+', '_', x)
    else:
        return x


def DEgeneAnalysisReadscount(gene_expr_file, metadata, outputprefix=None, compare_file=None, log2fc=1, fdr=0.1):
    # print(metadata)
    meta_mtx = pd.read_csv(metadata, sep="\t", header=0, index_col=None)
    summary = meta_mtx.describe(include="all")
    # Remove columns containing only one value
    meta_mtx = meta_mtx.loc[:, summary.loc["unique"] > 1]
    # replace non illegel characters 
    meta_mtx = meta_mtx.applymap(lambda x: replace_nonillegel(x))
    meta_mtx.to_csv(metadata, index=False, sep="\t")
    meta_mtx_columns = list(meta_mtx.columns)
    assert 'Group' in meta_mtx_columns
    design = 'Group'
    if 'Project_ID' in meta_mtx_columns:
        design = 'Project_ID+Group'
    # -------------------------------------------------------------
    if not compare_file:
        compare_file = outputprefix + '.compare_pair'
        compare_file_fh = open(compare_file, 'w')
        compareL = list(combinations(meta_mtx['Group'].unique(), 2))
        # print(compareL)
        print('\n'.join(['\t'.join(i) for i in compareL]), file=compare_file_fh)
        compare_file_fh.close()
    cmd = [root_dir + '/DESeq2.sh', '-f', gene_expr_file, '-s', metadata, '-d', '"' + design + '"',
           '-m pairwise', '-p', compare_file, '-F', str(log2fc), '-P', str(fdr),
           '-o', outputprefix, "2>", outputprefix + '.log']
    print(' '.join(cmd))
    os.system(' '.join(cmd))
    if os.stat(outputprefix + '.log').st_size != 0:
        log = open(outputprefix + '.log').readlines()
        # log is a list
        # ['错误: Discrete value supplied to continuous scale\n', '停止执行\n']
        return log
    return 'Success'


# ---DEgeneAnalysisReadscount----------------------------------------------


def normalizeReadsCount(expr_mat):
    '''
    Python version of DESeq2 normalize.
    '''
    expr_mat_tmp = expr_mat.loc[~(expr_mat == 0).any(axis=1)]
    expr_mat_tmp_log = np.log(expr_mat_tmp)
    expr_mat_tmp_log_row_mean = np.exp(expr_mat_tmp_log.mean(axis=1))
    expr_divide_row_mean = expr_mat_tmp.apply(lambda x: x / expr_mat_tmp_log_row_mean, axis=0)
    size_factor = expr_divide_row_mean.median()
    return expr_mat.apply(lambda x: x / size_factor, axis=1)


# ---normalizeReadsCount-------------------------------------

def matrixTransform(expr_mat, normalized=False, log_transfer=True, top=5000):
    '''
    Compute matrix correlation for normalized and unnormalzied ones.
    
    normalized: Default False (for reads count). The function will compute size
                factor and do normalize.
                If True, no normalization will be performed.
    log_transfer: Default True, log1p(x)/log(2) will be used to get log2 values.
                If False, no log transformed will be performed.

    Return:
        expr_mat - log2 transformed normalized matrix
        expr_mat_top - log2 transformed normalized matrix for top variable ones
    '''
    if not normalized:
        expr_mat = normalizeReadsCount(expr_mat)
    if log_transfer:
        expr_mat = np.log1p(expr_mat) / np.log(2)
    # ------------------------------------------

    stat = expr_mat.mad(axis=1).to_frame("mad")
    stat.sort_values(by="mad", axis=0, ascending=False, inplace=True)
    expr_mat_top = expr_mat.loc[stat[0:top].index]
    return expr_mat, expr_mat_top


# ---matrixTransform-----------------------------------------

def PCAmatrix(expr_mat, n_components=None, outputprefix=None):
    '''
    PCA analysis for given matrix
    '''
    expr_mat = expr_mat.T
    pca = PCA(n_components=n_components)
    pca.fit(expr_mat)
    columns = ['PC%i' % i for i in range(1, pca.n_components_ + 1)]
    pc_mat = pd.DataFrame(pca.transform(expr_mat), columns=columns,
                          index=expr_mat.index)
    pc_mat.index.name = "Sample"
    pc_mat = pc_mat.applymap(lambda x: "{:.2f}".format(x))
    pc_variance_ratio = pd.DataFrame({"PC": columns, "Explained_variance": pca.explained_variance_ratio_})
    pc_variance_ratio['Explained_variance'] = pc_variance_ratio['Explained_variance'].apply(
        lambda x: "{:.1%}".format(x))
    if outputprefix:
        pc_mat.to_csv(outputprefix + '.PCs.tsv', index=True, sep="\t")
        pc_variance_ratio.to_csv(outputprefix + '.PC_variance.tsv', index=False, sep="\t")
    return pc_mat, pc_variance_ratio


# ---PCAmatrix--------------------------------

def correlationMatrix(expr_mat, cor_method="pearson", outputprefix=None):
    '''
    Compute matrix correlation for normalized log2 transformed ones.
    
    cor_method: pearson (default), kendall, spearman
    '''
    # print(expr_mat.shape)
    corr = expr_mat.corr(method=cor_method)
    corr.index.name = cor_method
    # print(corr)
    outlierDF = outlierDetect(corr, threshold=4)
    corr_output = corr.applymap(lambda x: "{:.3f}".format(x))
    corr_output_withOutlier = outlierDF.join(corr_output)
    corr_output_withOutlier.sort_values(by="Outlier", axis=0, ascending=False, inplace=True)
    # print(corr)
    if outputprefix:
        corr_output.to_csv(outputprefix + '.' + cor_method + '.correlation.tsv',
                           index=True, sep="\t")
        corr_output_withOutlier.to_csv(
            outputprefix + '.' + cor_method + '.correlation_with_outlier.tsv',
            index=True, sep="\t")
    return corr, outlierDF


# ---correlationMatrix------------------------

def outlierDetect(corr, threshold=4):
    ''''''
    k = corr.abs().sum() - 1
    zscore = np.abs(stats.zscore(k))
    return pd.DataFrame({"Outlier": zscore > threshold}, index=corr.columns)


# ---outlierDetect----------------------
def checkMetadata(metadata, header=0):
    '''
    Used to check if metadata contains multiple species, remove columns containing unique values. 

    metadata: sample attribute table
        Species	Project_ID	Assay_type	Cell_type	Treatment	Sample
        Human	Altitude	RNA-seq	Peripheral blood cell	Pre-exposed to high altitude	PreExp_11
        Human	Altitude	RNA-seq	Peripheral blood cell	Pre-exposed to high altitude	PreExp_18
        Human	Altitude	RNA-seq	Peripheral blood cell	Pre-exposed to high altitude	PreExp_24
    '''

    meta_mtx = pd.read_csv(metadata, sep="\t", header=header, index_col=None)
    summary = meta_mtx.describe(include="all")
    # print(meta_mtx)
    # print(summary)
    # Remove columns containing only one value
    meta_mtx = meta_mtx.loc[:, summary.loc["unique"] > 1]
    meta_mtx_columns = list(meta_mtx.columns)

    multiple_species = False
    if 'Species' in meta_mtx_columns:
        multiple_species = summary.loc["unique", "Species"] > 1
        if multiple_species:
            meta_mtx['Sample'] = meta_mtx["Species"] + "_" + meta_mtx["Sample"]
            meta_mtx_columns.remove('Species')
    projectID = 0
    if 'Project_ID' in meta_mtx_columns:
        projectID = 1
        meta_mtx_columns.remove('Project_ID')
    meta_mtx_columns.remove('Sample')
    # print(meta_mtx_columns)
    assert len(meta_mtx_columns) >= 1, "Wrong meta data, no different groups."
    # todo lingdao
    # print(meta_mtx[meta_mtx_columns])
    # todo
    # meta_mtx['Group'] = meta_mtx[meta_mtx_columns].astype(str).add('_').sum(axis=1).str[:-1]
    if len(meta_mtx_columns) == 1 and 'Group' not in meta_mtx_columns:
        meta_mtx['Group'] = meta_mtx[meta_mtx_columns]
    else:
        meta_mtx['Group'] = meta_mtx[meta_mtx_columns].astype(str).add('_').sum(axis=1)[:-1]

    if multiple_species:
        meta_mtx_columns.insert(0, 'Species')

    samp_by_grp = meta_mtx[['Sample', 'Group']].groupby("Group")
    samp_by_grp_count = samp_by_grp.count()
    singleGroup = samp_by_grp_count[samp_by_grp_count["Sample"] < 2]
    meta_mtx_columns.append('Single_group')
    meta_mtx['Single_group'] = False
    if not singleGroup.empty:
        meta_mtx.loc[meta_mtx["Group"].isin(singleGroup.index), 'Single_group'] = True

    meta_mtx_columns.append('Single_batch')
    meta_mtx['Single_batch'] = False

    if projectID:
        samp_by_grp_sum = set(samp_by_grp.sum()['Sample'])
        samp_by_pid = meta_mtx[['Sample', 'Project_ID']].groupby("Project_ID")
        samp_by_pid_count = samp_by_pid.count()

        singleBatch = samp_by_pid_count[samp_by_pid_count["Sample"] < 2]
        if not singleBatch.empty:
            meta_mtx.loc[meta_mtx["Project_ID"].isin(singleBatch.index), 'Single_batch'] = True

        samp_by_pid_sum = set(samp_by_pid.sum()['Sample'])
        if (samp_by_grp_sum != samp_by_pid_sum):
            meta_mtx_columns.insert(0, 'Project_ID')
    meta_mtx_columns.insert(0, "Group")
    meta_mtx_columns.insert(0, "Sample")
    meta_mtx = meta_mtx[meta_mtx_columns]
    return (meta_mtx, multiple_species)


# ---------------------------------------------


def extractGivenGeneExpression(inputfile, geneL=[], sampleL=[], header=None, outputfile=None):
    '''
    Used in gene detail and GEM profile page for extracting genes
    '''
    matrix = pd.read_csv(inputfile, sep="\t", header=header, index_col=None)
    if header == None:
        matrix.columns = ["Gene", "Expr", "Sample", "Species"]
    if geneL and sampleL:
        matrix = matrix.loc[matrix['Gene'].isin(geneL) & matrix['Sample'].isin(sampleL)]
    elif geneL:
        matrix = matrix.loc[matrix['Gene'].isin(geneL)]
    elif sampleL:
        matrix = matrix.loc[matrix['Sample'].isin(sampleL)]
    else:
        print("Are you kidding?!", file=sys.stderr)
    if outputfile:
        matrix.to_csv(outputfile, sep="\t")
    else:
        return matrix


# ---extractGivenGeneExpression--------------------------------------------

def transferLongToWideForTwoSpecies(inputfile, orthologfile, outputfile=None,
                                    header=None, index_col=None,
                                    usecols=None, fill_value=0, remove_all_zero=True,
                                    all_integer=False, debug=False):
    '''
    inputfile: 4 columns matrix file without header by default

                A1BG    9   HAExp3D_11  Human
                ADA 171 HAExp3D_11  Human
                CDH2    3   HAExp3D_11  Human
                AKT3    73  HAExp3D_11  Human
                MED6    74  HAExp3D_11  Human
    
    orthologfile: 2 column matrix file with first row as header line. 
                (Duplicate value in the second column will only be kept once.)
                (The program will transfer gene symbol in the second column to the first column.)

                Human   Mouse
                A1BG    A1BG
                AKT3    AKT3

    '''

    matrix = pd.read_csv(inputfile, sep="\t", header=header, index_col=index_col, usecols=usecols)
    if header == None:
        matrix.columns = ["Gene", "Expr", "Sample", "Species"]
    matrix["Sample"] = matrix["Species"] + "_" + matrix["Sample"]

    ortholog_matrix = pd.read_csv(orthologfile, sep="\t", header=0, index_col=None)
    ortholog1, ortholog2 = ortholog_matrix.columns
    # Second column map to first column
    ortholog_matrix = ortholog_matrix.drop_duplicates(subset=[ortholog2], keep="first")
    ortholog1_geneL = ortholog_matrix[ortholog1]
    ortholog2_geneL = ortholog_matrix[ortholog2]
    orthologTransfer = dict(zip(ortholog2_geneL, ortholog1_geneL))

    # Select first species and filter genes not in ortholog table
    ortholog1_expr = matrix.loc[matrix["Species"] == ortholog1]
    ortholog1_expr = matrix.loc[matrix['Gene'].isin(ortholog1_geneL)]
    # Select second species and filter genes not in ortholog table
    ortholog2_expr = matrix.loc[matrix["Species"] == ortholog2]
    ortholog2_expr = matrix.loc[matrix['Gene'].isin(ortholog2_geneL)]
    # Transfer gene symbol
    ortholog2_expr = ortholog2_expr.replace({"Gene": orthologTransfer})
    # Concatenate two species together
    matrix = pd.concat([ortholog1_expr, ortholog2_expr])

    matrix = transferLongToWide(matrix, index="Gene", columns="Sample", values="Expr",
                                fill_value=fill_value, remove_all_zero=remove_all_zero,
                                all_integer=all_integer, debug=debug)
    if outputfile:
        matrix.to_csv(outputfile, sep="\t")
    else:
        return matrix


# ---transferLongToWideForTwoSpecies--------------------------------------------

def transferLongToWideSameSpecies(inputfile, outputfile=None, header=None, index_col=None,
                                  usecols=None, fill_value=0, remove_all_zero=True,
                                  all_integer=False, debug=False):
    '''
    Transfer concatenated gene expression matrix in long format to wide format.

    inputfile: 4 columns matrix file without header by default

                A1BG    9   HAExp3D_11  Human
                ADA 171 HAExp3D_11  Human
                CDH2    3   HAExp3D_11  Human
                AKT3    73  HAExp3D_11  Human
                MED6    74  HAExp3D_11  Human

    outputfile: Default None. Accept a filename. If None, the function will return a transferred matrix)

    fill_value: Default 0. Missed values will be replaced as 0.

    remove_all_zero: Default True. Remove a row for all zeros.

    all_integer: Default False. Set to True for reads count, then the numbers in matrix will all be integers.
    
    If duplicate values (for the combination of index/columns) exist, the program will try to only keep the first occurence and give a warning.
    
    '''

    matrix = pd.read_csv(inputfile, sep="\t", header=header, index_col=index_col, usecols=usecols)

    if header == None:
        matrix.columns = ["Gene", "Expr", "Sample", "Species"]

    matrix = transferLongToWide(matrix, index="Gene", columns="Sample", values="Expr",
                                fill_value=fill_value, remove_all_zero=remove_all_zero,
                                all_integer=all_integer, debug=debug)
    if outputfile:
        matrix.to_csv(outputfile, sep="\t")
    else:
        return matrix


# ------------------------------------------------------

def transferLongToWide(matrix, index="Gene", columns="Sample", values="Expr",
                       fill_value=0, remove_all_zero=True,
                       all_integer=False, debug=False):
    '''
    Transfer concatenated gene expression matrix in long format to wide format.

    matrix: 3 or 4 columns matrix file with header (By default, Species column will not be used)
                Gene    Expr    Sample  Species
                A1BG    9   HAExp3D_11  Human
                ADA 171 HAExp3D_11  Human
                CDH2    3   HAExp3D_11  Human
                AKT3    73  HAExp3D_11  Human
                MED6    74  HAExp3D_11  Human


    fill_value: Default 0. Missed values will be replaced as 0.

    remove_all_zero: Default True. Remove a row for all zeros.

    all_integer: Default False. Set to True for reads count, then the matri will be all integers.
    
    If duplicate values (for the combination of index/columns) exist, the program will try to only keep the first occurence and give a warning.
    
    '''

    # matrix = pd.read_csv(inputfile, sep="\t", header=header, index_col=index_col, usecols=usecols)
    # if header == None:
    #    matrix.columns = ["Gene", "Expr", "Sample", "Species"]
    try:
        # todo ???
        # matrix.to_csv('../ams/AMS.ttttttttttt.txt', sep='\t',index=False)
        matrix = matrix.pivot(index=index, columns=columns, values=values)

    except ValueError:
        print(
            "Duplicate gene names in at least one sample were found. The program will keep only the first occurence!!!",
            file=sys.stderr)
        matrix = matrix.drop_duplicates(subset=["Gene", "Sample"], keep="first")
        matrix = matrix.pivot(index=index, columns=columns, values=values)
    # ----------------------------------
    if fill_value != None:
        try:
            fill_value = int(fill_value)
        except ValueError:
            try:
                fill_value = float(fill_value)
            except ValueError:
                fill_value = fill_value
        matrix = matrix.fillna(fill_value)
        if debug:
            print(matrix.head(), file=sys.stderr)
    if all_integer:
        matrix = matrix.astype(int)
    if remove_all_zero:
        ## Remove all ZEROs
        matrix = matrix.loc[(matrix > 0).any(axis=1)]
    return matrix


# ------------------------------------------------------


# --------------------------------------------------------------------
def readLongIn(file, label="Anno", go_keepS=set([])):
    gene_col = 2
    go_id_col = 0
    go_desp_col = 1
    header = 1
    goAnnoD = defaultdict(set)
    geneSet = set([])
    for line in open(file):
        if header:
            header -= 1
            continue
        lineL = line.strip().split('\t')
        if len(lineL) < 3:
            print >> sys.stderr, line
        gene = lineL[gene_col]
        go_id = lineL[go_id_col]
        go_desp = lineL[go_desp_col]
        geneSet.add(gene)
        if (go_id == '--' and go_desp == '--') or (go_keepS and go_desp not in go_keepS):
            continue
        key = '\t'.join([go_id, go_desp])
        # if key not in goAnnoD:
        #    goAnnoD[key] = set([gene])
        # else:
        #    goAnnoD[key].add(gene)
        goAnnoD[key].add(gene)
    # -------------END reading file----------
    totalGene = len(geneSet)
    return goAnnoD, totalGene, geneSet


# ---------------------------------------------------

def DEenrichment(outputprefix, species, enrichTypeL):
    gene_file = outputprefix + '.all.DE'

    for annoType in enrichTypeL:
        go_file = go_dir + species + '.' + annoType
        functionalEnrichment(go_file, gene_file, annoType, output=outputprefix)


# ---------------------------------------------------

def functionalEnrichment(go_file, gene_file, annoType, output=None, pvalue_thresh=0.1, topPlot=0, top_term=0,
                         go_keep=None, debug=False):
    '''
    Functional description:
        This is designed to do the GO enrichment in house.

    go_file:
        The format of Gene Ontology file used 

        ## Optional annotation format--(long)--------
        GO term GO Description  Gene
        GO:0045116  Function description    Bra002219
        GO:0045116  Function description    Bra006497
        GO:0004091  Function description    Bra000230
        GO:0004091  Function description    Bra000969
        GO:0004091  Function description    Bra004627

    gene_file:
        One column gene list file or two columns with additional group column for each gene.

    annoType:
        Any string like BP, MF, CC or KEGG, Reactome represents what type of annotation used.

    pvalue_thresh:
        Multiple test corrected pvalue for enriched terms. Default 0.1.
    output:
        If there are 2 columns in gene list file, the second column will be used as a tag for output filename. Given string will be used as prefix.

    top_term:
        Specify number of top items output for each group. Default 0. Accept integers like 5 (only used for module split).

    topPlot:
        Specify number of top items for enrichment plot. Default 0. Accept integers like 20.
    go_keep:
        One column file containing anotation terms to be kept for enrichment analysis. If not supplied all annotation will be kept.

    TEST WELL
    '''

    pvalue_thresh_ori = pvalue_thresh
    if go_keep:
        go_keep = set([line.strip() for line in open(go_keep)])
    else:
        go_keep = set([])
    # -----------------------------------
    # annoGeneD = set([j for i in open(go_file) \
    #    for j in i.split('\t')[2].split(',')])
    # -----------------------------------------------
    geneGrpD = {}

    for line in open(gene_file):
        lineL = line.strip().split('\t')
        len_lineL = len(lineL)
        if len_lineL == 2:
            gene, grp = lineL
            grp = grp.replace(' ', '_')
        elif len_lineL == 1:
            gene = lineL[0]
            grp = 1
        if grp not in geneGrpD:
            geneGrpD[grp] = set([])
        geneGrpD[grp].add(gene)
    # ----------------------------------------------
    # geneD = set([i.split("\t")[0].strip() for i in open(gene_file)])
    goAnnoD, total_anno_gene, total_anno_gene_set = readLongIn(go_file, label=annoType, go_keepS=go_keep)
    if debug:
        print("total_anno_gene", total_anno_gene, file=sys.stderr)
        print("total_anno_gene_set", total_anno_gene_set, file=sys.stderr)

    out_file = ''
    for grp, geneD in geneGrpD.items():
        if grp == 1:
            if output:
                out_file = output
                fh_out = open(output, 'w')
            else:
                fh_out = sys.stdout
        else:
            if output:
                out_file = output + '.' + annoType + '.' + grp + '.xls'
                fh_out = open(out_file, 'w')
            else:
                out_file = gene_file + '.' + annoType + '.' + grp + '.xls'
                fh_out = open(out_file, 'w')
        # ---------------------------------------------
        if debug:
            print("geneD", geneD, file=sys.stderr)
        geneD = geneD.intersection(total_anno_gene_set)
        geneD_len = len(geneD)
        if debug:
            print("Annotated geneD_len", geneD_len, file=sys.stderr)
            print("Annotated geneD", geneD, file=sys.stderr)
        # --------------------------------
        annoL = []
        header = 1
        tmpL = []
        pL = []
        headerL = []
        overlapL = []
        for go_id, go_gene in goAnnoD.items():
            # go_desp, go_gene = itemL
            # anno_gene = [gene for gene in go_gene if gene in geneD]
            # anno_gene = set(anno_gene)
            anno_gene = go_gene.intersection(geneD)
            annoCount = len(anno_gene)
            if annoCount == 0:
                continue
            if debug:
                print(go_id, file=sys.stderr)
                print(go_gene, file=sys.stderr)
                print(geneD, file=sys.stderr)
                print("Anno_gene", anno_gene, file=sys.stderr)
                print("Anno_count", annoCount, file=sys.stderr)
            if annoCount > geneD_len:
                print("go_gene", go_gene, file=sys.stderr)
                print("anno_gene", anno_gene, file=sys.stderr)
                sys.exit(1)
            termCount = len(go_gene)
            fracT = annoCount * 1.0 / geneD_len / termCount * total_anno_gene
            if annoCount >= 3:
                if debug:
                    print(go_id, "large 3", file=sys.stderr)
                if fracT > 1:
                    # print(go_id, "fracT > 1", file=sys.stderr)
                    p = pvalue(annoCount, termCount - annoCount,
                               geneD_len - annoCount,
                               total_anno_gene - geneD_len + annoCount - termCount)
                    # p = p.two_tail
                    p = p.right_tail
                    # print([go_desp, annoCount-1, termCount-annoCount, geneD_len-annoCount, total_anno_gene-geneD_len+annoCount-termCount, p], file=sys.stderr)
                    # print([go_desp, annoCount, termCount, geneD_len, total_anno_gene, p], file=sys.stderr)
                    # if fracT > 1 and p <= pvalue_thresh*2:
                    fracT = "%.2f" % fracT
                    tmpL.append([go_id, ', '.join(anno_gene), str(annoCount),
                                 str(geneD_len), str(termCount), str(total_anno_gene), fracT, p, str(p)])
                    pL.append(p)
                else:
                    fracT = "%.2f" % fracT
                    overlapL.append([go_id, ', '.join(anno_gene), str(annoCount),
                                     str(geneD_len), str(termCount), str(total_anno_gene), fracT, "1", "1"])
            else:
                # for annoCount < 3
                fracT = "%.2f" % fracT
                overlapL.append([go_id, ', '.join(anno_gene), str(annoCount),
                                 str(geneD_len), str(termCount), str(total_anno_gene), fracT, "1", "1"])

        # -------------END reading file----------
        # ---Enrich
        if pL:
            # print(pL, file=sys.stderr)
            try:
                p_adjL = multipletests(pL, method="fdr_bh")[1]
            except ZeroDivisionError:
                print("ZeroDivisionError, feel free to continue", file=sys.stderr)
                print(pL, file=sys.stderr)
                print(tmpL, file=sys.stderr)
                continue
            # ---------------------------------------------
            for eachtmpL, p_adj in zip(tmpL, p_adjL):
                eachtmpL[-2] = format(eachtmpL[-2], '0.2E')
                eachtmpL[-1] = format(p_adj, '0.2E')
            # ------------------------------------------------
            tmpL.sort(key=lambda x: float(x[-2]))
        else:
            # no enrich
            print("No significant terms", file=sys.stderr)
            tmpL = overlapL
            tmpL.sort(key=lambda x: float(x[-3]))

        # print(tmpL, file=sys.stderr)
        headerL = [annoType + "_ID", annoType + "_description"]
        count = 0
        # This is set to output results even unsignifncant
        change_p = 0
        for i in tmpL:
            if pvalue_thresh == 1 and count > 50:
                pvalue_thresh = pvalue_thresh_ori
                change_p = 1
            if float(i[-2]) > pvalue_thresh:
                if change_p:
                    break
                if count < 5:
                    pvalue_thresh = 1
                    change_p = 1
                # break
            if headerL:
                print(
                    "%s\t%s\tTarget_gene\tTarget_count\tTarget_total\tTerm_count\tTotal_anno\tOdds_ratio\tPvalue\tFDR" % (
                        headerL[0], headerL[1]), file=fh_out)
                headerL = []
            print('\t'.join(i), file=fh_out)
            count += 1

        # ----------------------
        if out_file or grp != 1:
            fh_out.close()
            if topPlot:
                os.system("head -n " + str(topPlot + 1) + ' ' + out_file + '>' + out_file + '.top')
                enrichPlot = ["sp_enrichmentPlot.sh -f ", out_file + '.top',
                              '-o Odds_ratio -T numeric -v KEGG_description -c FDR -s TargetCount -l FDR -x "Odds ratio" -y "KEGG pathway"']
                os.system(' '.join(enrichPlot))
# --------------------------------------------------------------------------------------------------------
