Species	Project_ID	Assay_type	Cell_type	Treatment	Sample
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BK32
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BK24
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BT4
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BT7
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BT28
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BT1
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BT18
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BK11
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BK22
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BT33
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BT26
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BT29
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BT31
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BK9
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BK18
Human	Altitude	RNA-seq	Peripheral blood cell	Exposed to high altitude 3 days	HAExp3D_BT13
