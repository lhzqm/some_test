# from .ehbioUtility import DEgeneAnalysis, sampleClusterMatrixGenerate, transferLongToWideSameSpecies
import pandas as pd
from disease.ehbioUtility import transferLongToWideSameSpecies, DEgeneAnalysis, sampleClusterMatrixGenerate

# def sampleClusterMatrixGenerate(exprfile, metadata, orthologfile=None, outputprefix=None,
# fill_value=0, remove_all_zero=True,
# expression_type=False, debug=False):

# 生成热图数据
# sampleClusterMatrixGenerate(exprfile='../bf_ams/AMS.merger.txt',
#                             metadata='./AMS.show.txt',
#                             outputprefix='../bf_ams/AMS',
#                             expression_type='reads_count')

# print(transferLongToWideSameSpecies('../ams/AMS.merger.txt', fill_value=0, remove_all_zero=True, all_integer=True))


# panda = transferLongToWideSameSpecies('../ams/AMS.merger.txt', fill_value=0, remove_all_zero=True, all_integer=True,
#                                       outputfile='../ams/AMS.expr_mat.tsv')


# index = "Gene"
# columns = "Sample"
# values = "Expr",
# data = pd.read_csv('../ams/AMS.ttttttttttt.txt', sep='\t')
# print(data.head())
# data.pivot(index=index, columns=columns, values=values)
# panda.to_csv('./AMS.expr_mat.tsv', sep='\t')

#  需要这两个
# gene_expr_file = outputprefix + '.expr_mat.tsv'
# metadata = outputprefix + ".meta.tsv"

DEgeneAnalysis(expression_type="reads_count", outputprefix='../ams/AMS', fdr=0.05, log2fc=1)


# DEgeneAnalysis(expression_type="reads_count", outputprefix='../hapc/HAPC', fdr=0.05, log2fc=1)
#
# DEgeneAnalysis(expression_type="reads_count", outputprefix='../hph/HPH', fdr=0.05, log2fc=1)


# f = open('../ams/AMS.meta.tsv', 'r')
#
# sample = list()
# f1 = open('../ams/AMS.merger.txt', 'w')
# for i in f:
#     tmp = i.split("\t")
#     if tmp[0] != "Sample":
#         sample.append(tmp[0])
#
# for i in sample:
#     f2 = open("../readscount/{}.gene.readscount.txt".format(i), 'r')
#     for j in f2:
#         f1.write(j)
