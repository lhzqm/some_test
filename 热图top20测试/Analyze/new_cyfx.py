# import pandas as pd
# # 生成 meta.tsv
# # from disease.ehbioUtility import DEgeneAnalysis
from ehbioUtility import DEgeneAnalysis, transferLongToWideSameSpecies
#
# data = pd.read_csv('./AMS.show.txt', sep="\t")
# data[['Sample', 'Group']].to_csv('./AMS.meta.tsv', sep='\t', index=None)
#
# data = pd.read_csv('./HAPC.show.txt', sep="\t")
# data[['Sample', 'Group']].to_csv('./HAPC.meta.tsv', sep='\t', index=None)
#
# data = pd.read_csv('./HPH.show.txt', sep="\t")
# data[['Sample', 'Group']].to_csv('./HPH.meta.tsv', sep='\t', index=None)
#
# # 生成 merger.txt
# data = pd.read_csv('./AMS.meta.tsv', sep="\t")
# sample = data['Sample'].tolist()
# print(sample)
# print(len(sample))
# print(len(list(set(sample))))
#
# empt = pd.DataFrame()
# for i in data['Sample'].tolist():
#     data2 = pd.read_csv("../readscount/{}.gene.readscount.txt".format(i), sep="\t", header=None)
#     empt = pd.concat([empt, data2], ignore_index=True, sort=False)
# empt.to_csv('./AMS.merger.txt', sep="\t", index=None, header=None)
#
# data = pd.read_csv('./HPH.meta.tsv', sep="\t")
# sample = data['Sample'].tolist()
# print(sample)
# print(len(sample))
# print(len(list(set(sample))))
#
# empt = pd.DataFrame()
# for i in data['Sample'].tolist():
#     data2 = pd.read_csv("../readscount/{}.gene.readscount.txt".format(i), sep="\t", header=None)
#     empt = pd.concat([empt, data2], ignore_index=True, sort=False)
# empt.to_csv('./HPH.merger.txt', sep="\t", index=None, header=None)
#
# data = pd.read_csv('./HAPC.meta.tsv', sep="\t")
# sample = data['Sample'].tolist()
# print(sample)
# print(len(sample))
# print(len(list(set(sample))))
#
# empt = pd.DataFrame()
# for i in data['Sample'].tolist():
#     data2 = pd.read_csv("../readscount/{}.gene.readscount.txt".format(i), sep="\t", header=None)
#     empt = pd.concat([empt, data2], ignore_index=True, sort=False)
# empt.to_csv('./HAPC.merger.txt', sep="\t", index=None, header=None)
#
# print("生成expr_mat.tsv")
# transferLongToWideSameSpecies('./AMS.merger.txt', fill_value=0, remove_all_zero=True, all_integer=True,
#                               outputfile='./AMS.expr_mat.tsv')
# transferLongToWideSameSpecies('./HPH.merger.txt', fill_value=0, remove_all_zero=True, all_integer=True,
#                               outputfile='./HPH.expr_mat.tsv')
# transferLongToWideSameSpecies('./HAPC.merger.txt', fill_value=0, remove_all_zero=True, all_integer=True,
#                               outputfile='./HAPC.expr_mat.tsv')
#
# DEgeneAnalysis(expression_type="reads_count", outputprefix='./AMS', fdr=0.1, log2fc=1)
# DEgeneAnalysis(expression_type="reads_count", outputprefix='./HAPC', fdr=0.1, log2fc=1)
# DEgeneAnalysis(expression_type="reads_count", outputprefix='./HPH', fdr=0.1, log2fc=1)




