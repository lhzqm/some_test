import pandas as pd
import numpy as np

dates = pd.date_range('20170101', periods=6)
df = pd.DataFrame(np.random.randn(6, 4), index=dates, columns=list('ABCD'))
print("df:")
print(df)
print('-' * 50)
print('-' * 50)
# 通过索引选择
print("df.loc[:,['A','B']]")
print(df.loc[:, ['A', 'B']])

#显示标签切片，包括两个端点
print('-'*50)
print("df.loc['20170102':'20170104',['A','B']]")
print(df.loc['20170102':'20170104'])
print('-'*50)
