import subprocess

command = ["ls", "-l"]
completed = subprocess.run(command, capture_output=True, text=True)

print("args:", completed.args)
print("returncode:", completed.returncode)
print("stderr:", completed.stderr)
# print("stdout:", completed.stdout)
import difflib

import Levenshtein


def get_equal_rate(str1, str2):
    return Levenshtein.ratio(str1, str2)


def get_equal_rate_1(str1, str2):
    # 判断相似度的方法，用到了difflib库
    return difflib.SequenceMatcher(None, str1, str2).quick_ratio()


print(get_equal_rate("sox2".upper(), "sox".upper()))
print(get_equal_rate("qsox2".upper(), "sox".upper()))
print(get_equal_rate("sox21".upper(), "sox".upper()))
print(get_equal_rate("sox2-ot".upper(), "sox".upper()))
print(get_equal_rate("foxd3".upper(), "sox".upper()))
print(get_equal_rate("sox21-as1".upper(), "sox".upper()))
print(get_equal_rate("gsc".upper(), "sox".upper()))
print(get_equal_rate("cdx2".upper(), "sox".upper()))
print(get_equal_rate("hhex".upper(), "sox".upper()))
print(get_equal_rate("eomes".upper(), "sox".upper()))
print("-------------------------")
print(get_equal_rate_1("sox2".upper(), "sox".upper()))
print(get_equal_rate_1("qsox2".upper(), "sox".upper()))
print(get_equal_rate_1("sox21".upper(), "sox".upper()))
print(get_equal_rate_1("sox2-ot".upper(), "sox".upper()))
print(get_equal_rate_1("foxd3".upper(), "sox".upper()))
print(get_equal_rate_1("sox21-as1".upper(), "sox".upper()))
print(get_equal_rate_1("gsc".upper(), "sox".upper()))
print(get_equal_rate_1("cdx2".upper(), "sox".upper()))
print(get_equal_rate_1("hhex".upper(), "sox".upper()))
print(get_equal_rate_1("eomes".upper(), "sox".upper()))
result_data = [
    {"Gene_symbol": "qsox2"},
    {"Gene_symbol": "sox21"},
    {"Gene_symbol": "sox2-ot"},
    {"Gene_symbol": "foxd3"},
    {"Gene_symbol": "sox21-as1"},
    {"Gene_symbol": "gsc"},
    {"Gene_symbol": "cdx2"},
    {"Gene_symbol": "sox2"},
    {"Gene_symbol": "hhex"},
    {"Gene_symbol": "eomes"},

]

keyword = "sox"

result_data = sorted(
    result_data,
    key=lambda x: (
        (1 - get_equal_rate(x['Gene_symbol'].upper(), keyword.upper())),
        (get_equal_rate_1(x['Gene_symbol'].upper(), keyword.upper())),
    )
)
print(result_data)
