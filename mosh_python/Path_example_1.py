from pathlib import Path
from time import ctime
import shutil

# 0.文件系统
Path(r"C:Program Files\Microsoft")
Path("/usr/local/bin")
Path()
Path("ecommerce/__init__.py")
Path() / "ecommerce" / "__init__.py"

Path.home()

path = Path("ecommerce/__init__.py")
path.exists()
path.is_file()
path.is_dir()

print(path.name)
print(path.stem)
print(path.suffix)
print(path.parent)

path = path.with_name("file.txt")
print(path.absolute())
path = path.with_suffix(".py")
print(path.absolute())

# 1.操作目录
path = Path("ecommerce")
# path.exists()
# path.mkdir()
# path.rmdir()
# path.rename("ecommerce2")

print(path.iterdir())

for p in path.iterdir():
    print(p)
    print("*")

paths = [p for p in path.iterdir() if p.is_dir()]

glob_py_files = [p for p in path.glob("*.py")]

py_files = [p for p in path.rglob("*.py")]

print(py_files)

# 2.操作文件
path = Path("ecommerce/__init__.py")
# path.exists()
# path.rename("init.txt")
# path.unlink()

print(path.stat())

print(ctime(path.stat().st_ctime))

print(path.read_text())
# path.write_text()
# path.write_bytes()

source = Path("ecommerce/__init__.py")

target = Path() / "__init__.py"

# target.write_text(source.read_text())

shutil.copy(source, target)


# 处理压缩文件
