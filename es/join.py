import pandas as pd

# 读文件
compound_names = pd.read_csv("compound_names.tsv", sep="\t", index_col=None, header=0)
compound_names = compound_names[["cid", "mol_name"]]
cas = pd.read_csv("cas.tsv", sep="\t", index_col=None, header=0)
cas = cas[["cid", "cas_no"]]
compound_properties = pd.read_csv("compound_properties.tsv", sep="\t", index_col=None, header=0)
compound_properties = compound_properties[["cid", "smiles"]]

data = pd.merge(compound_names, cas, on="cid", how="left")

data = pd.merge(data, compound_properties, on="cid", how="left")

# 生成结果文件 检查错误
data.to_csv("{}.tsv".format("join"), sep="\t", index=None)

print(data.head())
