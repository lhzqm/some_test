import json

import pandas as pd
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

mapping = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,
    },
    "mappings": {
        "doc": {
            "dynamic": False,
            "properties": {
                "cid": {
                    "type": "keyword"
                },
                "cid_value": {
                    "type": "text"
                }
            }
        }
    }
}


def index(index_name):
    if es.indices.exists(index=index_name):
        es.indices.delete(index=index_name, ignore=[400, 404])

    es.indices.create(index=index_name, ignore=400, body=mapping)

    data = pd.read_csv("compound_names.tsv", sep="\t", index_col=None, header=0)

    # 添加_index列 不加 插入数据时 会报错
    data["_index"] = index_name

    # 添加_type列  不加 插入数据时 会报错
    data["_type"] = "doc"

    row, col = data.shape
    _id = range(1, row + 1)
    data["_id"] = _id

    data = data[["compound_names_cid", "mol_name", "_index", "_type", "_id"]]
    # data = data[["compound_properties_cid", "smiles"]]

    dict_in_list_object = data.to_dict("records")

    bulk(es, dict_in_list_object)


index_name = "new_compound_names"

# index(index_name=index_name)
# print("ok")
# quit()

match_all = {
    "query": {
        "match_all": {}
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}

match = {
    "query": {
        "wildcard": {
            "mol_name": "(1'R,5Z)-3-(1'-acetoxybutyl)-4-bromo-5-(brmomethylene)furan-2(5H)-one"
        }
    }
}

CMNPD21 = {
    "query": {
        "match": {
            "compound_names_cid": "CMNPD21"
        }
    }
}

wildcard = {
    "query": {
        "wildcard": {
            # "mol_name": "3-[(R)-1-Acetoxybutyl]-4-bromo-5-(bromomethylene)-2(5H)-furanone"
            "mol_name": "(1'R,5Z)-3-(1'-acetoxybutyl)-4-bromo-5-(brmomethylene)furan-2(5H)-one*"
            # "mol_name": "\\(1'r,5z\\)\\-3\\-\\(1'\\-acetoxybutyl\\)\\-4\\-bromo\\-5\\-\\(brmomethylene\\)furan\\-2\\(5h\\)\\-one*"
        }
    }
}
search_text = "(1'R,5Z)-3-(1'-acetoxybutyl)-4-bromo-5-(brmomethylene)furan-2(5H)-one"
query_dsl = {
    "query": {
        "bool": {
            "should": [
                # compound_names_cid Cid
                {"wildcard": {"cid": f"{search_text}*"}},

                # compound_properties Smiles
                {"wildcard": {"smiles": f"{search_text}*"}},

                # compound_names Name mol_name
                {"wildcard": {"mol_name": f"{search_text}*"}},

                # CAS
                {"wildcard": {"cas_no": f"{search_text}*"}},

                # chembl_targets Tid
                {"wildcard": {"tid": f"{search_text}*"}},
                # {"match": {"target_name": search_text}},

                # docs Did
                {"wildcard": {"did": f"{search_text}*"}},

                # taxonomy Oid
                {"wildcard": {"oid": f"{search_text}*"}},
            ],
        }
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}
result = es.search(index=index_name, body=query_dsl, filter_path=['hits.hits._score', "hits.hits._source"],
                   doc_type="doc")
# result = es.search(index=index_name, body=match, _source=['compound_names_cid', 'mol_name'])

print(json.dumps(result, indent=2, ensure_ascii=False))
