def needs_escaping(character):
    escape_chars = {
        '\\': True, '+': True, '-': True, '!': True,
        '(': True, ')': True, ':': True, '^': True,
        '[': True, ']': True, '\"': True, '{': True,
        '}': True, '~': True, '*': True, '?': True,
        '|': True, '&': True, '/': True
    }
    return escape_chars.get(character, False)


query = "[C@H]12O[C@H]([C@H](C\C=C/C#C)O[C@@H](C(Br)CC)C[C@H]1Br)C2"
sanitized = ''

for character in query:
    if needs_escaping(character):
        sanitized += '\\\{}'.format(character)
    else:
        sanitized += character

print(sanitized)
