import json

import pandas as pd
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

wildcard = {
    "query": {
        "wildcard": {
            "cas_no": "289\\-16\\-7*"
        }
    }
}

match = {
    "query": {
        "match": {
            "cas_no": "58966-89-5*"
        }
    }
}

match_all = {
    "query": {
        "match_all": {}
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}

index_name = "cas"
result = es.search(index=index_name, body=match, filter_path=['hits.hits._score', "hits.hits._source"])

print(json.dumps(result, indent=2, ensure_ascii=False))
