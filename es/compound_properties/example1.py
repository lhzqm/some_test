import pandas as pd
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")


def create_index(index_name):
    f = open("gene_mapping.json", "r")
    mapping = f.read()
    f.close()

    if es.indices.exists(index=index_name):
        es.indices.delete(index=index_name, ignore=[400, 404])

    es.indices.create(index=index_name, ignore=400, body=mapping)


_index_name = "zqm_test"


# create_index(index_name=_index_name)

# compound_properties.txt

def add_data(file_name, field=None, index_name=None):
    if not index_name:
        index_name = file_name.split(".")[0]

    data = pd.read_csv(file_name, sep="\t", index_col=None, header=0)

    # 添加_index列 不加 插入数据时 会报错
    data["_index"] = index_name

    # 添加_type列  不加 插入数据时 会报错
    data["_type"] = "doc"

    row, col = data.shape
    _id = range(1, row + 1)
    data["_id"] = _id

    if field:
        # 选择字段创建索引
        field.extend(["_index", "_type", "_id"])
        # 取值
        data = data[field]
    else:
        data = data
    # 填充 缺失值
    data = data.fillna("None")

    # 生成结果文件 检查错误
    data.to_csv("{}.tsv".format(index_name), sep="\t", index=None)

    # 生成字典对象列表
    dict_in_list_object = data.to_dict("records")

    # 插入数据
    bulk(es, dict_in_list_object)
    return "索引生成完毕!"


print(add_data(file_name="compound_properties.tsv", index_name="zqm_test",
               field=["compound_properties_cid", "smiles"]))

print("ok")


