import json

import pandas as pd
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

mapping = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,
    },
    "mappings": {
        "doc": {
            "properties": {
                "compound_properties_cid": {
                    "type": "text"
                },
                "search_smiles": {
                    "type": "keyword",
                },
                "smiles": {
                    "type": "keyword"
                }
            }
        }
    }
}


def index(index_name):
    if es.indices.exists(index=index_name):
        es.indices.delete(index=index_name, ignore=[400, 404])

    es.indices.create(index=index_name, ignore=400, body=mapping)

    data = pd.read_csv("compound_properties.tsv", sep="\t", index_col=None, header=0)

    # 添加_index列 不加 插入数据时 会报错
    data["_index"] = index_name

    # 添加_type列  不加 插入数据时 会报错
    data["_type"] = "doc"

    data["search_smiles"] = data.smiles.str.lower()

    row, col = data.shape
    _id = range(1, row + 1)
    data["_id"] = _id

    data = data[["compound_properties_cid", "search_smiles", "smiles", "_index", "_type", "_id"]]
    # data = data[["compound_properties_cid", "smiles"]]

    dict_in_list_object = data.to_dict("records")

    data.to_csv("{}.tsv".format(index_name), sep="\t", index=None)

    bulk(es, dict_in_list_object)

    # for data in dict_in_list_object:
    #     es.index(index=index_name, doc_type='doc', body=data)


index_name = "new_compound_properties"

# index(index_name=index_name)
# print("ok")
# quit()

match_all = {
    "query": {
        "match_all": {}
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}

match = {
    "query": {
        "match": {
            "search_smiles": "[c@h]12o*".lower()
        }
    }
}
wildcard2 = {
    "query": {
        "wildcard": {
            "search_smiles": "[C@H]12O".lower()
        }
    }
}

wildcard = {
    "query": {
        "wildcard": {
            # "smiles": "\\[C@H\\]12O\\[C@H\\]\\(\\[C@H\\]\\(C\\\C=C\\/C#C\\)O*"
            "search_smiles": "\\[c@h\\]12o\\[c@h\\]\\(\\[c@h\\]\\(c\\\c=c\\/c#c\\)o\\[c@@h\\]\\(c\\(br\\)cc\\)c\\[c@h\\]1br\\)c2".lower()
            # "smiles": "\\[C@H\\]12O\\[C@H\\]\\(\\[C@H\\]\\(C\\\C=C\\/C#C\\)O\\[C@@H\\]\\(C\\(Br\\)CC\\)C\\[C@H\\]1Br\\)C2"
            # \\[C@@H\\]\\(C\\(Br\\)CC\\)C\\[C@H\\]1Br\\)C2
        }
    }
}
search_text = "S1CS\\(\\=O\\)\\(\\=O\\)CSCS1".lower()
search_text = "S1CSSC1".lower()

query_dsl = {
    "query": {
        "bool": {
            "should": [
                # compound_properties Smiles
                {"wildcard": {"search_smiles": f"{search_text}*"}}
            ],
        }
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}
result = es.search(index=index_name, body=query_dsl, filter_path=['hits.hits._score', "hits.hits._source"])

print(json.dumps(result, indent=2, ensure_ascii=False))

# "regexp": {
#     "title.keyword": ".*\\[test.*"
# }
# "regexp": {
#     "title": ".*\\[test.*"
# }
