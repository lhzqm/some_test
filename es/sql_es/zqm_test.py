import difflib
import json

from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18128")


def get_equal_rate_1(str1, str2):
    # 判断相似度的方法，用到了difflib库
    return difflib.SequenceMatcher(None, str1, str2).quick_ratio()


def search_for_gene(keyword, index_name, page_no, page_size):
    # 高级搜索 连接 ES !!!!!!!!!

    if keyword:
        # 主页的全局搜索使用es
        search_text = keyword.lower()
        count_query_dsl = {
            "query": {
                "bool": {
                    "should": [
                        # Gene_ID
                        {"wildcard": {"Gene_ID": f"{search_text}*"}},
                        # Gene_symbol
                        {"wildcard": {"Gene_symbol": f"*{search_text}*"}},
                        # Entrez_ID
                        {"wildcard": {"Entrez_ID": f"{search_text}*"}},
                        # Gene_description
                        {"wildcard": {"Gene_description": f"{search_text}*"}},
                        # GO_term_name
                        {"wildcard": {"GO_term_name": f"{search_text}*"}},
                        # GO_term_accession
                        {"wildcard": {"GO_term_accession": f"{search_text}*"}},
                        # Reactome_ID
                        {"wildcard": {"Reactome_ID": f"{search_text}*"}},
                        # Reactome_pathway
                        {"wildcard": {"Reactome_pathway": f"{search_text}*"}},
                    ],
                }
            }
        }
        count = es.count(index=index_name, body=count_query_dsl)["count"]

        query_dsl = {
            "query": {
                "bool": {
                    "should": [
                        # Gene_ID
                        {"wildcard": {"Gene_ID": f"{search_text}*"}},
                        # Gene_symbol
                        {"wildcard": {"Gene_symbol": f"*{search_text}*"}},
                        # Entrez_ID
                        {"wildcard": {"Entrez_ID": f"{search_text}*"}},
                        # Gene_description
                        {"wildcard": {"Gene_description": f"{search_text}*"}},
                        # GO_term_name
                        {"wildcard": {"GO_term_name": f"{search_text}*"}},
                        # GO_term_accession
                        {"wildcard": {"GO_term_accession": f"{search_text}*"}},
                        # Reactome_ID
                        {"wildcard": {"Reactome_ID": f"{search_text}*"}},
                        # Reactome_pathway
                        {"wildcard": {"Reactome_pathway": f"{search_text}*"}},
                    ],
                }
            },
            "_source": ["ID", "Gene_symbol", "Gene_description"]
        }

        if not count:
            count_query_dsl = {
                "query": {
                    "bool": {
                        "should": [
                            # Gene_description
                            {"wildcard": {"Gene_description": f"*{search_text}*"}},
                            # GO_term_name
                            {"wildcard": {"GO_term_name": f"*{search_text}*"}},
                            # GO_term_accession
                            {"wildcard": {"GO_term_accession": f"*{search_text}*"}},
                            # Reactome_ID
                            {"wildcard": {"Reactome_ID": f"*{search_text}*"}},
                            # Reactome_pathway
                            {"wildcard": {"Reactome_pathway": f"*{search_text}*"}},
                            # Alias
                            {"wildcard": {"Alias": f"{search_text}*"}},
                        ],
                    }
                }
            }
            count = es.count(index=index_name, body=count_query_dsl)["count"]

            query_dsl = {
                "query": {
                    "bool": {
                        "should": [
                            # Gene_description
                            {"wildcard": {"Gene_description": f"*{search_text}*"}},
                            # GO_term_name
                            {"wildcard": {"GO_term_name": f"*{search_text}*"}},
                            # GO_term_accession
                            {"wildcard": {"GO_term_accession": f"*{search_text}*"}},
                            # Reactome_ID
                            {"wildcard": {"Reactome_ID": f"*{search_text}*"}},
                            # Reactome_pathway
                            {"wildcard": {"Reactome_pathway": f"*{search_text}*"}},
                            # Alias
                            {"wildcard": {"Alias": f"{search_text}*"}},
                        ],
                    }
                },
                "_source": ["ID", "Gene_symbol", "Gene_description"]
            }
            if not count:
                return count, []

        results = es.search(index=index_name, body=query_dsl,
                            filter_path=["hits.hits._source", "hits.total"], doc_type="doc", size=count)

        result_data = [result["_source"] for result in results["hits"]["hits"]]
        # 排序
        result_data = sorted(result_data,
                             key=lambda x: ((1 - get_equal_rate_1(x['Gene_symbol'], keyword.upper())), x['Gene_symbol'])
                             )

        result_data = result_data[(page_no - 1) * page_size:(page_no * page_size)]
        # result_data.sort()

    else:
        count = 0
        result_data = []
    return count, result_data


keyword = "sox"
index_name = "biodemo1_gene_1"
page_no = 1
page_size = 10
# count, result_data = search_for_gene(keyword, index_name, page_no, page_size)
# print(count)
# print(json.dumps(result_data, indent=2, ensure_ascii=False))

match_all = {
    "query": {
        "match_all": {}
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}
results = es.search(index=index_name, body=match_all,
                    filter_path=["hits.hits._source", "hits.total"], doc_type="doc")

print(json.dumps(results, indent=2, ensure_ascii=False))
