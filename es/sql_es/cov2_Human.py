import json

import pandas as pd
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18128")

mapping = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,
        "analysis": {
            "normalizer": {
                "my_lowercase": {
                    "type": "custom",
                    "filter": ["lowercase"]
                }
            }
        }
    },
    "mappings": {
        "doc": {
            "properties": {
                "ID": {
                    "type": "text"
                },
                "Gene_ID": {
                    "type": "keyword",
                    "normalizer": "my_lowercase"
                },
                "Gene_symbol": {
                    "type": "keyword",
                    "normalizer": "my_lowercase"
                },
                "Entrez_ID": {
                    "type": "keyword",
                    "normalizer": "my_lowercase"
                },
                "Gene_description": {
                    "type": "keyword",
                    "normalizer": "my_lowercase"
                },
                "GO_term_name": {
                    "type": "keyword",
                    "normalizer": "my_lowercase"
                },
                "GO_term_accession": {
                    "type": "keyword",
                    "normalizer": "my_lowercase"
                },
                "Reactome_ID": {
                    "type": "keyword",
                    "normalizer": "my_lowercase"
                },
                "Reactome_pathway": {
                    "type": "keyword",
                    "normalizer": "my_lowercase"
                },
            }
        }
    }
}


def index(index_name):
    if es.indices.exists(index=index_name):
        es.indices.delete(index=index_name, ignore=[400, 404])

    es.indices.create(index=index_name, ignore=400, body=mapping)

    data = pd.read_csv("human.anno.withalias.txt", sep="\t", index_col=None, header=0)

    # 添加_index列 不加 插入数据时 会报错
    data["_index"] = index_name

    # 添加_type列  不加 插入数据时 会报错
    data["_type"] = "doc"

    row, col = data.shape
    _id = range(1, row + 1)
    data["_id"] = _id
    data.insert(0, 'ID', _id)

    # 填充 缺失值
    data = data.fillna("None")

    dict_in_list_object = data.to_dict("records")

    # print(dict_in_list_object)

    bulk(es, dict_in_list_object)


# Invalid index name [Millet], must be lowercase 索引名必须小写 否则会报错
index_name = "cov2_gene_1"

# index(index_name=index_name)
# print("ok")
# quit()
#
match_all = {
    "query": {
        "match_all": {}
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}

match = {
    "query": {
        "wildcard": {
            "mol_name": "(1'R,5Z)-3-(1'-acetoxybutyl)-4-bromo-5-(brmomethylene)furan-2(5H)-one"
        }
    }
}

CMNPD21 = {
    "query": {
        "match": {
            "compound_names_cid": "CMNPD21"
        }
    }
}

wildcard = {
    "query": {
        "wildcard": {
            # "mol_name": "3-[(R)-1-Acetoxybutyl]-4-bromo-5-(bromomethylene)-2(5H)-furanone"
            "mol_name": "(1'R,5Z)-3-(1'-acetoxybutyl)-4-bromo-5-(brmomethylene)furan-2(5H)-one*"
            # "mol_name": "\\(1'r,5z\\)\\-3\\-\\(1'\\-acetoxybutyl\\)\\-4\\-bromo\\-5\\-\\(brmomethylene\\)furan\\-2\\(5h\\)\\-one*"
        }
    }
}
search_text = "source"
# Gene stable ID	Gene name	NCBI gene ID	Gene description
# GO term name	GO term accession	Reactome ID	Reactome pathway
query_dsl = {
    "query": {
        "bool": {
            "should": [
                # compound_names_cid Cid
                {"wildcard": {"Gene_ID": f"{search_text}*"}},

                # compound_properties Smiles
                {"wildcard": {"Gene_symbol": f"*{search_text}*"}},

                # compound_names Name mol_name
                {"wildcard": {"Entrez_ID": f"{search_text}*"}},

                # CAS
                {"wildcard": {"Gene_description": f"{search_text}*"}},

                # chembl_targets Tid
                {"wildcard": {"GO_term_name": f"{search_text}*"}},
                # {"match": {"target_name": search_text}},

                # docs Did
                {"wildcard": {"GO_term_accession": f"{search_text}*"}},

                # taxonomy Oid
                {"wildcard": {"Reactome_ID": f"{search_text}*"}},

                # taxonomy Oid
                {"wildcard": {"Reactome_pathway": f"{search_text}*"}},
            ],
        }
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    },
    "_source": ["ID", "Gene_symbol", "Gene_description"]
}

# filter_path=['hits.hits._score', "hits.hits._source"]
results = es.search(index=index_name, body=query_dsl, filter_path=["hits.hits._source", "hits.total"],
                    doc_type="doc", size=30)
query_dsl2 = {
    "query": {
        "bool": {
            "should": [
                # compound_names_cid Cid
                {"wildcard": {"Gene_ID": f"{search_text}*"}},

                # compound_properties Smiles
                {"wildcard": {"Gene_symbol": f"*{search_text}*"}},

                # compound_names Name mol_name
                {"wildcard": {"Entrez_ID": f"{search_text}*"}},

                # CAS
                {"wildcard": {"Gene_description": f"{search_text}*"}},

                # chembl_targets Tid
                {"wildcard": {"GO_term_name": f"{search_text}*"}},
                # {"match": {"target_name": search_text}},

                # docs Did
                {"wildcard": {"GO_term_accession": f"{search_text}*"}},

                # taxonomy Oid
                {"wildcard": {"Reactome_ID": f"{search_text}*"}},

                # taxonomy Oid
                {"wildcard": {"Reactome_pathway": f"{search_text}*"}},
            ],
        }
    }
}
# count = es.count(index=index_name, body=query_dsl2)["count"]
# data = [result["_source"] for result in results["hits"]["hits"]]
# print(data)
result = es.search(index=index_name, body=query_dsl, _source=['compound_names_cid', 'mol_name'])

print(json.dumps(results, indent=2, ensure_ascii=False))
# print(count)
