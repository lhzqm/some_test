import pandas as pd

# data = pd.read_csv("./compound_names.txt", sep="\t", index_col=None, header=0)
# data = pd.read_csv("./cas.txt", sep="\t", index_col=None, header=0)
# data = pd.read_csv("./chembl_targets.txt", sep="\t", index_col=None, header=0)
# data = pd.read_csv("taxonomy.txt", sep="\t", index_col=None, header=0)
data = pd.read_csv("docs.txt", sep="\t", index_col=None, header=0, low_memory=False)

print(data.shape)

row, col = data.shape
nlist = range(1, row + 1)
# data["_id"] = nlist
data["did_value"] = data["did"]

# data["_index"] = "compound_names_CID"
# data["_type"] = "compound_names_CID_Doc"

# data = data[["cid", "cid_value", "_index", "_type", "_id"]]
data = data[["did", "did_value"]]

data.to_csv("docs.tsv", sep="\t", index=None)
