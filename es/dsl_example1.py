# 导入包
import json
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q, A

es = Elasticsearch("47.92.159.134:18129")

s = Search(using=es, index="bank")  # using: 指定es 引擎  index:指定索引

response = s.params(size=3).filter("match_all").sort("_id").execute().to_dict()

# 根据字段查询，可以多个查询条件叠加
res_2 = s.params(size=3).query("match", gender="F").query("match", age="32").execute().to_dict()

# 用Q()对象查询多个对象，在多个字段中，fields是一个列表，可以存放多个field，
# query为所要查询的值，如果要查询多个值可以用空格隔开（似乎查询的时候Q对象只接受同种类型的数据，如
# 果文本和数字混杂在一块就会报错，建立查询语句出错，有待考察，如query="Amber 11"就会失败，
# fields也是一样，另外query可以接受单个数字的查询，如果是多个同样会报相同的错误）
# Q()第一个参数是查询方法，具体用法及其他方法可以参考elasticsearch的官方文档

q = Q("multi_match", query="Amber Hattie", fields=["firstname"])
res_3 = s.query(q).execute().to_dict()

# 搜索，q是指定搜索内容，可以看到空格对q查询结果没有影响，
# size指定个数，from_指定起始位置，
# q用空格隔开可以多个查询也可以限定返回结果的字段
# ，filter_path可以指定需要显示的数据，如本例中显示在最后的结果中的只有_id和_type

res_4 = es.search(index="bank", q="Holmes", size=3, from_=0)
res_5 = es.search(index="bank", q=" 39225", size=5)
res_6 = es.search(index="bank", q=" 39225", size=5)

# 直接执行Search()会默认搜索所有数据
# res_7 = s.execute()

# 查询，match指定操作方法，country="all"，指定查询值，country为要查询的值所在的field，
# execute()为执行以上操作
res = s.query("match", balance="20278").execute().to_dict()
# 过滤，在此为范围过滤，range是方法，
# timestamp是所要查询的field的名字，
# gte意为大于等于，lt意为小于，根据需要设定即可(似乎过滤只能接受数字形式的内容，如果是文本就会返回空)
# 关于term和match的区别，term是精确匹配，match会模糊化，会进行分词，返回匹配度分数，
# （term查询字符串之接受小写字母，如果有大写会返回空即没有命中，match则是不区分大小写都可以进行查询，返回结果也一样）

# 实例1: 范围查询 .query("match", email="let").execute().to_dict()
res8 = s.filter("range", account_number={"gte": 900, "lt": 1000}).execute().to_dict()
# 实例2: 普通过滤
res9 = s.filter("terms", balance=["20278", "16869"]).execute().to_dict()

# 聚合，聚合可以放在查询，过滤等操作的后面叠加，需要加aggs，bucket即为分组，
# 其中第一个参数是分组的名字，自己指定即可，
# 第二个参数是方法，第三个是指定的field，metric也是同样，metric的方法有sum、avg、max、min等等，
# 但是需要指出的是有两个方法可以一次性返回这些值，
# stats和extended_stats，后者还可以返回方差等值，很方便，此过程可能会出现一些错误，具体见本文最后相关bug

# 实例1
s.aggs.bucket("per_country", "terms", field="timestamp").metric("sum_click", "stats", field="click").metric(
    "sum_request", "stats", field="request")

# 实例2
s.aggs.bucket("per_age", "terms", field="click.keyword").metric("sum_click", "stats", field="click")

# 实例3
s.aggs.metric("sum_age", "extended_stats", field="impression")

# 实例4
s.aggs.bucket("per_age", "terms", field="country.keyword")
# 最后依然是要execute，此处注意s.aggs......的操作不能用变量接收（如res=s.aggs......的操作就是错误的），聚合的结果会在res中显示

# 实例5
a = A("range", field="account_number", ranges=[{"to": 900}, {"from": 11, "to": 21}])
# 此聚合是根据区间进行聚合
res10 = s.execute().to_dict()
res11 = a.to_dict()

# 3.es索引的方式

# 可能有时候你会发现一个问题，用term或terms进行查询文本信息的时候竟然没有返回任何数据，例如：需要查询elasticsearch - dsl，Elasticseach，
# elasticsearch
# client这类文本信息，结果都是查不到的，原因不在于term方法，而是因为es建立索引的方式，
# es默认建立索引的时候会是analyzed，这个设置会对写入es的数据进行分词并全部转换成小写，这样做是为了方便进行全文搜索，
# 如elasticsearch - dsl存储的时候是elasticsearch和dsl分开的，但有时候我们需要进行精确查询，这个守候需要设置索引为not_analyzed，
# 此设置下不会进行分词处理，但显然不利于全文搜索，
# 查到有人的解决方法是，设置两个存储相同内容的field，一个设置analyzed，另一个设置not_analyzed，
# 一个用来精确查询，另一个用来全文搜索。但是也可以为每个字段设置两个index，一个是text类型，一个keyword类型。
# 另外如果对数据内容没有太大要求的时候，可以再写入es之前对数据进行处理，过滤掉“-“、空格等非单词字符，
# 如Elasticsearch - dsl
# client直接存储成Elasticsearchdslclient
#
# 在elasticsearch - dsl中没有找到设置not_analyzed的接口，原因是在elasticsearch - dsl只需要设置类型为keyword即可，
# 在此还有一个高级功能，一个field也可以设置多个类型，代码如下：

# es = Elasticsearch(hosts=es_hosts)
# m = Mapping(your_type)
# m.field("country", "keyword")
# m.field("province", "keyword")
# m.field("phone_brand", "keyword", field={"use_for_search": Text() ** 1 **})
# m.field("phone_model", "keyword")
# m.save(es_index, using=es)
#
# i = Index(_es_index, using=es)
# print(i.get_mapping())
#
# ** 1 ** 处的类型需要从elasticsearch_dsl导入，其他类型可以自行查找。
# 多个类型怎么用呢？指定field的同时指定类型即可，
# 例如我想对phone_brand进行全文搜索，这个时候就不希望精确查询了，phone_brand换成phone_brand.use_for_search即可，前面的例子中keyword就是这个原因。


# print(response)
print(json.dumps(res11, indent=2, ensure_ascii=False))
