import time

import pandas as pd
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch

import json

es = Elasticsearch("47.92.159.134:18129")

mapping = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,
    }
}

# result = es.indices.create(index='cas', ignore=400, body=mapping)
# print(result)
# quit()
data = pd.read_csv("./cas.txt", sep="\t", index_col=None, header=0)
data["_index"] = "cas"
data["_type"] = "casDoc"
data = data[["cid", "cas_no", "_index", "_type"]]

# 保存文件
data.to_csv("cas.tsv", sep="\t", index=None)

# data = data.head()
# print(data)
cid_smiles_dict_list = data.to_dict("records")
# example = {"cid": "CMNPD1", "smiles": "S1CSSC1"}
# print(cid_smiles_dict_list)

# 添加数据
bulk(es, cid_smiles_dict_list)

dsl_all = {
    "query": {
        "match_all": {}
    }
}

dsl = {
    "query": {
        "bool": {
            "should": [
                {"prefix": {"cid": "c"}},
                {"prefix": {"smiles": "5"}}
            ]
        }
    }
}

# time.sleep(3)
result = es.search(index='cas', body=dsl, filter_path=["hits.hits._source"], size=5)
# print(result)
# print(json.dumps(result, indent=2, ensure_ascii=False))
print(json.dumps(result, indent=2, ensure_ascii=False))
print(es.count(index="compound_properties", doc_type="compoundPropertiesDoc"))
