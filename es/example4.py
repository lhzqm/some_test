import json
from datetime import datetime
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

# ignore 400 cause by IndexAlreadyExistsException when creating an index
# es.indices.delete(index='test-index', ignore=[400, 404])
# es.indices.create(index='test-index', ignore=400)

# ignore 404 and 400
# es.indices.delete(index='test-index', ignore=[400, 404])
datas = [
    {
        'title': '美国留给伊拉克的是个烂摊子吗',
        'url': 'http://view.news.qq.com/zt2011/usa_iraq/index.htm',
        'date': '2011-12-16'
    },
    {
        'title': '公安部：各地校车将享最高路权',
        'url': 'http://www.chinanews.com/gn/2011/12-16/3536077.shtml',
        'date': '2011-12-16'
    },
    {
        'title': '中韩渔警冲突调查：韩警平均每天扣1艘中国渔船',
        'url': 'https://news.qq.com/a/20111216/001044.htm',
        'date': '2011-12-17'
    },
    {
        'title': '中国驻洛杉矶领事馆遭亚裔男子枪击 嫌犯已自首',
        'url': 'http://news.ifeng.com/world/detail_2011_12/16/11372558_0.shtml',
        'date': '2011-12-18'
    }
]

# for data in datas:
#     es.index(index='test-index', body=data)

# search = es.search(index='test-index', filter_path=['hits.hits._*'])
# # search = es.search(index='test-index', filter_path=['hits.hits._id', 'hits.hits._source'])
search2 = es.search(index='test-index')
print(search2)
# print(json.dumps(search2, indent=2, ensure_ascii=False))

# result = es.exists(index="test-index")
# print(result)

info = es.info()
print(info)

print(es.indices.exists(index="test-index"))

result = es.indices.get(index="test-index")
result = es.indices.get_mapping(index="test-index")
result = es.indices.get_settings(index="test-index")
result = es.indices.open(index="test-index")
print(result)