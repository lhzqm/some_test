import time

import pandas as pd

from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch

import json

es = Elasticsearch("47.92.159.134:18129")

mapping = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,
    }
}

# data = pd.read_csv("./compound_names.txt", sep="\t", index_col=None, header=0)
# data["_index"] = "compound_names"
# data["_type"] = "compound_namesDoc"
# data = data[["cid", "name_status", "mol_name", "_index", "_type"]]
#
# # 保存文件
# data.to_csv("compound_names.tsv", sep="\t", index=None)

# data = data.head()
# print(data)
# cid_smiles_dict_list = data.to_dict("records")
# example = {"cid": "CMNPD1", "smiles": "S1CSSC1"}
# print(cid_smiles_dict_list)

# 添加数据
# bulk(es, cid_smiles_dict_list)

dsl_all = {
    "query": {
        "match_all": {}
    }
}

dsl = {
    "query": {
        "bool": {
            "should": [
                {"prefix": {"cid": "cmnpd1"}}
            ]
        }
    }
}

prefix = {
    "query": {
        "prefix": {
            "cid": "cmnpd1"
        }
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}

term = {
    "query": {
        "term": {
            "cid": "cmnpd1"
        }
    }
}
wildcard = {
    "query": {
        "wildcard": {
            "cid": "cmnpd*"
        }
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}

match = {
    "query": {
        "match": {
            "cid": "cmnpd2"
        }
    }
}

match_phrase = {
    "query": {
        "match_phrase": {
            "cid": {
                "query": "cmnpd1",
                "slop": 1
            }
        }
    }
}

match_jd = {
    "query": {
        "match": {
            "cid": {
                "query": "cmnpd1",
                "minimum_should_match": "30%"
            }
        }
    }
}

# compound_names_cid
result = es.search(index='compound_names_cid', body=prefix, filter_path=["hits.hits.*"], size=5)
# print(result)
# print(json.dumps(result, indent=2, ensure_ascii=False))
print(json.dumps(result, indent=2, ensure_ascii=False))
print(es.count(index="compound_names_cid"))

# 查询数据，两种get and search
# get获取
# res = es.get(index="compound_names_cid", doc_type="compound_names_cid_Doc", id=1)
# print(json.dumps(res, indent=2, ensure_ascii=False))
