import difflib
import re

a = "[Source:HGNC Symbol;Acc:HGNC:11189]"

b = re.sub('[\W]', ' ', a)
print(a)
print(b.strip())


def get_equal_rate_1(str1, str2):
    return difflib.SequenceMatcher(None, str1, str2).quick_ratio()


print(get_equal_rate_1("sox", "sox1"))
print(get_equal_rate_1("sox", "sox2"))