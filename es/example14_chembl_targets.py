import time

import pandas as pd

from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch

import json

es = Elasticsearch("47.92.159.134:18129")

mapping = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,
    }
}

# result = es.indices.create(index='chembl_targets', ignore=400, body=mapping)
# print(result)

# data = pd.read_csv("./chembl_targets.txt", sep="\t", index_col=None, header=0)
# data["_index"] = "chembl_targets"
# data["_type"] = "chembl_targetsDoc"
# data = data[["tid", "target_name", "_index", "_type"]]
#
# # 保存文件
# data.to_csv("chembl_targets.tsv", sep="\t", index=None)
#
# # data = data.head()
# # print(data)
# cid_smiles_dict_list = data.to_dict("records")
# example = {"cid": "CMNPD1", "smiles": "S1CSSC1"}
# print(cid_smiles_dict_list)

# 添加数据
# if es.indices.exists(index="chembl_targets"):
# bulk(es, cid_smiles_dict_list)

dsl_all = {
    # "_source": "cid",
    "query": {
        "match_all": {}
    }
}

dsl = {
    "query": {
        "bool": {
            "should": [
                {"prefix": {"tid": "cmnpd1"}},
                {"prefix": {"target_name": "Dihydrofolate"}}
            ]
        }
    }
}

# time.sleep(3)
result = es.search(index='chembl_targets', body=dsl, filter_path=["hits.hits._source"], size=5)
# print(result)
# print(json.dumps(result, indent=2, ensure_ascii=False))
print(json.dumps(result, indent=2, ensure_ascii=False))
print(es.count(index="chembl_targets"))
