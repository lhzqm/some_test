import pandas as pd
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch


def create_index(file_name, field=None, index_name=None):
    """
    create index
    :param file_name: 文件名
    :param field: list 字段名
    :param index_name: str 索引名
    :return:
    """
    if not index_name:
        index_name = file_name.split(".")[0]

    es = Elasticsearch("47.92.159.134:18129")

    # 设置mapping 使集群健康值为绿色
    mapping = {
        "settings": {
            "number_of_shards": 1,
            "number_of_replicas": 0,
        }
    }

    # 创建索引
    if es.indices.exists(index=index_name):
        es.indices.delete(index=index_name, ignore=[400, 404])

    es.indices.create(index=index_name, ignore=400, body=mapping)

    # 读文件
    data = pd.read_csv(file_name, sep="\t", index_col=None, header=0)

    # 添加_index列 不加 插入数据时 会报错
    data["_index"] = index_name

    # 添加_type列  不加 插入数据时 会报错
    # data["_type"] = "{}_Doc".format(index_name)
    data["_type"] = "keyword"

    row, col = data.shape
    _id = range(1, row + 1)
    data["_id"] = _id

    # 如果传给我字段 就用 传给的字段做索引 如果不传就用全字段做索引
    if field:
        # 选择字段创建索引
        field.extend(["_index", "_type", "_id"])
        # 取值
        data = data[field]
    else:
        data = data

    # 填充 缺失值
    data = data.fillna("None")

    # 生成结果文件 检查错误
    data.to_csv("{}.tsv".format(index_name), sep="\t", index=None)
    # 生成字典对象列表
    dict_in_list_object = data.to_dict("records")
    print(dict_in_list_object)
    # quit()

    # 插入数据
    bulk(es, dict_in_list_object)
    return "索引生成完毕!"


print(create_index(file_name="compound_properties.tsv", index_name="zqm_test",
                   field=["compound_properties_cid", "smiles"]))

# print(create_index(file_name="cas.tsv", field=["cas_cid", "cas_no"]))
#
# print(create_index(file_name="compound_names.tsv", field=["compound_names_cid", "mol_name"]))
#
# print(create_index(file_name="chembl_targets.tsv", field=["tid", "tid_value"]))
#
# print(create_index(file_name="taxonomy.tsv", field=["oid", "oid_value"]))
#
# print(create_index(file_name="docs.tsv", field=["did", "did_value"]))  # low_memory=False

# print(create_index(file_name="chemont_dictionary.txt", field=["chemont_id", "chemont_name"]))

# print(create_index(file_name="human.anno.withalias.txt", index_name="human",
#                    field=["Gene_ID", "Gene_Symbol", "Gene_description", "GO_term_name", "Reactome_pathway", "Alias"]))

# print(create_index(file_name="join.tsv", index_name="join"))

# 索引名必须小写
# print(create_index(file_name="compound_names_CID.tsv", index_name="compound_names_cid"))
