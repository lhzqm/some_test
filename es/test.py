import pandas as pd

data = pd.read_csv("compound_properties.tsv", index_col=None, header=0, sep="\t")
# print(data.head())
print(data["smiles"].str.len().min())
# max_title_len = df[yaxis].str.len().max()

data = pd.read_csv("compound_names.tsv", index_col=None, header=0, sep="\t")
# print(data.head())
print(data["mol_name"].str.len().min())
