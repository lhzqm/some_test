import json
from datetime import datetime
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

mapping = {
    "properties": {
        'info': {
            'type': 'text',
        }
    }
}

datas = [
    {
        'Gene_symbol': 'Sox14',
        'Gene_ID': 'ENSG00000168875',
        'Gene_description': 'SRY-box 14 [Source:HGNC Symbol;Acc:HGNC:11193]',
        'ID': '11339',
    },
    {
        'Gene_symbol': 'Qsox2',
        'Gene_ID': 'ENSG00000165661',
        'Gene_description': 'quiescin sulfhydryl oxidase 2 [Source:HGNC Symbol;Acc:HGNC:30249]',
        'ID': '12232',
    },
    {
        'Gene_symbol': 'Sox5',
        'Gene_ID': 'ENSG00000134532',
        'Gene_description': 'SRY-box 5 [Source:HGNC Symbol;Acc:HGNC:11201]',
        'ID': '19090',
    },
    {
        'Gene_symbol': 'Sox1',
        'Gene_ID': 'ENSG00000182968',
        'Gene_description': 'SRY-box 1 [Source:HGNC Symbol;Acc:HGNC:11189]',
        'ID': '60207',
    },
    {
        'Gene_symbol': 'Sox3',
        'Gene_ID': 'ENSG00000134595',
        'Gene_description': 'SRY-box 3 [Source:HGNC Symbol;Acc:HGNC:11199]',
        'ID': '62847',
    }, {
        'Gene_symbol': 'Zox14',
        'Gene_ID': 'BNSG00000168875',
        'Gene_description': 'ARY-box 14 [Source:HGNC Symbol;Acc:HGNC:11193]',
        'ID': '11339',
    },
    {
        'Gene_symbol': 'Asox2',
        'Gene_ID': 'DNSG00000165661',
        'Gene_description': 'Ouiescin sulfhydryl oxidase 2 [Source:HGNC Symbol;Acc:HGNC:30249]',
        'ID': '12232',
    },
    {
        'Gene_symbol': 'Sox5',
        'Gene_ID': 'DNSG00000134532',
        'Gene_description': 'HRY-box 5 [Source:HGNC Symbol;Acc:HGNC:11201]',
        'ID': '19090',
    },
    {
        'Gene_symbol': 'Xox1',
        'Gene_ID': 'ENSG00000182968',
        'Gene_description': 'SRY-box 1 [Source:HGNC Symbol;Acc:HGNC:11189]',
        'ID': '60207',
    },
    {
        'Gene_symbol': 'Qox3',
        'Gene_ID': 'HNSG00000134595',
        'Gene_description': 'HRY-box 3 [Source:HGNC Symbol;Acc:HGNC:11199]',
        'ID': '62847',
    },
    {
        'Gene_symbol': 'Hox3',
        'Gene_ID': 'HNSG00000134595',
        'Gene_description': 'HRY-box 3 [Source:HGNC Symbol;Acc:HGNC:11199]',
        'ID': '62847',
    },

]

dsl = {
    'query': {
        'prefix': {
            "Gene_symbol": "s"
        }
    }
}

dsl2 = {
    "query": {
        "bool": {
            # "must": {"match": {"title": "quick"}},
            # "must_not": {"match": {"title": "lazy"}},
            "should": [
                {"prefix": {"Gene_symbol": "h"}},
                {"prefix": {"Gene_description": "q"}}
            ]
        }
    }
}

result = es.search(index='z_test', body=dsl, filter_path=['hits.hits._score', "hits.hits._source"], size=5)
print(json.dumps(result, indent=2, ensure_ascii=False))
print("*" * 10)
result2 = es.search(index='z_test', body=dsl2, filter_path=['hits.hits._score', "hits.hits._source"], size=5)
print(json.dumps(result2, indent=2, ensure_ascii=False))
