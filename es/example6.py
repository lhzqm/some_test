import json
from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk

es = Elasticsearch("47.92.159.134:18129")

mapping = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,
    },
    "properties": {
        'ID': {
            'type': 'integer',
        },
        'Gene_ID': {
            'type': 'text',
        },
        'Gene_description': {
            'type': 'text',
        },
        'Gene_symbol': {
            'type': 'text',
        },
    }
}
# es.indices.delete(index='zqm_test', ignore=[400, 404])
# quit()
# result = es.indices.create(index='zqm_test', ignore=400, body=mapping)
# result = es.indices.put_mapping(index='zqm_test', doc_type='zqm_doc', body=mapping)
# print(result)
# quit()

datas = [
    {
        'Gene_symbol': 'Sox14',
        'Gene_ID': 'ENSG00000168875',
        'Gene_description': 'SRY-box 14 [Source:HGNC Symbol;Acc:HGNC:11193]',
        'ID': '11339',
    },
    {
        'Gene_symbol': 'Qsox2',
        'Gene_ID': 'ENSG00000165661',
        'Gene_description': 'quiescin sulfhydryl oxidase 2 [Source:HGNC Symbol;Acc:HGNC:30249]',
        'ID': '12232',
    },
    {
        'Gene_symbol': 'Sox5',
        'Gene_ID': 'ENSG00000134532',
        'Gene_description': 'SRY-box 5 [Source:HGNC Symbol;Acc:HGNC:11201]',
        'ID': '19090',
    },
    {
        'Gene_symbol': 'Sox1',
        'Gene_ID': 'ENSG00000182968',
        'Gene_description': 'SRY-box 1 [Source:HGNC Symbol;Acc:HGNC:11189]',
        'ID': '60207',
    },
    {
        'Gene_symbol': 'Sox3',
        'Gene_ID': 'ENSG00000134595',
        'Gene_description': ' 3 [Source:HGNC Symbol;Acc:HGNC:11199]',
        'ID': '62847',
    },

]

# for data in datas:
#     es.index(index='zqm_test', doc_type='politics', body=data)
#
# result = es.search(index='zqm_test', filter_path=['hits.hits._source'])
#
# print(json.dumps(result, indent=2, ensure_ascii=False))

dsl = {
    'query': {
        'wildcard': {
            "Gene_description": "s*",
            # "fields": ["Gene_description", "Gene_symbol"]
        }
    },

}

#
result = es.search(index='zqm_test', doc_type='politics', body=dsl,
                   filter_path=['hits.hits._source', "hits.hits._score"])

print(json.dumps(result, indent=2, ensure_ascii=False))


def gendata():
    Gene_symbol = ["1", "2", "3"]
    Gene_ID = ["1", "2", "3"]
    Gene_description = ["1", "2", "3"]
    ID = [1, 2, 3]

    for i in range(len(Gene_symbol)):
        yield {
            "_index": "zqm_test",
            "_type": "politics",
            "politics": {
                'Gene_symbol': Gene_symbol[i],
                'Gene_ID': Gene_ID[i],
                'Gene_description': Gene_description[i],
                'ID': ID[i],
            },
        }

# bulk(es, gendata())
