import json
from datetime import datetime
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

dsl_all = {
    "query": {
        "match_all": {}
    }
}
dsl_prefix = {
    "query": {
        "prefix": {"cid": "cmnpd1"},
    }
}
dsl = {
    "query": {
        "bool": {
            "should": [
                {"prefix": {"cid": "cmnpd1"}},
                {"prefix": {"smiles": "c1c"}}
            ]
        }
    }
}

result = es.search(index='compound_properties', body=dsl_prefix, filter_path=["hits.hits._source"], size=5)
print(result)
print(result["hits"]["hits"])
print(json.dumps(result, indent=2, ensure_ascii=False))

# 获取数据量
print(es.count(index="compound_properties", doc_type="compoundPropertiesDoc"))
