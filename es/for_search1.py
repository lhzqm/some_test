import json

from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")


def for_search(search_text: str):
    # 检索词
    search_text = search_text.lower()
    # 设置检索词
    query_dsl = {
        "query": {
            "bool": {
                "should": [
                    # compound_properties
                    {"match": {"cid": search_text}},
                    {"match": {"smiles": search_text}},

                    # compound_names
                    # {"match": {"cid": search_text}},
                    {"match": {"mol_name": search_text}},

                    # cas
                    # {"match": {"cid": search_text}},
                    {"match": {"cas_no": search_text}},

                    # chembl_targets
                    {"match": {"tid": search_text}},
                    {"match": {"target_name": search_text}},
                    # chemont_dictionary
                    {"match": {"chemont_id": search_text}},
                    {"match": {"chemont_name": search_text}},
                    # docs
                    {"match": {"did": search_text}},
                    # taxonomy
                    {"match": {"oid": search_text}},
                    {"match": {"o_name": search_text}},
                ]
            }
        }
    }
    query_dsl = {
        "query": {
            "bool": {
                "should": [
                    # compound_properties
                    {"prefix": {"cid": search_text}},
                    {"prefix": {"smiles": search_text}},
                    # cas
                    # {"prefix": {"cid": search_text}},
                    {"prefix": {"cas_no": search_text}},
                    # chembl_targets
                    {"prefix": {"tid": search_text}},
                    {"prefix": {"target_name": search_text}},
                    # chemont_dictionary
                    {"prefix": {"chemont_id": search_text}},
                    {"prefix": {"chemont_name": search_text}},
                    # compound_names
                    # {"prefix": {"cid": search_text}},
                    {"match": {"mol_name": search_text}},
                    # docs
                    {"prefix": {"did": search_text}},
                    # taxonomy
                    {"prefix": {"oid": search_text}},
                    {"prefix": {"o_name": search_text}},
                ]
            }
        }
    }
    # 构建 索引列表
    index_list = ["compound_properties", "cas", "chembl_targets", "compound_names", "docs", "taxonomy"]
    # index_list = ["compound_names"]
    # 构建返回结果列表
    ret_result = list()

    for index in index_list:
        es_result = es.search(index=index, body=query_dsl, size=50)
        if es_result:
            search_result = dict()
            search_result["title"] = index
            # search_result["data"] = [i["_source"] for i in es_result["hits"]["hits"]]
            search_result["data"] = es_result
            if search_result["data"]:
                ret_result.append(search_result)

    return ret_result


result = for_search("C1C(=C(C)C[C@@](C)(\C=C\Cl)[C@@H]1Br)Br")

# result = for_search("cmnpd1")

print(json.dumps(result, indent=2, ensure_ascii=False))
