import difflib
import json
import operator

from elasticsearch import Elasticsearch


def get_equal_rate_1(str1, str2):
    return difflib.SequenceMatcher(None, str1, str2).quick_ratio()


# def search_result_function(search_text):
#     # 连接 es
#     es = Elasticsearch("47.92.159.134:18129")
#     # 设置检索 dsl
#     query_dsl = {
#         "query": {
#             "bool": {
#                 "should": [
#                     # compound_names_cid Cid
#                     {"wildcard": {"cid": f"{search_text}*"}},
#
#                     # compound_properties Smiles
#                     {"wildcard": {"smiles": f"{search_text}*"}},
#
#                     # compound_names Name
#                     {"wildcard": {"mol_name": f"{search_text}*"}},
#
#                     # CAS
#                     {"wildcard": {"cas_no": f"{search_text}*"}},
#
#                     # chembl_targets Tid
#                     {"wildcard": {"tid": f"{search_text}*"}},
#                     # {"match": {"target_name": search_text}},
#
#                     # docs Did
#                     {"wildcard": {"did": f"{search_text}*"}},
#
#                     # taxonomy Oid
#                     {"wildcard": {"oid": f"{search_text}*"}},
#                     # {"match": {"o_name": search_text}},
#                 ],
#             }
#         },
#         "sort": {
#             "_id": {  # 根据age字段升序排序
#                 "order": "asc"  # asc升序，desc降序
#             }
#         }
#     }
#     # 构建 索引列表 chemont_dictionary 暂时不考虑 "compound_properties", "cas",
#     # 化合物名称 化合物属性 cas ChEMBL靶点 docs 物种
#     index_list = ["compound_names", "compound_properties", "cas", "compound_names_cid",
#                   "chembl_targets", "docs", "taxonomy"]
#
#     index_dict = {
#         "compound_properties": "compounds",
#         "cas": "compounds",
#         "compound_names": "compounds",
#         "compound_names_cid": "compounds",
#
#         "docs": "documents",
#         "taxonomy": "organisms",
#         "chembl_targets": "targets",
#     }
#
#     # 构建返回结果列表
#     ret_results = list()
#
#     for index in index_list:
#         es_result = es.search(index=index, body=query_dsl,
#                               filter_path=["hits.hits.*"], size=5)
#         if es_result:
#             search_result = dict()
#             search_result["label"] = index_dict[index]
#             search_result["index"] = index
#
#             search_result["options"] = [
#                 {
#                     "label": i["_source"][list(i["_source"].keys())[1]],
#                     "value": i["_source"][list(i["_source"].keys())[0]]
#                 }
#
#                 for i in es_result["hits"]["hits"]
#             ]
#             if search_result["options"]:
#                 ret_results.append(search_result)
#
#     ret_result = sorted(ret_results, key=operator.itemgetter('label'), reverse=False)
#     if not ret_result:
#         # 设置检索 dsl
#         query_dsl = {
#             "query": {
#                 "bool": {
#                     "should": [
#                         # compound_properties Smiles
#                         {"match": {"smiles": f"{search_text}*"}},
#
#                         # compound_names Name
#                         {"match": {"mol_name": f"{search_text}*"}},
#
#                         # CAS
#                         {"match": {"cas_no": f"{search_text}*"}},
#                     ],
#                 }
#             },
#             "sort": {
#                 "_id": {  # 根据age字段升序排序
#                     "order": "asc"  # asc升序，desc降序
#                 }
#             }
#         }
#
#         # 构建 索引列表
#         index_list = ["compound_names", "compound_properties", "cas"]
#         for index in index_list:
#             es_result = es.search(index=index, body=query_dsl,
#                                   filter_path=["hits.hits._source", "hits.hits._score"], size=3)
#             if es_result:
#                 search_result = dict()
#                 search_result["label"] = index_dict[index]
#                 search_result["options"] = [
#                     {
#                         "label": i["_source"][list(i["_source"].keys())[1]],
#                         "value": i["_source"][list(i["_source"].keys())[0]]
#                     }
#                     for i in es_result["hits"]["hits"]
#                     if get_equal_rate_1(search_text, i["_source"][list(i["_source"].keys())[1]].lower()) > 0.8
#                 ]
#
#                 if search_result["options"]:
#                     ret_result.append(search_result)
#
#     if not ret_result:
#         for index in index_list:
#             es_result = es.search(index=index, q=search_text, filter_path=["hits.hits.*"], size=5)
#             if es_result:
#                 search_result = dict()
#                 search_result["label"] = index_dict[index]
#                 search_result["options"] = [
#                     {
#                         "label": i["_source"][list(i["_source"].keys())[1]],
#                         "value": i["_source"][list(i["_source"].keys())[0]]
#                     }
#                     for i in es_result["hits"]["hits"]
#                     if get_equal_rate_1(search_text, i["_source"][list(i["_source"].keys())[1]].lower()) > 0.7
#                 ]
#
#                 if search_result["options"]:
#                     ret_result.append(search_result)
#
#     return ret_result

def needs_escaping(character):
    escape_chars = {
        '\\': True, '+': True, '-': True, '!': True,
        '(': True, ')': True, ':': True, '^': True,
        '[': True, ']': True, '\"': True, '{': True,
        '}': True, '~': True, '*': True, '?': True,
        '|': True, '&': True, '/': True
    }
    return escape_chars.get(character, False)


def deal_with_search_text(search_text):
    sanitized = ''
    for character in search_text:
        if needs_escaping(character):
            sanitized += '\\{}'.format(character)
        else:
            sanitized += character
    return sanitized


def search_result_function(search_text):
    # search_text = deal_with_search_text(search_text)

    score = 0.65
    # 连接 es
    es = Elasticsearch("47.92.159.134:18129")
    # 设置检索 dsl
    query_dsl = {
        "query": {
            "bool": {
                "should": [
                    # compound_names_cid Cid
                    {"wildcard": {"cid": f"{search_text}*"}},

                    # compound_properties Smiles
                    # {"wildcard": {"smiles": f"{search_text}*"}},

                    # compound_names Name mol_name
                    {"wildcard": {"search_mol_name": f"{search_text}*"}},

                    # CAS
                    {"wildcard": {"cas_no": f"{search_text}*"}},

                    # chembl_targets Tid
                    {"wildcard": {"tid": f"{search_text}*"}},
                    # {"match": {"target_name": search_text}},

                    # docs Did
                    {"wildcard": {"did": f"{search_text}*"}},

                    # taxonomy Oid
                    {"wildcard": {"oid": f"{search_text}*"}},
                ],
            }
        },
        "sort": {
            "_id": {  # 根据age字段升序排序
                "order": "asc"  # asc升序，desc降序
            }
        }
    }

    # 构建 索引列表 chemont_dictionary 暂时不考虑 "compound_properties", "cas",
    # 化合物名称 化合物属性 cas ChEMBL靶点 docs 物种
    # index_list = ["compound_names", "compound_properties", "cas", "compound_names_cid", "new_compound_names",
    #               "chembl_targets", "docs", "taxonomy"]
    index_list = ["new_compound_names", "compound_properties", "cas", "compound_names_cid",
                  "chembl_targets", "docs", "taxonomy"]
    index_dict = {
        "compound_properties": "compound",
        "cas": "compound",
        "compound_names": "compound",
        "compound_names_cid": "compound",
        "new_compound_names": "compound",
        "new_compound_properties": "compound",

        "docs": "document",
        "taxonomy": "organism",
        "chembl_targets": "target",
    }

    # 构建返回结果列表
    ret_results = list()

    for index in index_list:
        es_result = es.search(index=index, body=query_dsl, filter_path=["hits.hits.*"], size=5)
        if es_result:
            search_result = dict()
            search_result["label"] = index_dict[index]
            search_result["index"] = index
            search_result["options"] = [
                {
                    "label": i["_source"][list(i["_source"].keys())[1]],
                    "value": i["_source"][list(i["_source"].keys())[0]]
                }
                if len(i["_source"].keys()) == 2
                else
                {
                    "label": i["_source"][list(i["_source"].keys())[0]],
                    "value": i["_source"][list(i["_source"].keys())[2]]
                }
                for i in es_result["hits"]["hits"]

            ]
            if search_result["options"]:
                ret_results.append(search_result)

    # ret_result = sorted(ret_results, key=operator.itemgetter('label'), reverse=False)
    msg = 'Search success! - 1'
    ret_result = ret_results

    if not ret_result:
        search_text = deal_with_search_text(search_text)

        # 设置检索 dsl
        query_dsl = {
            "query": {
                "bool": {
                    "should": [
                        # compound_properties Smiles
                        {"wildcard": {"search_smiles": f"{search_text}*"}}
                    ],
                }
            },
            "sort": {
                "_id": {  # 根据age字段升序排序
                    "order": "asc"  # asc升序，desc降序
                }
            }
        }

        # 构建 索引列表
        index_list = ["new_compound_properties"]
        for index in index_list:
            es_result = es.search(index=index, body=query_dsl,
                                  filter_path=["hits.hits._source", "hits.hits._score"], size=5)

            if es_result:
                search_result = dict()
                search_result["label"] = index_dict[index]

                search_result["options"] = [
                    {
                        "label": i["_source"][list(i["_source"].keys())[1]],
                        "value": i["_source"][list(i["_source"].keys())[0]]
                    }
                    if len(i["_source"].keys()) == 2
                    else
                    {
                        "label": i["_source"][list(i["_source"].keys())[0]],
                        "value": i["_source"][list(i["_source"].keys())[2]]
                    }
                    for i in es_result["hits"]["hits"]
                    # if get_equal_rate_1(search_text, i["_source"][list(i["_source"].keys())[1]].lower()) > 0.1
                ]

                # search_result["options"] = [
                #     {
                #         "label": i["_source"][list(i["_source"].keys())[1]],
                #         "value": i["_source"][list(i["_source"].keys())[0]]
                #     }
                #     for i in es_result["hits"]["hits"]
                #     if get_equal_rate_1(search_text, i["_source"][list(i["_source"].keys())[1]].lower()) > score
                # ]

                if search_result["options"]:
                    ret_result.append(search_result)
        msg = 'Search success! - 2'

    # if not ret_result:
    #     for index in index_list:
    #         es_result = es.search(index=index, q=search_text, filter_path=["hits.hits.*"], size=5)
    #         if es_result:
    #             search_result = dict()
    #             search_result["label"] = index_dict[index]
    #             search_result["options"] = [
    #                 {
    #                     "label": i["_source"][list(i["_source"].keys())[1]],
    #                     "value": i["_source"][list(i["_source"].keys())[0]]
    #                 }
    #                 for i in es_result["hits"]["hits"]
    #                 if get_equal_rate_1(search_text, i["_source"][list(i["_source"].keys())[1]].lower()) > score
    #             ]
    #
    #             if search_result["options"]:
    #                 ret_result.append(search_result)
    #     msg = 'Search success! - 3'

    return ret_result, msg, search_text


# result = search_result_function("Halipeptin A")
# print(json.dumps(result, indent=2, ensure_ascii=False))


# result = search_result_function("CCCCCCCC(OC)C\\C=C\\CCC(N(C\\C(\\Cl)=C\\[C@@](C(=O)C(C)=CC1)(O2)[C@@H]12)C)=O")
# result = search_result_function("cccccccc(oc)c\\c=c\\ccc(n(c\\c(\\cl)=c\\[c@@](c(=o)c(c)=cc1)(o2)[c@@h]12)c)=o")
# result = search_result_function("C1(N(C)C)CSSC1".lower())
# result = search_result_function("CCCCCCCCCCCCCCCCC".lower())
# result = search_result_function(
#     "CCCCCCCC(OC)C\\C=C\\CCC(N(C\\C(\\Cl)=C\\[C@@](C(=O)[C@@H](C)[C@@H](OC(C)=O)C1)(O2)[C@@H]12)C)=O".lower())

# result, msg = search_result_function("1,2,4-Trithiolane".lower())

result, msg, _search_text = search_result_function(
    "6790-63-2".lower())

#
# result, msg, search_text = search_result_function(
#     "CMNPD".lower())

print(json.dumps(result, indent=2, ensure_ascii=False))

print("_search_text:", _search_text)
print("msg:", msg)
# if es_result:
#     search_result = dict()
#     search_result["label"] = index_dict[index]
#     search_result["index"] = index
#
#     search_result["options"] = [
#         {
#             "label": i["_source"][list(i["_source"].keys())[1]],
#             "value": i["_source"][list(i["_source"].keys())[0]]
#         }
#         if len(i["_source"].keys()) > 1
#         else
#         {
#             "label": i["_source"][list(i["_source"].keys())[0]],
#             "value": i["_source"][list(i["_source"].keys())[0]]
#         }
#         for i in es_result["hits"]["hits"]
#     ]
#     if search_result["options"]:
#         ret_results.append(search_result)
