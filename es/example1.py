import json

from elasticsearch import Elasticsearch

# es = Elasticsearch("47.92.159.134:18129")
# result = es.indices.create(index='news', ignore=400)
# print(result)
#
# es = Elasticsearch()
# result = es.indices.delete(index='news', ignore=[400, 404])
# print(result)

# es = Elasticsearch("47.92.159.134:18129")
# es.indices.create(index='news', ignore=400)
# data = {'title': '美国留给伊拉克的是个烂摊子吗', 'url': 'http://view.news.qq.com/zt2011/usa_iraq/index.htm'}
# result = es.create(index='news', doc_type='politics', id=1, body=data)
# print(result)

# result = es.index(index='news', doc_type='politics', body=data)


# es = Elasticsearch("47.92.159.134:18129")
# data = {'title': '美国留给伊拉克的是个烂摊子吗', 'url': 'http://view.news.qq.com/zt2011/usa_iraq/index.htm', "date": "2011-12-16"}
# # result = es.update(index='news', doc_type='politics', body=data, id=1)
# result = es.index(index='news', doc_type='politics', body=data, id=1)
#
# print(result)


es = Elasticsearch("47.92.159.134:18129")
mapping = {
    'properties': {
        'title': {
            'type': 'text',
            # 'analyzer': 'ik_max_word',
            # 'search_analyzer': 'ik_max_word'
        }
    }
}

es.indices.delete(index='news', ignore=[400, 404])
es.indices.create(index='news', ignore=400)
result = es.indices.put_mapping(index='news', doc_type='politics', body=mapping)

datas = [
    {
        'title': '美国留给伊拉克的是个烂摊子吗',
        'url': 'http://view.news.qq.com/zt2011/usa_iraq/index.htm',
        'date': '2011-12-16'
    },
    {
        'title': '公安部：各地校车将享最高路权',
        'url': 'http://www.chinanews.com/gn/2011/12-16/3536077.shtml',
        'date': '2011-12-16'
    },
    {
        'title': '中韩渔警冲突调查：韩警平均每天扣1艘中国渔船',
        'url': 'https://news.qq.com/a/20111216/001044.htm',
        'date': '2011-12-17'
    },
    {
        'title': '中国驻洛杉矶领事馆遭亚裔男子枪击 嫌犯已自首',
        'url': 'http://news.ifeng.com/world/detail_2011_12/16/11372558_0.shtml',
        'date': '2011-12-18'}]

for data in datas:
    es.index(index='news', doc_type='politics', body=data)

# result = es.search(index='news', doc_type='politics')
# print(result)

dsl = {
    'query': {
        'match': {'url': 'http://news.ifeng.com/world'}}
}

# result = es.search(index='news', doc_type='politics', body=dsl)
# print(result)
#
# print(json.dumps(result, indent=4, ensure_ascii=False))
