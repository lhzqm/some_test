import json

from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")


def for_search(search_text: str):
    # 检索词
    search_text = search_text.lower()
    # 设置检索词
    query_dsl = {
        "query": {
            "wildcard": {
                "Gene_Symbol": search_text + "*",
                # Gene_ID	Gene_Symbol	Entrez_ID	Gene_description	GO_term_name
                # GO_term_accession	Reactome_ID	Reactome_pathway	Alias
                # "fields": ["Gene_ID", "Gene_Symbol", "Gene_description", "GO_term_name", "Reactome_pathway", "Alias"]
                # "fields": ["Gene_Symbol"]
            }
        }
    }
    # query_dsl = {
    #     "query": {
    #         "wildcard": {
    #             "query": search_text,
    #             # Gene_ID	Gene_Symbol	Entrez_ID	Gene_description	GO_term_name
    #             # GO_term_accession	Reactome_ID	Reactome_pathway	Alias
    #             "fields": ["Gene_ID", "Gene_Symbol", "Entrez_ID", "Gene_description", "GO_term_name",
    #                        "GO_term_accession", "Reactome_ID", "Reactome_pathway", "Alias"]
    #         }
    #     }
    # }

    # 构建 索引列表
    index_list = ["human"]

    # 构建返回结果列表
    ret_result = list()

    for index in index_list:
        es_result = es.search(index=index, body=query_dsl, size=5)
        if es_result:
            search_result = dict()
            search_result["title"] = index
            search_result["data"] = es_result
            if es_result:
                ret_result.append(search_result)

    return ret_result


result = for_search("sox")

print(json.dumps(result, indent=2, ensure_ascii=False))
