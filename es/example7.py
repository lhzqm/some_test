import json
from datetime import datetime
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

mapping = {
    "properties": {
        'info': {
            'type': 'text',
        }
    }
}

# es.indices.delete(index='z_test', ignore=[400, 404])
#
# es.indices.create(index='z_test', ignore=400)
#
# result = es.indices.put_mapping(index='z_test', doc_type='politics', body=mapping)

# print(result)

datas = [
    {
        'Gene_symbol': 'Sox14',
        'Gene_ID': 'ENSG00000168875',
        'Gene_description': 'SRY-box 14 [Source:HGNC Symbol;Acc:HGNC:11193]',
        'ID': '11339',
    },
    {
        'Gene_symbol': 'Qsox2',
        'Gene_ID': 'ENSG00000165661',
        'Gene_description': 'quiescin sulfhydryl oxidase 2 [Source:HGNC Symbol;Acc:HGNC:30249]',
        'ID': '12232',
    },
    {
        'Gene_symbol': 'Sox5',
        'Gene_ID': 'ENSG00000134532',
        'Gene_description': 'SRY-box 5 [Source:HGNC Symbol;Acc:HGNC:11201]',
        'ID': '19090',
    },
    {
        'Gene_symbol': 'Sox1',
        'Gene_ID': 'ENSG00000182968',
        'Gene_description': 'SRY-box 1 [Source:HGNC Symbol;Acc:HGNC:11189]',
        'ID': '60207',
    },
    {
        'Gene_symbol': 'Sox3',
        'Gene_ID': 'ENSG00000134595',
        'Gene_description': 'SRY-box 3 [Source:HGNC Symbol;Acc:HGNC:11199]',
        'ID': '62847',
    },

]

# for data in datas:
#     es.index(index='z_test', doc_type='politics', body=data)

# result = es.search(index='z_test', doc_type='politics')
# print(json.dumps(result, indent=2, ensure_ascii=False))

# dsl = {
#     'query': {
#         'wildcard': {
#             "Gene_symbol": "sox1*"
#         }
#     }
# }

dsl = {
    'query': {
        'prefix': {
            "Gene_symbol": "s"
        }
    }
}

# dsl = {
#     'query': {
#         'terms': {
#             "Gene_symbol": ["sox1", "sox5"]
#         }
#     },
# }
# dsl = {
#     'query': {
#         'match': {
#             "Gene_symbol": "sox1"
#         }
#     },
# }
result = es.search(index='z_test', body=dsl, filter_path=['hits.hits._score', "hits.hits._source"], size=5)
# result = es.search(index='z_test', body=dsl)
# result2 = es.search(index='z_test', q="s", filter_path=['hits.hits._source'])
print(json.dumps(result, indent=2, ensure_ascii=False))
print("*" * 10)
# print(json.dumps(result2, indent=2, ensure_ascii=False))

# 获取数据量
# print(es.count(index="z_test", doc_type="politics"))
