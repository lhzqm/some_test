import pandas as pd
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk

es = Elasticsearch("47.92.159.134:18129")
# 设置mapping 使集群健康值为绿色
mapping = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,
    }
}
# 创建索引
# result = es.indices.create(index='compound_properties', ignore=400, body=mapping)
# 读文件
data = pd.read_csv("./compound_properties.txt", sep="\t", index_col=None, header=0)
# 添加_index列 不加会报错
data["_index"] = "compound_properties"
# 添加_type列  不加会报错
data["_type"] = "compound_propertiesDoc"
# 选择字段创建索引
data = data[["cid", "smiles", "_index", "_type"]]
# 生成结果文件 检查错误
# data.to_csv("compound_properties.tsv", sep="\t", index=None)

data = data.head()
print(data)
# 生成字典对象列表
cid_smiles_dict_list = data.to_dict("records")

from collections import OrderedDict, defaultdict

dd = defaultdict(list)
li = data.to_dict('records', into=dd)

print(cid_smiles_dict_list)

print(li)

# 插入数据

# bulk(es, cid_smiles_dict_list)
