import json
from datetime import datetime
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

mapping = {
    "properties": {
        'info': {
            'type': 'text',
        }
    }
}

# es.indices.delete(index='z_test', ignore=[400, 404])
#
# es.indices.create(index='z_test', ignore=400)
#
# result = es.indices.put_mapping(index='z_test', doc_type='politics', body=mapping)

# print(result)

datas = [
    {
        'Gene_symbol': 'Zox14',
        'Gene_ID': 'BNSG00000168875',
        'Gene_description': 'ARY-box 14 [Source:HGNC Symbol;Acc:HGNC:11193]',
        'ID': '11339',
    },
    {
        'Gene_symbol': 'Asox2',
        'Gene_ID': 'DNSG00000165661',
        'Gene_description': 'Ouiescin sulfhydryl oxidase 2 [Source:HGNC Symbol;Acc:HGNC:30249]',
        'ID': '12232',
    },
    {
        'Gene_symbol': 'Sox5',
        'Gene_ID': 'DNSG00000134532',
        'Gene_description': 'HRY-box 5 [Source:HGNC Symbol;Acc:HGNC:11201]',
        'ID': '19090',
    },
    {
        'Gene_symbol': 'Xox1',
        'Gene_ID': 'ENSG00000182968',
        'Gene_description': 'SRY-box 1 [Source:HGNC Symbol;Acc:HGNC:11189]',
        'ID': '60207',
    },
    {
        'Gene_symbol': 'Qox3',
        'Gene_ID': 'HNSG00000134595',
        'Gene_description': 'HRY-box 3 [Source:HGNC Symbol;Acc:HGNC:11199]',
        'ID': '62847',
    },
    {
        'Gene_symbol': 'Hox3',
        'Gene_ID': 'HNSG00000134595',
        'Gene_description': 'HRY-box 3 [Source:HGNC Symbol;Acc:HGNC:11199]',
        'ID': '62847',
    },
]
# example = {"cid": "CMNPD1", "smiles": "S1CSSC1"}
# for data in datas:
#     es.index(index='z_test', doc_type='politics', body=data)

match_all = {
    "query": {
        "match_all": {}
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}
dsl = {
    "query": {
        "match": {
            "smiles": "CCCCCCCC(OC)C\\C=C\\CCC(N(C\\C(\\Cl)=C\\[C@@](C(=O)[C@@H](C)[C@@H](OC(C)=O)C1)(O2)[C@@H]12)C)=O"
        }
    }
}
# result = es.search(index='zqm_test', body=match_all, filter_path=['hits.hits._score', "hits.hits._source"])
result = es.search(index='zqm_test',
                   q="C(/Br)(\Br)=C\C(C)Br",
                   filter_path=['hits.hits._score', "hits.hits._source"])

print(json.dumps(result, indent=2, ensure_ascii=False))

# 获取数据量
print(es.count(index="zqm_test", doc_type="doc"))
