from datetime import datetime
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

doc = {
    'author': 'kimchy',
    'text': 'Elasticsearch: cool. bonsai cool.',
    'timestamp': datetime.now(),
}

# res = es.index(index="test-index", id=2, body=doc)

# print(res['result'])

# res = es.get(index="test-index", id=1)

# print(res['_source'])

# es.indices.refresh(index="test-index")

res = es.search(index="test-index", body={"query": {"match_all": {}}})

res2 = es.search(index='test-index', filter_path=['hits.hits._id', 'hits.hits._type'])

res3 = es.search(index='test-index', filter_path=['hits.hits._score', 'hits.hits._source'])

print(res)
print(res2)
print(res3)

print(es.count(index="test-index"))

print(es.exists(index="test-index", id=1, _source=True))

print(es.exists_source(index="test-index", id=1))

# print(es.explain(index="test-index", id=1))

print(es.get(index="test-index", id=1, _source=True, _source_excludes=["text"]))

print(es.get_source(index="test-index", id=1, _source=True))

# print(es.index(index="test-index", id=1, _source=True))

print(es.info())

print(es.ping())

print(es.search())

# print(es.search(index="test-index", q="kimchy", lenient=True))

# print(es.get_script(id=1))

# print("Got %d Hits:" % res['hits']['total'])

# for hit in res['hits']['hits']:
#     print("%(timestamp)s %(author)s: %(text)s" % hit["_source"])

# es.indices.analyze
