from selenium import webdriver


class Douyu(object):
    def __init__(self):
        self.url = 'https://www.douyu.com/directory/all'
        self.driver = webdriver.Chrome()

    def parse_data(self):
        # 获取所有的房间节点列表
        # room_list = self.driver.find_elements_by_xpath('//*[@id="live-list-contentbox"]/li/a')
        room_list = self.driver.find_elements_by_xpath('//*[@id="listAll"]/section[2]/div[2]/ul/li/div/a')
        print(len(room_list))
        data_list = []

        for room in room_list:
            temp = dict()
            temp['title'] = room.find_element_by_xpath('./div/a[1]/div[2]/div[1]/h3').text
            temp['type'] = room.find_element_by_xpath('./div[2]/div[1]/span').text
            temp['owner'] = room.find_element_by_xpath('./div/p/span[1]').text
            temp['num'] = room.find_element_by_xpath('./div/p/span[2]').text
            temp['cover'] = room.find_element_by_xpath('./span/img').get_attribute('src')
            print(temp)
            data_list.append(temp)

    def __del__(self):
        self.driver.close()

    def run(self):
        self.driver.get(self.url)
        self.parse_data()


if __name__ == '__main__':
    douyu = Douyu()
    douyu.run()
