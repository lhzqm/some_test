# coding:utf-8
"""
为什么要用线程运行，还有什么情况下用queue和线程
    提升爬取速度
对爬虫还是挺感兴趣, 但目前市场形式是什么样的, 为了顺利就业平时应该怎么学


1 多线程

    守护线程是否跟随主线程的退出而退出
        殉葬
    如何设置守护线程
        t.setDaemon(True)
    如何使主线程等待子线程结束
        t.join()
    queue的使用使为了什么？
        线程安全
    让主线程等待队列操作完毕再退出
        q.join()


2 动态html
    如何导入selenium
        from selenium  import webdriver

    如何使用selenium
        创建一个浏览器对象
            driver = webdriver.XXX()
            无头方案
                PhantomJS
                chrome
                    opt = webdriver.ChromOption()
                    opt.add_argument('headless')
                    driver = webdriver.Chrome(opt)
        访问
            driver.get(url)

    selenium定位元素
        el = driver.find_element_by_xxx(value)
        driver.find_element(By.xxx, value)
    8种定位方式
        xpath
        id
        name
        partial_link_text
        class_name

        css_selector

    元素的操作
        el.click()
        el.submit()
        el.send_keys(data)
        el.clear()

        el.text
        el.get_attribute(key)
    窗口与iframe框架
        dirver.window_handles
        driver.switch_to.window()

        iframe主要用于登录
            id/el
            driver.switch_to.frame()
    执行js
        js = 'scrollTo(0,500)'
        driver.execute_script(js)
"""
