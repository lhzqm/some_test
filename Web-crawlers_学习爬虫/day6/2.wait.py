# coding:utf-8
from selenium import webdriver

url = 'http://www.baidu.com'

dr = webdriver.Chrome()
# 隐式等待，最大等待指定的时间，单位为s
dr.implicitly_wait(5)
dr.get(url)

el_img = dr.find_element_by_xpath('//*[@id="lg"]/img[200000]')
print(el_img.get_attribute('src'))
