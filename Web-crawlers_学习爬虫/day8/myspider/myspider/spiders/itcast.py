# -*- coding: utf-8 -*-
import scrapy
from myspider.items import MyspiderItem


class ItcastSpider(scrapy.Spider):
    name = 'itcast'
    allowed_domains = ['itcast.cn']
    # 修改起始的url
    start_urls = ['http://www.itcast.cn/channel/teacher.shtml']

    def parse(self, response):
        # 获取所有的教师节点
        node_list = response.xpath('//div[@class="li_txt"]')
        print(len(node_list))

        # data_list = []
        # 遍历教师节点列表
        for node in node_list:
            item = MyspiderItem()

            item['name'] = node.xpath('./h3/text()').extract()[0]
            item['title'] = node.xpath('./h4/text()').extract()[0]
            item['desc'] = node.xpath('./p/text()').extract()[0]
            # 将数据放到数据列表中
            # data_list.append(item)
            yield item

        # # 返回数据
        # return data_list
        # next_url= ''
        # yield next_url

    # def parse_detail(self):
