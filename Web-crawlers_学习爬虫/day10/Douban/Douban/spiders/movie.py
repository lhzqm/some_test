# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from Douban.items import DoubanItem


class MovieSpider(CrawlSpider):
    name = 'movie'
    allowed_domains = ['douban.com']
    # 修改起始的url
    start_urls = ['https://movie.douban.com/top250']

    rules = (
        # 编辑链接提取规则
        Rule(LinkExtractor(allow=r'\?start=\d+&filter='), callback='parse_item', follow=True),
    )

    def parse_item(self, response):
        # print()
        print('----', response.request.headers['User-Agent'])

        movie_list = response.xpath('//*[@id="content"]/div/div[1]/ol/li/div/div[2]')
        # print(len(movie_list))

        # 编列节点列表
        for movie in movie_list:
            # 构建一个数据存储容器
            item = DoubanItem()

            # 从没一个节点中抽取数据
            item['name'] = movie.xpath('./div[1]/a/span[1]/text()').extract_first()
            item['score'] = movie.xpath('./div[2]/div/span[2]/text()').extract_first()
            item['info'] = ''.join([i.strip() for i in movie.xpath('./div[2]/p[1]/text()').extract()]).replace('\xa0',
                                                                                                               '')
            item['desc'] = movie.xpath('./div[2]/p[2]/span/text()').extract_first()

            # 返回数据
            # print(item)
            yield item
