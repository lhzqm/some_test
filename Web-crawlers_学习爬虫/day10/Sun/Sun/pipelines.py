# -*- coding: utf-8 -*-

# Define your item pipelines here

# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json
# 从settings文件中读取配置信息
from scrapy.conf import settings
from pymongo import MongoClient


class SunPipeline(object):
    def open_spider(self, spider):
        self.file = open('dongguan.json', 'w')

    def process_item(self, item, spider):
        str_data = json.dumps(dict(item)) + ',\n'
        self.file.write(str_data)
        return item

    def close_spider(self, spider):
        self.file.close()


class MongoPipeline(object):

    def open_spider(self, spider):
        # 获取数据库配置信息
        host = settings['MONGO_HOST']
        port = settings['MONGO_PORT']
        db = settings['MONGO_DB']
        col = settings['MONGO_COL']
        # print('---', host,port,db,col)

        # 创建数据库的链接
        self.client = MongoClient(host, port)

        self.db = self.client[db]

        self.col = self.db[col]

    def process_item(self, item, spider):
        dict_data = dict(item)
        self.col.insert(dict_data)

        return item

    def close_spider(self, spider):
        self.client.close()
