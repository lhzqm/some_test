# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from Sun.items import SunItem

class DongguanSpider(CrawlSpider):
    name = 'dongguan'
    # 核对允许的域名，并修改
    allowed_domains = ['sun0769.com']
    # 修改起始的url
    start_urls = ['http://wz.sun0769.com/index.php/question/questionType?type=4']

    rules = (
        # 构造用于提取列表页面的规则
        Rule(LinkExtractor(allow=r'questionType'), follow=True),
        #
        Rule(LinkExtractor(allow=r'html/question/\d+/\d+.shtml'), callback='parse_item'),
    )

    def parse_item(self, response):
        # 构建存储数据的容器
        item = SunItem()

        # 抽取数据
        item['title'] = response.xpath('//title/text()').extract_first().split('_')[0]
        item['link'] = response.url
        item['number'] = response.xpath('/html/body/div[6]/div/div[1]/div[1]/strong/text()').extract_first().split(':')[-1].strip()
        item['content'] = response.xpath('//meta[@name="description"]/@content').extract_first().replace('\xa0', '')


        # 返回给引擎
        # yield item
        yield scrapy.Request(url,callback,headers={})


