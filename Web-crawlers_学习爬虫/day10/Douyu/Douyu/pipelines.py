# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.pipelines.images import ImagesPipeline
import scrapy
import os
from scrapy.utils.project import get_project_settings


class DouyuPipeline(object):
    def process_item(self, item, spider):
        return item


class ImagePipeline(ImagesPipeline):
    IMAGES_STORE = get_project_settings().get('IMAGES_STORE')

    # 用于提交图片请求，请求提交之后，下载器自动下载图片
    def get_media_requests(self, item, info):
        # 不需要指定callback
        yield scrapy.Request(item['link'])

    # 下载完毕之后，下载信息将会通过results传到到该方法
    def item_completed(self, results, item, info):
        paths = [data['path'] for ok, data in results if ok]
        # print(paths)

        # 拼接旧文件路径
        old_name = self.IMAGES_STORE + os.sep + paths[0]
        # 拼接新文件路径
        new_name = self.IMAGES_STORE + os.sep + paths[0].split('/')[0] + os.sep + item['nickname'] + '.jpg'
        os.rename(old_name, new_name)

        return item
