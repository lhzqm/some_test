# -*- coding: utf-8 -*-
import scrapy
import json
from Douyu.items import DouyuItem


class DouyuSpider(scrapy.Spider):
    name = 'douyu'
    allowed_domains = ['douyucdn.cn']
    base_url = 'http://capi.douyucdn.cn/api/v1/getVerticalRoom?limit=20&offset='
    offset = 0
    start_urls = [base_url]

    def parse(self, response):
        # 将json数据转换成python字典
        dict_data = json.loads(response.body)
        # print(dict_data)

        # 获取房间列表
        room_list = dict_data['data']

        # 遍历房间列表，从每一个房间信息中抽取数据
        for room in room_list:
            # 创建item实例
            item = DouyuItem()

            # 倒换数据
            item['nickname'] = room['nickname']
            item['uid'] = room['owner_uid']
            item['link'] = room['vertical_src']
            item['city'] = room['anchor_city']

            # 返回
            yield item

        # 翻页处理，判断是否到页尾
        if len(room_list) != 0:
            self.offset += 20
            url = self.base_url + str(self.offset)
            # 创建请求并返回给引擎
            yield scrapy.Request(url, callback=self.parse)
