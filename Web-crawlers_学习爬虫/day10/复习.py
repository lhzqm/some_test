# coding:utf-8
"""
反馈

1.爬虫后续有哪些比较好的发展方向？能否说一下这些方向的工作是怎样的？

复习.py
1 请求与相应
    1.1 请求的分类
        get
        post
    1.2 Request的实例是什么请求
        url
        callback
        headers
        cookies
        dont_filter
        meta    主要用于解析方法之间的数据传递
    1.3 FormRequest的实例是什么请求，比Request多了什么参数
        post
        formdata={}
    1.4 FormRequest.from_response的作用
        填写表单

    1.5 响应的生成位置
        下载器
        下载器中间件

2 crawspider类
    2.1 与spider类爬虫的区别
        导入类

        继承
            scrapy.spider------->crawlspider
        rules
            Rule(LinkExtractor(allow=()),callback,follow,process_links,process_requests)
        不能重写parse
            parse
                return _parse
    2.2 Rule实例参数


    2.3 链接提取器的作用
        自动提取链接

        特点:
            自动补全 url
            可以不跟从 crawlspider

    2.4 follow参数的作用
        决定是否在提取的链接对应的响应中继续应用链接提取器

    2.5 创建crawlspider爬虫的命令
        scrapy genspider -t crawl name domain

        编写rules规则
        编写解析方法
"""
