# coding:utf-8
import requests


class Tieba(object):

    def __init__(self, name, pn):
        self.name = name
        self.base_url = 'https://tieba.baidu.com/f?kw={}&ie=utf-8&pn='.format(name)
        self.url_list = [self.base_url + str(i * 50) for i in range(pn)]
        # print(self.url_list)
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'
        }

    def get_data(self, url):
        res = requests.get(url, headers=self.headers)

        return res.content

    def save_data(self, data, index):
        file_name = self.name + str(index) + '.html'

        with open(file_name, 'wb')as f:
            f.write(data)

    def run(self):
        # 构建url列表
        # 构建请求头
        # 遍历url列表

        for url in self.url_list:
            # 发送请求获取响应
            data = self.get_data(url)
            # 计算url在列表中的索引
            index = self.url_list.index(url)
            # 写文件保存
            self.save_data(data, index)


if __name__ == '__main__':
    tieba = Tieba('帝吧', 4)
    tieba.run()

# 构建url列表

# 构建请求头

# 遍历url列表

# 发送请求获取相应

# 写文件保存
