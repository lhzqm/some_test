"""
#coding:utf-8

# 反馈信息
1.# -*- coding:utf-8 -*-
import requests
url = 'http://www.baidu.com'
response = requests.get(url)
print(response.content.decode()) # 为什么运行界面我输出的response.content.decode()显示的网页代码不换行 而运行下面代码的时候会换行呢

# -*- coding:utf-8 -*-
import requests
url = 'http://www.baidu.com/s'
ins = input('請輸入')
ins = ins.encode('utf-8')
temp = { 'wd':ins }
headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36"}
reponse = requests.get(url, headers=headers, params=temp)
with open('baidu.html', 'w',encoding='utf-8')as f:
  f.write(reponse.content.decode())

#1 什么是爬虫
    模拟客户端，发送网络请求获取对应的相应，并且按照规则自动提取数据的程序

#2 爬虫分类
    通用爬虫
    聚焦爬虫
        累积式     去重
        增量式     新产生的url
                  旧url上更新了数据
        深网

#3 http与https的区别
    ssl
    性能 https慢
    安全 https安全

#4 url结构
    协议
    host
    port
    path
    querystring     ?key=value&key=value1
    锚点      #

#5 dns服务器的功能
    域名 ---- ip

#6 请求与响应
    请求
        User-Agent
        referer
        cookies
    响应
        Last-Modified       静态页面的更新时间

#7 get与post区别
    参数位置
    安全性
    数据量大小

#8 状态码
    任何状态吗都不可信，一切以源码为准
    任何状态吗都不可信,一切以源码为准
#9 字符串转化
    str ----- bytes
        encode()

        decode()

#10 requests的使用
    import requests

    # 发送一个简单的请求
    requests.get(url)

    # 发送带请求头的请求
    headers = {
        'User-Agent': 'sdnxhbjnkasmd'
    }
    requests.get(url, headers=headers)
    # 带参数的请求
    url

    params = {}
    requests.get(url, headers=headers,params=prams)
"""
