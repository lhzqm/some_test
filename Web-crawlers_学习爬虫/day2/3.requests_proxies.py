# coding:utf-8

import requests

url = 'http://www.toutiao.com'

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'
}

# 构建代理
proxies = {
    # 'http': 'http://118.67.218.197:8080',
    'https': 'https://118.67.218.197:8080',
}

# 私人代理
# proxies = {
#     'http': 'http://user:pwd@118.67.218.197:8080',
#     'https': 'https://user:pwd@118.67.218.197:8080',
# }

response = requests.get(url, headers=headers, proxies=proxies)
