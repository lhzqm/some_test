# coding:utf-8
# 导入
from pymongo import MongoClient

# 链接数据库服务器
client = MongoClient('127.0.0.1', 27017)
# 查看数据库链接信息
print(client.address)

# 选择一个数据库
# db = client.python27
db = client['python27']

# 选择集合
# col = db.test
col = db['test']

# for i in range(10):
#     col.insert({"name":"name_"+str(i), "number":i})

# 获取查询结果游标，可以遍历获取数据
# result = col.find()
# print(result)
# for data in result:
#     print(data)

# 获取一条数据
# data = col.find_one()
# print(data)

# 删除数据
# col.remove({"name":"name_0"})


# 更新操作
# col.update({"name":"name_1"},{"name":"yang"})
# col.update({"name":"name_2"},{"$set":{"name":"yang"}})

col.update({"name": "name_10"}, {"$set": {"name": "yang"}}, upsert=True)

result = col.find()
print(result)
for data in result:
    print(data)
