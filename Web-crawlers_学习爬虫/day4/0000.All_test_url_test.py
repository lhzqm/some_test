from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

from test_all_web import 中国科学院动物研究所, \
    中国科学院植物研究所, \
    中国科学院生物物理研究所, \
    中国科学院遗传与发育生物学研究所, \
    中山大学新闻网, \
    清华大学药学院, \
    清华大学生命科学院, \
    清华大学医学院, \
    北京生命科学研究所, \
    北京大学生命科学学院, \
    中国科学院武汉病毒研究所, \
    中国科学技术大学生命科学学院, \
    华中科技大学同济医学院基础医学院, \
    上海交通大学医学院, \
    华中农业大学, \
    中山大学生命科学学院首页动图, \
    同济大学医学院, \
    复旦大学上海医学院, \
    中国医学科学院_北京协和医学院, \
    中国农业大学生物学院, \
    中国科学院生化与细胞生物学研究所, \
    中国科学院上海生命科学研究院, \
    中国科学院心理研究所, \
    中国科学院微生物研究所, \
    中国科学院北京基因组研究所

import os
import linecache
import time

start_time = time.clock()
f0 = open('./test/md5.txt', 'r', encoding='utf-8')
old_information = f0.readlines()
f0.close()

old_information = eval(str(old_information))

# 1
test = 中国科学院动物研究所. \
    Institute_of_Zoology('http://www.ioz.ac.cn/gb2018/xwdt/kyjz/')
test.run()
# 'http://www.ioz.ac.cn/gb2018/xwdt/kyjz/index_1.html'
# 'http://www.ioz.ac.cn/gb2018/xwdt/kyjz/'


# 2
test = 中国科学院植物研究所. \
    Institute_of_Botany('http://www.ibcas.ac.cn/News/')
test.run()
# http://www.ibcas.ac.cn/News/index_1.html
# 'http://www.ibcas.ac.cn/News/'

# 3
test = 中国科学院生物物理研究所. \
    Institute_of_Biophysics('http://www.ibp.cas.cn/kyjz/')
test.run()
# http://www.ibp.cas.cn/kyjz/
# http://www.ibp.cas.cn/kyjz/index_1.html

# 4
test = 中国科学院遗传与发育生物学研究所. \
    Institute_of_Genetic_Development('http://www.genetics.ac.cn/xwzx/kyjz/')
test.run()
# http://www.genetics.ac.cn/xwzx/kyjz/
# http://www.genetics.ac.cn/xwzx/kyjz/index_1.html

# 5
test = 中山大学新闻网. \
    Zhongshan_University_News_Network('http://news2.sysu.edu.cn/ky/')
test.run()
# http://news2.sysu.edu.cn/ky/
# http://news2.sysu.edu.cn/ky/index1.htm

# 6
test = 北京大学生命科学学院. \
    Peking_University_School_of_Life_Sciences('http://www.bio.pku.edu.cn/homes/Index/news/22/22_2.html')
test.run()
# http://www.bio.pku.edu.cn/homes/Index/news/22/22.html
# http://www.bio.pku.edu.cn/homes/Index/news/22/22_2.html

# 7
test = 北京生命科学研究所. \
    Beijing_Institute_of_Life_Sciences('http://www.nibs.ac.cn/news.php?cid=4&sid=15')
test.run()
# http://www.nibs.ac.cn/news.php?cid=4&sid=15
# http://www.nibs.ac.cn/news.php?cid=4&sid=15&page=2

# 8
test = 清华大学医学院. \
    Tsinghua_University_School_of_Medicine('http://www.med.tsinghua.edu.cn/MoreServlet?newsClass=12')
test.run()
# http://www.med.tsinghua.edu.cn/MoreServlet?newsClass=12
# http://www.med.tsinghua.edu.cn/MoreServlet?rowCount=135&curPage=2

# 9
test = 清华大学生命科学院. \
    Tsinghua_University_School_of_Life_Science('http://life.tsinghua.edu.cn/publish/smkx/11191/index_2.html')
test.run()
# http://life.tsinghua.edu.cn/publish/smkx/11191/index.html
# http://life.tsinghua.edu.cn/publish/smkx/11191/index_2.html

# 10
test = 清华大学药学院. \
    Tsinghua_University_School_of_Pharmacy('http://www.sps.tsinghua.edu.cn/cn/news/achievement/')
test.run()
# http://www.sps.tsinghua.edu.cn/cn/news/achievement/
# http://www.sps.tsinghua.edu.cn/cn/news/achievement/2.html

# 11
test = 中国科学院武汉病毒研究所.Virus_research_institute('http://www.whiov.cas.cn/kyjz_105338/')
test.run()

# 12
test = 中国科学技术大学生命科学学院.College_Of_Life_Sciences('https://biox.ustc.edu.cn/632/list.htm')
test.run()

# 13
test = 华中科技大学同济医学院基础医学院.Test('http://jcyxy.tjmu.edu.cn/kxyj/kycg.htm')
test.run()

# 14
test = 上海交通大学医学院.Test('https://www.shsmu.edu.cn/news/kydt1.htm')
test.run()

# 15
test = 华中农业大学.Test('http://yjs.hzau.edu.cn/cxcy.htm')
test.run()

# 16
test = 中山大学生命科学学院首页动图.Test('http://lifesciences.sysu.edu.cn/')
test.run()

# 17
test = 同济大学医学院.Test('http://202.120.163.6/Web/News/92')
test.run()

# 18
test = 复旦大学上海医学院.Test('http://shmc.fudan.edu.cn/')
test.run()

# 19
test = 中国医学科学院_北京协和医学院.Test('http://www.cams.ac.cn/blog/category/uncategorized/')
test.run()

# 20
test = 中国农业大学生物学院.Test('http://cbs.cau.edu.cn/col/col35535/index.html')
test.run()

# 21
test = 中国科学院生化与细胞生物学研究所.Test('http://www.sibcb.ac.cn/cpRecentRes.asp?type=%BF%C6%D1%D0%BD%F8%D5%B9')
test.run()

# 22
test = 中国科学院上海生命科学研究院.Test('http://www.sibs.cas.cn/xwdt/kyjz/')
test.run()

# 23
test = 中国科学院心理研究所.Test('http://www.psych.ac.cn/xwzx/kyjz/')
test.run()

# 24
test = 中国科学院微生物研究所.Test('http://www.im.cas.cn/xwzx2018/kyjz/')
test.run()

# 25
test = 中国科学院北京基因组研究所.Test('http://www.big.cas.cn/xwzx/kyjz/index_1.html')
test.run()
# http://www.big.cas.cn/xwzx/kyjz/
# http://www.big.cas.cn/xwzx/kyjz/index_1.html

f1 = open('./test/md5.txt', 'r', encoding='utf-8')
new_information = f1.readlines()
f1.close()

new_information = eval(str(new_information))

old_last_list = []
for old_dit in old_information:
    old_dit = eval(str(old_dit))
    old_last_list.append(old_dit)

new_last_list = []
for new_dit in new_information:
    new_dit = eval(str(new_dit))
    new_last_list.append(new_dit)

# print(old_last_list)
# print(new_last_list)
f = open('result.txt', 'w+', encoding='utf-8')
content_update = []
for i in range(len(old_last_list)):
    if old_last_list[i]['hashlib_md5'] == new_last_list[i]['hashlib_md5']:
        pass
        # print('{}:'.format(i + 1) + new_last_list[i]['name'] + '-->\t没有更新')
        # content.append('{}:'.format(i + 1) + new_last_list[i]['name'] + '-->\t没有更新' + '\n')
        # f.write('{}:'.format(i + 1) + new_last_list[i]['name'] + '-->\t没有更新' + '\n')
    else:
        # print('{}:'.format(i + 1) + new_last_list[i]['name'] + '-->\t!!!有更新!!!\t' + new_last_list[i]['url'])
        content_update.append(new_last_list[i]['name'])
        f.write('{}:'.format(i + 1) + new_last_list[i]['name'] + '-->\t!!!有更新!!!\t' + new_last_list[i]['url'] + '\n')
f.close()

end_time = time.clock()

print(end_time - start_time)

import smtplib
from email.header import Header
from email.mime.text import MIMEText

# 第三方 SMTP 服务
# mail_host = "smtp.163.com"  # SMTP服务器
# mail_user = "xiaowutongzhi_zqm@163.com"  # 用户名
# mail_pass = "ehbio001"  # 授权密码，非登录密码
# smtpserver = 'smtp.163.com'
# sender = 'xiaowutongzhi_zqm <xiaowutongzhi_zqm@163.com>'  # 发件人邮箱(最好写全, 不然会失败)
# receivers = ['<554839316@qq.com>', 'train <train@ehbio.com>']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱


f = open('result.txt', "r", encoding='utf-8')
content = f.read()
# print(len(content))

if len(content) == 0:
    print('没有更新')
    # quit()
    exit()

import datetime

nowTime = datetime.datetime.now().strftime('%Y-%m-%d-%H:%M:%S')  # 现在
title = '{}:更新情况'.format(nowTime)  # 邮件主题

# content = str(content)

smtpserver = 'smtp.163.com'


def sendEmail(content_update, smtpserver):
    sender = 'xiaowutongzhi_zqm <xiaowutongzhi_zqm@163.com>'
    receiver = ['<554839316@qq.com>', 'train <train@ehbio.com>']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱
    # smtpserver = 'smtp.163.com'
    username = "xiaowutongzhi_zqm@163.com"  # 用户名
    password = "ehbio001"  # 授权密码，非登录密码
    mail_title = title
    # mail_title = '主题：这是带附件的邮件'

    # 创建一个带附件的实例
    message = MIMEMultipart()
    message['From'] = sender
    message['To'] = ",".join(receiver)
    message['Subject'] = Header(mail_title, 'utf-8')

    # 邮件正文内容
    message.attach(MIMEText(content, 'plain', 'utf-8'))

    # 构造附件1（附件为TXT格式的文本）
    att1 = MIMEText(open('./test/%s.txt' % content_update[0], 'rb').read(), 'base64', 'utf-8')
    att1["Content-Type"] = 'application/octet-stream'
    att1["Content-Disposition"] = 'attachment; filename="text1.txt"'
    message.attach(att1)

    smtpObj = smtplib.SMTP_SSL()  # 注意：如果遇到发送失败的情况（提示远程主机拒接连接），这里要使用SMTP_SSL方法
    smtpObj.connect(smtpserver)
    smtpObj.login(username, password)
    smtpObj.sendmail(sender, receiver, message.as_string())
    print("邮件发送成功！！！")
    smtpObj.quit()

    # # message = MIMEText(content, 'plain', 'utf-8')  # 内容, 格式, 编码
    # message = MIMEMultipart()
    # message['From'] = sender
    # message['To'] = receivers
    # message['Subject'] = Header(title, 'utf-8')
    # print(content)
    # # 邮件正文内容
    # message.attach(MIMEText('来来来，这是邮件的正文', 'plain', 'utf-8'))
    #
    # # 构造附件1（附件为TXT格式的文本）
    # print(content_update)
    # # print(content_update[0])
    #
    # # att1 = MIMEText(open('./test/%s.txt' % content_update[0], 'rb').read(), 'base64', 'utf-8')
    # att1 = MIMEText(open('2to3.py', 'rb').read(), 'base64', 'utf-8')
    # att1["Content-Type"] = 'application/octet-stream'
    # att1["Content-Disposition"] = 'attachment; filename="text1.txt"'
    # message.attach(att1)
    #
    # try:
    #     # smtpObj = smtplib.SMTP_SSL(mail_host, 465)  # 启用SSL发信, 端口一般是465
    #     smtpObj = smtplib.SMTP_SSL()  # 注意：如果遇到发送失败的情况（提示远程主机拒接连接），这里要使用SMTP_SSL方法
    #     smtpObj.connect(smtpserver)
    #     smtpObj.login(mail_user, mail_pass)  # 登录验证
    #     smtpObj.sendmail(sender, receivers, message.as_string())  # 发送
    #     print("mail has been send successfully.")
    # except smtplib.SMTPException as e:
    #     print(e)


def send_email2(SMTP_host, from_account, from_passwd, to_account, subject, content):
    email_client = smtplib.SMTP(SMTP_host)
    email_client.login(from_account, from_passwd)
    # create msg
    msg = MIMEText(content, 'plain', 'utf-8')
    msg['Subject'] = Header(subject, 'utf-8')  # subject
    msg['From'] = from_account
    msg['To'] = to_account
    email_client.sendmail(from_account, to_account, msg.as_string())

    email_client.quit()


# if __name__ == '__main__':
sendEmail(content_update,smtpserver)
# receiver = '***'
# send_email2(mail_host, mail_user, mail_pass, receiver, title, content)
end_time2 = time.clock()
print(end_time2 - start_time)
