# coding:utf-8
from queue import Queue

q = Queue()
# qs = Queue(maxsize=10)

# for i in range(10):
#     q.put(i)
#     qs.put(i)

# q.put(10)
# qs.put_nowait(10)

# print(q.get())

print(q.unfinished_tasks)
q.put(1)
print(q.unfinished_tasks)
q.get()
print(q.unfinished_tasks)
q.task_done()
print(q.unfinished_tasks)

# 让主线程等待队列操作结束在退出
q.join()
