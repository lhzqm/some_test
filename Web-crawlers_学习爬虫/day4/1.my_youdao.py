import requests
import json
import time
import random
import hashlib


class Youdao(object):
    def __init__(self, word):
        self.word = word
        self.url = 'http://fanyi.youdao.com/translate_o?smartresult=dict&smartresult=rule'
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
            'Referer': 'http://fanyi.youdao.com/?keyfrom=dict2.index',
            'Cookie': 'OUTFOX_SEARCH_USER_ID_NCOO=1853134378.3670228; _ga=GA1.2.525458067.1551680008; OUTFOX_SEARCH_USER_ID="1973214119@10.168.11.12"; P_INFO=lhsmzqm@163.com|1556016252|0|other|00&99|gud&1555761769&mail163#zhj&null#10#0#0|&0|mail163|lhsmzqm@163.com; JSESSIONID=aaaH3NHJevMLagOTDY42w; DICT_SESS=v2|8UiQjnvGhP4OLgK0MYA0gBPLUAO4QB0k564PZhHUfR6FnLPynMkf0guOMJyhHY50QZhLwLRfk50pynHwyRHzfRzWhLYfnHlWR; DICT_LOGIN=1||1570763311874; ___rl__test__cookies=1570763330554'

        }
        self.post_data = None

    def generate_post_data(self):
        # 构造r
        # , r = "" + (new Date).getTime()
        # , i = r + parseInt(10 * Math.random(), 10);

        now = int(time.time() * 1000)
        randint = random.randint(0, 9)

        new_r = str(now)
        i = str(now + randint)

        n = self.word
        r = str(now + randint)
        # 构造o
        S = "fanyideskweb"

        D = "n%A-rKaT5fb[Gy?;N5@Tj"

        tempstr = S + n + r + D

        # 1. 构建哈希对象
        md5 = hashlib.md5()

        # 2. 将需要进行hash运算的字符串更新到hash对象中,python3中需要bytes类型的字符串数据
        md5.update(tempstr.encode())
        # 3. 获取字符串对应的hash值
        o = md5.hexdigest()

        self.post_data = {
            "i": self.word,
            "from": "AUTO",
            "to": "AUTO",
            "smartresult": "dict",
            "client": "fanyideskweb",
            "salt": r,
            "sign": o,
            "ts": "1570759301128",
            "bv": "3a019e7d0dda4bcd253903675f2209a5",
            "doctype": "json",
            "version": "2.1",
            "keyfrom": "fanyi.web",
            "action": "FY_BY_REALTlME",
        }

    def get_data(self):
        response = requests.post(self.url, headers=self.headers, data=self.post_data)
        return response.content

    def parse_data(self, data):
        dict_data = json.loads(data)
        print(dict_data)
        print(dict_data['translateResult'][0][0]['tgt'])
        # print(dict_data['translateResult'][0][0]['tgt'])

    def run(self):
        # url
        # headers
        # 构造post请求
        self.generate_post_data()
        # 发送post请求
        data = self.get_data()
        # print(data)
        # 解析相应
        self.parse_data(data)


if __name__ == '__main__':
    youdao = Youdao('life')
    youdao.run()
