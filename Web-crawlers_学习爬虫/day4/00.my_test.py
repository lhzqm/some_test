import os
import requests
from lxml import etree


class Test(object):
    def __init__(self, url):
        self.url = url
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        }

    def get_data(self, url):
        response = requests.get(url, headers=self.headers)
        return response.content

    def parse_list_page(self, data):
        # 将相应内容创建成element对象
        html = etree.HTML(data)
        # 获取所有标题节点列表
        # 获取url node_url = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a/@href')[0]
        node_url = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a')
        # 获取标题 node_title = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a/p/text()')[0]
        node_title = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a/p')
        # 获取日期 node_data = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a/span/text()')[0]
        node_data = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a/span')

        url_list = []
        for node in node_url:
            url_list.append(node.xpath('./@href')[0])

        title_list = []
        for node in node_title:
            title_list.append(node.xpath('./text()')[0])

        date_list = []
        for node in node_data:
            date_list.append(node.xpath('./text()')[0])

        total_list = []
        # 生成字典
        for i in range(len(url_list)):
            temp = dict()
            temp['title'] = title_list[i]
            temp['date'] = date_list[i]
            temp['url'] = url_list[i]
            total_list.append(temp)

        return total_list

    def parse_detail_page(self, page):
        html = etree.HTML(page)

        # 获取详情页面所有的段落
        image_list_duanluo = html.xpath('//*[@id="zoom"]/div/p/span/text()')

        return image_list_duanluo

    def run(self):
        # 构建列表页面
        # 构建headers
        # 循环
        # 发起列表页面请求
        data = self.get_data(self.url)
        with open('test.html', 'wb')as f:
            f.write(data)

        # 解析列表页面,获取标题详情URL列表
        self.parse_list_page(data)
        # 解析列表页面请求
        detail_list = self.parse_list_page(data)
        # print(detail_list)

        # 写文件 #todo
        f = open('北京生物科学院___第一页.txt', 'w+', encoding='utf-8')
        for detail in detail_list:
            f.write(detail['title'] + '\t')
            # detail_not_point = detail['url'].replace('.', '', 1)
            f.write(detail['date'] + '\t')
            detail = 'http://www.bio.pku.edu.cn' + detail['url']
            f.writelines(detail + "\n")

        # 遍历列表
        # for detail in detail_list:
        #     # 发起详情页面请求
        #     # print(detail['url'].replace('.', ''))
        #     detail2 = detail
        #     detail_not_point = detail['url'].replace('.', '', 1)
        #     detail = 'http://www.genetics.ac.cn/xwzx/kyjz' + detail_not_point
        #     # print(detail)
        #     page = self.get_data(detail)

        # 发起详情页请求

        # print(detail['url'])
        # 解析详情页面响应获取图片url列表
        # image_list = self.parse_detail_page(page)

        # 下载图片

        # self.download(image_list, detail2['title'])

        # 判断下一页


if __name__ == '__main__':
    test = Test('http://www.bio.pku.edu.cn/homes/Index/news/22/22.html')
    test.run()
