import os
import requests
from lxml import etree
import time

from my_self_test import tets, Youdao

youdao = Youdao('life')
youdao.run()


# 打印当前时间
# print(time.ctime().split(' ')[-1])


class Test(object):
    def __init__(self, url):
        # 构建列表页面
        self.url = url
        # 构建headers
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        }

    def get_data(self, url):
        response = requests.get(url, headers=self.headers)
        return response.content

    def parse_list_page(self, data):
        # 将相应内容创建成element对象
        html = etree.HTML(data)
        # 获取所有标题节点列表
        node_list = html.xpath('//*[@id="newContent"]/div[3]/div/div/div[2]/a')
        # 获取日期节点列表
        node_date_list = html.xpath('//*[@id="newContent"]/div[3]/div/div/div[1]/a')

        # print(len(node_list))
        # print(len(node_date_list))

        detail_list = []
        for index in range(len(node_list)):
            temp = dict()
            temp['title'] = node_list[index].xpath('./text()')[0].replace('\n\t\t\t\t\t\t', '').replace('\t\t\t\t\t',
                                                                                                        '')
            temp['date'] = node_date_list[index].xpath('./text()')[0].replace('\t\t\t\t\t', '')
            temp['url'] = node_list[index].xpath('./@href')[0]
            detail_list.append(temp)

        # print(detail_list)

        # next_url = html.xpath('//*[@id="pagenav_17"]/@href')

        return detail_list

    def parse_detail_page(self, page):
        html = etree.HTML(page)

        # 获取详情页面所有的段落
        image_list_duanluo = html.xpath('//*[@id="zoom"]/div/p/span/text()')

        return image_list_duanluo

    def run(self):
        # 循环
        # 发起列表页面请求
        data = self.get_data(self.url)
        with open('my_self_test.html', 'wb')as f:
            f.write(data)

        # 解析列表页面,获取标题详情URL列表

        # 解析列表页面请求
        # self.parse_list_page(data)

        detail_list = self.parse_list_page(data)

        # print(detail_list)

        # 写文件 #todo

        f = open('清华大学药学院___第一页.txt', 'w+', encoding='utf-8')
        for detail in detail_list:
            f.write(detail['title'] + '\t')
            f.write(detail['date'] + '\t')
            url = detail['url']
            print(url)
            # http://www.med.tsinghua.edu.cn/SingleServlet?newsId=1871
            # detail = 'http://www.med.tsinghua.edu.cn/' + url
            f.writelines(url + "\n")


if __name__ == '__main__':
    test = Test('http://www.sps.tsinghua.edu.cn/cn/news/achievement/')
    test.run()
