import os
import requests
from lxml import etree



class Test(object):
    def __init__(self, url):
        self.url = url
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        }

    def get_data(self, url):
        response = requests.get(url, headers=self.headers)
        return response.content

    def parse_list_page(self, data):
        # 将相应内容创建成element对象
        html = etree.HTML(data)
        # 获取所有标题节点列表
        node_list = html.xpath('//*[@id="wholebody"]/table[7]/tr/td[3]/table/tr/td/table[3]/tr/td[1]/a')
        # print(len(node_list))

        detail_list = []
        for node in node_list:
            temp = {}
            temp['title'] = node.xpath('./text()')[0]
            temp['url'] = node.xpath('./@href')[0]
            detail_list.append(temp)
            # print(detail_list)

        next_url = html.xpath('//*[@id="pagenav_17"]/@href')

        return detail_list, next_url

    def parse_detail_page(self, page):
        html = etree.HTML(page)

        # 获取详情页面所有的段落
        image_list_duanluo = html.xpath('//*[@id="zoom"]/div/p/span/text()')

        return image_list_duanluo

    # def download(self, image_list, title):
    #     if not os.path.exists('txt'):
    #         os.makedirs('txt')
    #
    #     f = open('txt/' + str(title), 'w', encoding='utf-8')
    #     # with open(filename, 'w')as f:
    #     f.write(str(image_list))
    #
    #     print(image_list)
    #
    #     print(len(image_list))
    #
    #     # print(detail_list)
    #
    #     # for url in image_list:
    #     # print(url)
    #     # filename = str(i) + '.txt'
    #     # i += 1
    #     # # data = self.get_data(url)
    #     # f = open('txt/' + filename, 'w', encoding='utf-8')
    #     # # with open(filename, 'w')as f:
    #     # f.write(image_list)

    def run(self):
        # 构建列表页面
        # 构建headers
        # 循环
        # 发起列表页面请求
        data = self.get_data(self.url)
        with open('my_self_test.html', 'wb')as f:
            f.write(data)

        # 解析列表页面,获取标题详情URL列表
        self.parse_list_page(data)
        # 解析列表页面请求
        detail_list, next_url = self.parse_list_page(data)
        # print(detail_list)

        # 写文件 #todo
        f = open('中国科学院遗传与发育生物研究所___第一页.txt', 'w+', encoding='utf-8')
        for detail in detail_list:
            f.write(detail['title'] + '\t')
            detail_not_point = detail['url'].replace('.', '', 1)
            date = detail_not_point[detail_not_point.find('t'):detail_not_point.rfind('_')]
            # print(date)
            f.write(date + '\t')
            detail = 'http://www.genetics.ac.cn/xwzx/kyjz' + detail_not_point
            f.writelines(detail + "\n")

        # 遍历列表
        # for detail in detail_list:
        #     # 发起详情页面请求
        #     # print(detail['url'].replace('.', ''))
        #     detail2 = detail
        #     detail_not_point = detail['url'].replace('.', '', 1)
        #     detail = 'http://www.genetics.ac.cn/xwzx/kyjz' + detail_not_point
        #     # print(detail)
        #     page = self.get_data(detail)

        # 发起详情页请求

        # print(detail['url'])
        # 解析详情页面响应获取图片url列表
        # image_list = self.parse_detail_page(page)

        # 下载图片

        # self.download(image_list, detail2['title'])

        # 判断下一页


if __name__ == '__main__':
    test = Test('http://www.genetics.ac.cn/xwzx/kyjz/')
    test.run()
