import os
import requests
from lxml import etree


class Test(object):
    def __init__(self, url):
        # 构建列表页面
        self.url = url
        # 构建headers
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        }

    def get_data(self, url):
        response = requests.get(url, headers=self.headers)
        return response.content

    def parse_list_page(self, data):
        # 将相应内容创建成element对象
        html = etree.HTML(data)
        # 获取所有标题节点列表
        node_list = html.xpath('//*[@id="trs-widget-2640"]/div/div/div[1]/div/div/div/div/ul/li/span[1]/a')
        print(len(node_list))

        detail_list = []
        for node in node_list:
            temp = dict()
            temp['title'] = node.xpath('./text()')[0]
            temp['url'] = node.xpath('./@href')[0]
            detail_list.append(temp)

        print(detail_list)

        return detail_list

    def parse_detail_page(self, page):
        html = etree.HTML(page)

        # 获取详情页面所有的段落
        image_list_duanluo = html.xpath('//*[@id="zoom"]/div/p/span/text()')

        return image_list_duanluo

    def run(self):
        # 循环
        # 发起列表页面请求
        data = self.get_data(self.url)
        with open('my_self_test.html', 'wb')as f:
            f.write(data)

        # 解析列表页面,获取标题详情URL列表

        # 解析列表页面请求
        # self.parse_list_page(data)

        detail_list = self.parse_list_page(data)
        # print(detail_list)

        # 写文件 #todo

        f = open('20.中国科学院武汉病毒研究所___第一页.txt', 'w+', encoding='utf-8')
        for detail in detail_list:
            f.write(detail['title'] + '\t')
            detail_not_point = detail['url'].replace('.', '', 1)
            print(detail_not_point)
            print(detail_not_point.find('t'))
            date = detail_not_point[detail_not_point.find('t'):detail_not_point.rfind('_')]
            print(date)

            f.write(date + '\t')
            # http://www.whiov.cas.cn/kyjz_105338/201910/t20191014_5407522.html
            detail = 'http://www.whiov.cas.cn/kyjz_105338' + detail_not_point
            f.writelines(detail + "\n")

            # http://www.ibp.cas.cn/kyjz/zxdt/zxdt/201904/t20190418_5276958.html
            # http://www.ibp.cas.cn/kyjz/zxdt/201904/t20190418_5276958.html


if __name__ == '__main__':
    test = Test('http://www.whiov.cas.cn/kyjz_105338/')
    test.run()
