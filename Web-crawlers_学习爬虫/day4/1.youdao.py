# coding:utf-8
import requests
import json
import time
import random
import hashlib


class Youdao(object):

    def __init__(self, word):
        self.word = word
        self.url = 'http://fanyi.youdao.com/translate_o?smartresult=dict&smartresult=rule'
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',
            'Referer': 'http://fanyi.youdao.com/',
            'Cookie': 'OUTFOX_SEARCH_USER_ID=-1392948312@10.169.0.84; OUTFOX_SEARCH_USER_ID_NCOO=1507578365.437866; fanyi-ad-id=44881; fanyi-ad-closed=1; JSESSIONID=aaaTxaaEb6tQza56gPZow; ___rl__test__cookies=1527727306501'
        }
        self.post_data = None

    def generate_post_data(self):
        # 构造r
        # r = "" + ((new Date).getTime() + parseInt(10 * Math.random(), 10))
        now = int(time.time() * 1000)
        randint = random.randint(0, 9)
        r = str(now + randint)

        # 构造o
        # o = u.md5(S + n + r + D)
        S = "fanyideskweb"
        n = self.word
        D = "n%A-rKaT5fb[Gy?;N5@Tj"

        tempstr = S + n + r + D

        # 1 构建hash对象
        md5 = hashlib.md5()

        # 2 将需要进行hash运算的字符串更新到hash对象中,python3中需要bytes类型的字符串数据
        md5.update(tempstr.encode())

        # 3.获取字符串对应的hash值
        o = md5.hexdigest()

        self.post_data = {
            "i": self.word,
            "from": "AUTO",
            "to": "AUTO",
            "smartresult": "dict",
            "client": "fanyideskweb",
            "salt": r,
            "sign": o,
            "doctype": "json",
            "version": "2.1",
            "keyfrom": "fanyi.web",
            "action": "FY_BY_REALTIME",
            "typoResult": False,
        }

    def parse_data(self, data):
        dict_data = json.loads(data)
        print(dict_data)
        print(dict_data['translateResult'][0][0]['tgt'])

    def get_data(self):
        response = requests.post(self.url, headers=self.headers, data=self.post_data)
        return response.content

    def run(self):
        # url
        # headers
        # 构造post请求的数据
        self.generate_post_data()
        # 发送post请求
        data = self.get_data()

        # 解析响应
        self.parse_data(data)


if __name__ == '__main__':
    youdao = Youdao('analysis')
    youdao.run()
