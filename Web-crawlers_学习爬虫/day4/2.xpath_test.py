# coding:utf-8
from lxml import etree

text = ''' 
<div> 
    <ul> 
        <li class="item-1"><a href="link1.html">first item</a></li> 
        <li class="item-1"><a href="link2.html">second item</a></li> 
        <li class="item-inactive"><a href="link3.html">third item</a></li> 
        <li class="item-1"><a href="link4.html">fourth item</a></li> 
        <li class="item-0"><a href="link5.html">fifth item</a>
    </ul> 
</div> 
'''

html = etree.HTML(text)
# print(dir(html))

node_list = html.xpath('//li/a')
for node in node_list:
    text = node.xpath('./text()')[0] if len(node.xpath('./text()')) > 0 else None
    link = node.xpath('./@href')[0] if len(node.xpath('./@href')) > 0 else None
    print(text, link)

# print(html.xpath('//li[1]/a/text()')[0])
text_list = html.xpath('//a/text()')
link_list = html.xpath('//a/@href')
# print(html.xpath('//li[@class="item-inactive"]/a/text()'))


# zip命令用于合并列表索引相同的值
for data in zip(text_list,link_list):
    print(data)


# print(html)
# print(type(html))
# print(dir(html))

# 将element对象转换成sting字符串
# result = etree.tostring(html)
# print(result)
