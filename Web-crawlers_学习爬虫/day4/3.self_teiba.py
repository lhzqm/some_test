import os

import requests
from lxml import etree


class Tieba(object):
    def __init__(self, name):
        self.name = name
        # 构建列表页面url
        self.url = 'https://tieba.baidu.com/f?kw={}'.format(self.name)
        # 构建headers
        self.headers = {
            'User-Agent': 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)',
        }

    def get_data(self, url):
        response = requests.get(url, headers=self.headers)
        return response.content

    def parse_list_page(self, data):
        # 将相应内容创建成element对象
        html = etree.HTML(data)

        # 获取所有标题节点列表
        node_list = html.xpath('//li[@class=" j_thread_list clearfix"]/div/div[2]/div[1]/div[1]/a')
        detail_list = []
        for node in node_list:
            temp = dict()
            temp['title'] = node.xpath('./text()')[0]
            temp['url'] = 'http://tieba.baidu.com' + node.xpath('./@href')[0]
            detail_list.append(temp)

        next_url = html.xpath('//a[@class="next pagination-item "]/@href')

        return detail_list, next_url

    def parse_detail_page(self, page):
        html = etree.HTML(page)
        # 获取详情页面所有的图片
        image_list = html.xpath('//*[contains(@id,"post_content_")]/img/@src')
        # print(image_list)
        return image_list

    def download(self, image_list):
        if not os.path.exists('my_images'):
            os.makedirs('my_images')

        # print(image_list)
        for url in image_list:
            if 'emoticon' in url or '.jpg' not in url:
                continue
            filename = 'my_images' + os.sep + url.split('/')[-1]
            # print(filename)
            data = self.get_data(url)
            # print(data)
            with open(filename, 'wb')as f:
                f.write(data)

    def run(self):
        url = self.url
        # 循环
        while Tieba:
            # 发起列表页面请求
            data = self.get_data(url)
            with open('self.html', 'wb')as f:
                f.write(data)
            # 解析列表页面,获取标题详情url
            detail_list, next_url = self.parse_list_page(data)
            print(next_url)
            # 遍历列表
            for detail in detail_list:
                # 发起详情页面请求
                # print(detail['url'])
                page = self.get_data(detail['url'])

                # 解析详情页面响应获取图片url列表
                image_list = self.parse_detail_page(page)

                # 下载图片
                self.download(image_list)

            # 判断下一页
            if next_url is []:
                break
            else:
                url = 'https:' + next_url[0]


if __name__ == '__main__':
    tieba = Tieba('python ')
    tieba.run()
