import hashlib
import os
import requests
from lxml import etree
import time


# 打印当前时间
# print(time.ctime().split(' ')[-1])


class Test(object):
    def __init__(self, url):
        # 构建列表页面
        self.url = url
        # 构建headers
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        }

    def get_data(self, url):
        try:
            response = requests.get(url, headers=self.headers, timeout=3)
        except Exception as e:
            print("中国科学院生化与细胞生物学研究所___第一页_8连接超时{}".format(e))
            return
        return response.content

    def parse_list_page(self, data):
        # 将相应内容创建成element对象
        html = etree.HTML(data)
        # 获取所有标题节点列表
        node_list = html.xpath('/html/body/table[3]/tr/td[2]/table/tr[3]/td/table/tr/td/table/tr/td[2]/a')
        # /html/body/div/div[3]/div/div[2]/ul/li/a
        # 获取日期节点列表
        node_date_list = html.xpath('/html/body/table[3]/tr/td[2]/table/tr[3]/td/table/tr/td/table/tr/td[2]')

        # print(len(node_list))
        # print(len(node_date_list))

        detail_list = []
        for index in range(len(node_list)):
            temp = dict()
            temp['title'] = node_list[index].xpath('./text()')[0]
            temp['date'] = node_date_list[index].xpath('./text()')[0]
            temp['url'] = node_list[index].xpath('./@href')[0]
            detail_list.append(temp)

        # print(detail_list)

        return detail_list

    def run(self):
        # 循环
        # 发起列表页面请求
        data = self.get_data(self.url)
        # with open('my_self_test.html', 'wb')as f:
        #     f.write(data)

        # 解析列表页面,获取标题详情URL列表

        # 解析列表页面请求
        # self.parse_list_page(data)
        detail_list = self.parse_list_page(data)

        # 写文件
        f = open('./test/中国科学院生化与细胞生物学研究所.txt', 'w+', encoding='utf-8')
        f1 = open('./test/md5.txt', 'a+', encoding='utf-8')
        for index, detail in enumerate(detail_list):
            if index == 0:
                md5 = hashlib.md5()
                md5.update(detail['title'].encode())
                o = md5.hexdigest()
                f1.write("{'hashlib_md5':'%s','name':'中国科学院生化与细胞生物学研究所','url':'%s'}" % (o, self.url) + '\n')

            f.write(detail['title'] + '\t')
            f.write(detail['date'] + '\t')
            url = detail['url']

            # http://www.sibcb.ac.cn/cpRecentRes-1.asp?id=4888

            detail = "http://www.sibcb.ac.cn/" + url
            # print(detail)
            f.writelines(detail + "\n")


if __name__ == '__main__':
    test = Test('http://www.sibcb.ac.cn/cpRecentRes.asp?type=%BF%C6%D1%D0%BD%F8%D5%B9')
    test.run()
