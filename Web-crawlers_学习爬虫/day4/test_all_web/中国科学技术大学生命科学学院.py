import hashlib
import os
import requests
from lxml import etree


class College_Of_Life_Sciences(object):
    def __init__(self, url):
        # 构建列表页面
        self.url = url
        # 构建headers
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        }

    def get_data(self, url):
        try:
            response = requests.get(url, headers=self.headers, timeout=3)
        except Exception as e:
            print("中国科学技术大学生命科学学院___第一页连接超时{}".format(e))
            return
        return response.content

    def parse_list_page(self, data):
        # 将相应内容创建成element对象
        html = etree.HTML(data)
        # 获取所有标题节点列表
        node_list = html.xpath('//*[@id="wp_news_w2"]/li/a')
        # print(len(node_list))

        detail_list = []
        for node in node_list:
            temp = dict()
            temp['title'] = node.xpath('./text()')[0]
            temp['url'] = node.xpath('./@href')[0]
            detail_list.append(temp)

        # print(detail_list)
        return detail_list

    def run(self):
        # 循环
        # 发起列表页面请求
        data = self.get_data(self.url)
        # with open('my_self_test.html', 'wb')as f:
        #     f.write(data)

        # 解析列表页面,获取标题详情URL列表

        # 解析列表页面请求
        # self.parse_list_page(data)

        detail_list = self.parse_list_page(data)
        # print(detail_list)

        # 写文件 #todo

        f = open('./test/中国科学技术大学生命科学学院.txt', 'w+', encoding='utf-8')
        f1 = open('./test/md5.txt', 'a+', encoding='utf-8')
        for index, detail in enumerate(detail_list):
            if index == 0:
                md5 = hashlib.md5()
                md5.update(detail['title'].encode())
                o = md5.hexdigest()
                f1.write("{'hashlib_md5':'%s','name':'中国科学技术大学生命科学学院','url':'%s'}" % (o, self.url) + '\n')

            f.write(detail['title'] + '\t')
            # print(detail['title'])
            detail_not_point = detail['url']
            # print(detail_not_point)
            date = detail_not_point[detail_not_point.find('/'):detail_not_point.find('c')]
            # print(date)

            f.write(date + '\t')
            # https://biox.ustc.edu.cn/2019/0927/c632a392583/page.htm
            detail = 'https://biox.ustc.edu.cn' + detail_not_point
            f.writelines(detail + "\n")


if __name__ == '__main__':
    test = College_Of_Life_Sciences('https://biox.ustc.edu.cn/632/list.htm')
    test.run()
