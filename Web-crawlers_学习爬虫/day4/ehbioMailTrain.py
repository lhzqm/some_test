#!/usr/bin/python
# -*- coding: utf-8 -*-  

desc = '''
Send emails.
'''

import sys
import imp

imp.reload(sys)
sys.setdefaultencoding('utf-8')

import os
from optparse import OptionParser as OP
import smtplib
import mimetypes
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.MIMEAudio import MIMEAudio
from email.MIMEImage import MIMEImage
from email.Encoders import encode_base64
from email.header import Header
from time import sleep, localtime, strftime

timeformat = "%Y-%m-%d %H:%M:%S"
from math import log


def cmdparameter(argv):
    if len(argv) == 1:
        global desc
        print(desc, file=sys.stderr)
        cmd = 'python ' + argv[0] + ' -h'
        os.system(cmd)
        sys.exit(1)
    usages = "%prog -r receiver@smtp.com -s subjsct -c hello -a attach1,attach2"
    parser = OP(usage=usages)
    # parser.add_option("-f", "--mail-file", dest='file',
    #        metavar="FILE", help="A file containing at least two \
    # columns. The first column is mail address, the second column is \
    # receiver's name.")
    parser.add_option("-r", "--receiver", dest='receiver',
                      metavar="RECEIVER", default="chentong_biology@163.com",
                      help="The receiver for your mail. Default <chentong_biology@163.com>.")
    parser.add_option("-C", "--Cc", dest='Cc',
                      metavar="CC",
                      help="CC mail address,one or multiple separated by comma")
    parser.add_option("-s", "--subject", dest='subject',
                      metavar="SUBJECT", default="原中科院一线生信人员组织的插图绘制和生信分析研讨班",
                      help="The subject for your mail")
    parser.add_option("-c", "--context", dest='context',
                      metavar="CONTEXT", help="The main context for your mail. \
Symbol <-> should be given if you want SYS.STDIN as the mail content.")
    parser.add_option("-z", "--context-is-file", dest='context_file',
                      default=False, action="store_true", metavar="CONTEXT", help="Context is file. The main context for your mail. \
Symbol <-> should be given if you want SYS.STDIN as the mail content.")
    parser.add_option("-a", "--attachement", dest='attachment',
                      metavar="ATTACHEMENT",
                      # default="BioinformaticsCourse.pdf",
                      # default="易汉博基因科技-简版宣传册.pdf",
                      help="The attachment for your mail. \
Multiple attachments should be separated by ','.")
    parser.add_option("-u", "--user", dest='user',
                      metavar="USER", default='train@ehbio.com',
                      help="The username for mail sender.")
    parser.add_option("-p", "--passwd", dest='passwd',
                      metavar="PASSWD", default='ehbio_train_1011',
                      help="The passwd for mail sender.")
    parser.add_option("-S", "--smtp", dest='smtp',
                      metavar="SMTP", default='smtp.ym.163.com',
                      help="The mail send server, for 163 is \
smtp.163.com. For tencent is smtp.genesino.com")
    parser.add_option("-P", "--port", dest='port',
                      default=994, type="int",
                      help="SSL port for 163 is 994. for tencent 465.")
    parser.add_option("-t", "--encry-type", dest='encry_type',
                      default='SSL',
                      help="SSL (default) or TLS")
    (options, args) = parser.parse_args(argv[1:])
    return (options, args)


def sendMail(subject, text, recipient, attachmentFilePaths, gmailUser, mailServer, Cc):
    msg = MIMEMultipart()
    msg['From'] = "%s<%s>" % (Header("易生信培训", "utf-8"), "train@ehbio.com")
    msg['To'] = recipient
    receiver = recipient.split(',')
    if Cc:
        msg['Cc'] = Cc
        receiver.extend(Cc.split(','))
    msg['Subject'] = Header(subject, "utf-8")
    msg.attach(MIMEText(text, 'html', 'utf-8'))

    for attachmentFilePath in attachmentFilePaths:
        if attachmentFilePath:
            msg.attach(getAttachment(attachmentFilePath))

        #    mailServer = smtplib.SMTP(smtp)
    #    mailServer.ehlo()
    #    mailServer.starttls()
    #    mailServer.ehlo()
    #    mailServer.login(gmailUser, gmailPassword)
    try:
        mailServer.sendmail(gmailUser, receiver, msg.as_string())
    except smtplib.SMTPDataError as e:
        print(e, file=sys.stderr)
        print(recipient, "mail failed", file=sys.stderr)
    except smtplib.SMTPServerDisconnected:
        raise smtplib.SMTPServerDisconnected

    # print 'Sent email to %s' % recipient


def getAttachment(attachmentFilePath):
    contentType, encoding = mimetypes.guess_type(attachmentFilePath)

    if contentType is None or encoding is not None:
        contentType = 'application/octet-stream'

    mainType, subType = contentType.split('/', 1)
    if mainType == 'text':
        file = open(attachmentFilePath, 'r')
        attachment = MIMEText(file.read(), 'base64', 'utf-8')
    else:
        file = open(attachmentFilePath, 'rb')

        if mainType == 'message':
            attachment = email.message_from_file(file)
        elif mainType == 'image':
            attachment = MIMEImage(file.read(), _subType=subType)
        elif mainType == 'audio':
            attachment = MIMEAudio(file.read(), _subType=subType)
        else:
            attachment = MIMEBase(mainType, subType)
    attachment.set_payload(file.read())
    encode_base64(attachment)

    file.close()

    attachment.add_header('Content-Disposition', 'attachment',
                          filename=os.path.basename(attachmentFilePath).encode('utf-8'))
    return attachment


# start to test
def main():
    options, args = cmdparameter(sys.argv)
    subject = options.subject
    context = options.context
    context_file = options.context_file
    port = options.port
    Cc = options.Cc
    if context != '-':
        content = context
    else:
        fh = sys.stdin
        content = sys.stdin.read()
    if context_file:
        content = open(context).read()
    # --------------------
    # content += '\n\nPlease DO NOT respond to this address. \n\n\
    # Mail to chentong_biology@163.com for more information.'
    recipient = options.receiver
    attach = [options.attachment]
    # if options.attachment:
    #    attach = [i.strip() for i in options.attachment.split(',')]
    # else:
    #    attach = []
    user = options.user
    passwd = options.passwd
    smtp = options.smtp
    encry_type = options.encry_type

    header = 0
    count = 1

    if (encry_type == "SSL"):
        mailServer = smtplib.SMTP_SSL(smtp, port)
    elif (encry_type == "TLS"):
        mailServer = smtplib.SMTP(smtp, port)
        mailServer.starttls()

        # mailServer.set_debuglevel(1)
    mailServer.ehlo()
    # mailServer.starttls()
    # mailServer.ehlo()
    mailServer.login(user, passwd)

    # print "Connected"

    try:
        sendMail(subject, content, recipient, attach, user, mailServer, Cc)
    except smtplib.SMTPServerDisconnected:
        reconnect = 1

    # print "End send mail to %s --%s" % (recipient,
    #        strftime(timeformat, localtime()))
    # --------------------------------END reading--
    mailServer.close()


# -------------------------END main---------------

main()
