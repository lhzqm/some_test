import os
import re
import requests
from lxml import etree


class Test(object):
    def __init__(self, url):
        # 构建列表页面
        self.url = url
        # 构建headers
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        }

    # 获取数据
    def get_data(self, url):
        response = requests.get(url, headers=self.headers)
        return response.content

    def parse_list_page(self, data):
        # 将相应内容创建成element对象
        html = etree.HTML(data)
        # 获取所有标题节点列表
        node_list = html.xpath('//*[@id="wholebody"]/table[7]/tr/td[3]/table/tr/td/table[3]/tr/td[1]/a')
        # print(len(node_list))

        # 详情页列表
        detail_list = []
        for node in node_list:
            temp = dict()
            temp['title'] = node.xpath('./text()')[0]
            temp['url'] = node.xpath('./@href')[0]
            detail_list.append(temp)
            # print(detail_list)

        # 下一页连接
        next_url = html.xpath('//*[@id="pagenav_tail"]/@href')
        # print(next_url) ['index_18.html']
        return detail_list, next_url

    def parse_detail_page(self, page):
        html = etree.HTML(page)

        # 获取详情页面所有的段落
        image_list_duanluo = html.xpath('//*[@id="zoom"]/div/p/span/text()')

        return image_list_duanluo

    def run(self):
        url = self.url
        # 循环
        i = 0
        while True:
            # 发起列表页面请求
            data = self.get_data(url)
            # with open('my_self_test.html', 'wb')as f:
            #     f.write(data)

            # 解析列表页面,获取标题详情URL列表
            detail_list, next_url = self.parse_list_page(data)

            # 写文件 #todo
            f = open('中国科学院遗传与发育生物研究所___全部页.txt', 'a+', encoding='utf-8')
            for detail in detail_list:
                f.write(detail['title'] + '\t')
                detail_not_point = detail['url'].replace('.', '', 1)

                date = detail_not_point[8:detail_not_point.rfind('_')]
                # print(date)
                f.write(date + '\t')
                detail = 'http://www.genetics.ac.cn/xwzx/kyjz' + detail_not_point

                f.writelines(detail + "\n")

            if len(next_url) == 0:
                break
            else:
                # url = 'https:' + next_url[0]
                i = i + 1
                if i >= int(re.findall(r"\d+?\d*", next_url[0])[0]) + 1:
                    break
                # print(i)
                url = 'http://www.genetics.ac.cn/xwzx/kyjz/index_{}.html'.format(i)
                # print(url)


if __name__ == '__main__':
    test = Test('http://www.genetics.ac.cn/xwzx/kyjz/')
    test.run()
