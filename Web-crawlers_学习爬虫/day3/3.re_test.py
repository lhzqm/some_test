# coding:utf-8
import re

data = '北京python27期'
print(re.findall('python27', data))
print(re.findall('北京', data))
# 匹配汉字
print(re.findall('[\u4e00-\u9fa5]', data))

print('***')
# .匹配所有
data = 'a\nb'
print(re.findall('a.b', data, re.DOTALL))
print(re.findall('a.b', data, re.S))

print('***')
# \转义
data = 'helloa|bworld'
print(re.findall('a\|b', data))

print('***')
# []
data = 'abc_adc'
print(re.findall('b|d', data))
print(re.findall('[bd]', data))

print('***')
data = 'sdmfbjkjd   46354  sdnkjfhdk 23y875 sdfkjhiw384'
print(re.findall('\d+?', data))
print(re.findall('\D', data))
print(re.findall('\s', data))
print(re.findall('\S', data))
print(re.findall('\w', data))
print(re.findall('\W', data))

print('***')

data = 'a\nb'
pattern = re.compile('a.b', re.S)
print(pattern.findall(data))

# 分组
print(re.findall(r"a(.*)b(c)", "a\nbc", re.DOTALL))
