# coding:utf-8
import requests
import re
import json


class Guokr(object):
    def __init__(self):
        self.url = 'https://www.guokr.com/ask/highlight/?page=1'
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'
        }
        self.file = open('guokr.json', 'w', encoding='utf-8')

    def get_data(self, url):
        response = requests.get(url, headers=self.headers)
        # 解码
        return response.content.decode()

    def parse_data(self, data):
        # <a target="_blank" href="https://www.guokr.com/question/669761/">印度人把男人的生殖器叫林伽，把女人的生殖器叫瑜尼，林伽和瑜尼的交合，便是瑜伽。这是真还是假的</a>
        results = re.findall('<a target="_blank" href="(.*?)">(.*?)</a>', data)

        data_list = []
        for result in results:
            url = result[0]
            title = result[-1]
            if 'class' in url:
                continue

            temp = dict()
            temp['title'] = title
            temp['url'] = url
            data_list.append(temp)

        # 获取下一页
        # <a href="/ask/highlight/?page=2">下一页</a>
        next_url = re.findall('<a href="(/ask/highlight/\?page=\d+)">下一页</a>', data)

        return data_list, next_url

    def save_data(self, data_list):
        for data in data_list:
            json_data = json.dumps(data, ensure_ascii=False) + ',\n'
            self.file.write(json_data)

    def __del__(self):
        self.file.close()

    def run(self):
        # url
        # headers
        url = self.url

        while True:
            # 发送请求，获取响应
            data = self.get_data(url)

            # 从响应中使用正则解析数据
            data_list, next_url = self.parse_data(data)

            # 保存数据
            self.save_data(data_list)

            # 退出循环条件
            if next_url is []:
                break

            # 构建下一页url
            url = 'https://www.guokr.com' + next_url[0]


if __name__ == '__main__':
    guokr = Guokr()
    guokr.run()
