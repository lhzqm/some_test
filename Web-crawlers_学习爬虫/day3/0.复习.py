# coding:utf-8

"""
爬虫的基础知识
    1. post的用法
        requests.post(url,data={})
    2. post的使用场景
        模拟登陆
        对api接口发送请求
    3. requests如何使用代理
        proxies= {'http':'http://ip:port','https':'https://ip:port'}
        proxies= {'http':'http://user:pwd@ip:port','https':'https://user:pwd@ip:port'}
        requests.get(url,proxies=proxies)

    4. 为什么使用代理
        分散请求
        保护自身ip

    5. cookie的使用方法
        headers
            headers = {
                'Cookies' :''
            }

        cookies
            cookies = {'key1':'value1',.....}

            response = requests.get(url,cookies=cookies)

    6. session的使用方法
        创建对象
         session= requests.Session()

         seession.post(url,data={})

    小方法
        requetss.utils.dict_from_cookiejar
        verify=False
        timeout

        proxy_list = [ip:port,ip1:port1]
        proxy = random.choice(proxy_list)
        requests.get('http://www.baidu.com',)

js分析过程
    1.找到js
        initiator
        事件监听
        search all files
    2.分析js
        js语法
    3.模拟js
        用Python实现
"""
