# coding:utf-8
import requests
import json


class Douban(object):

    def __init__(self):
        # self.base_url = 'https://m.douban.com/rexxar/api/v2/subject_collection/filter_tv_american_hot/items?start={}&count=18'
        self.base_url = 'https://m.douban.com/rexxar/api/v2/subject_collection/tv_american/items?start={}&count=18'
        self.offset = 0
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Mobile Safari/537.36',
            'Referer': 'https://m.douban.com/tv/american'
        }
        self.file = open('douban.json', 'w', encoding='utf-8')

    def get_data(self, url):
        response = requests.get(url, headers=self.headers)
        return response.content

    def parse_data(self, data):
        dict_data = json.loads(data)
        # print(dict_data)
        # 取出电视剧列表
        # tv_list = dict_data['subject_collection_items']
        tv_list = dict_data["subject_collection_items"]

        data_list = []
        # 遍历列表，拿出每一部电视剧信息
        for tv in tv_list:
            temp = dict()
            temp['title'] = tv['title']
            temp['url'] = tv['url']
            data_list.append(temp)
        return data_list

    def save_data(self, data_list):

        for data in data_list:
            json_data = json.dumps(data, ensure_ascii=False) + ',\n'

            self.file.write(json_data)

    def __del__(self):
        self.file.close()

    def run(self):
        # 构建url
        # 请求头
        while True:
            url = self.base_url.format(self.offset)
            # 发送请求
            data = self.get_data(url)
            # print(data)

            # 解析响应，得到数据
            data_list = self.parse_data(data)

            # 保存数据
            self.save_data(data_list)
            # 模拟翻页
            self.offset += 18

            # 设置循环退出条件
            if data_list is []:
                break


if __name__ == '__main__':
    douban = Douban()
    douban.run()
