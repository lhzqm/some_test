# coding:utf-8
import json

data = '{"key": "北京python27期"}'
# json转字典 loads 大括号里面必须是双引号
dict_data = json.loads(data)
print(type(dict_data))

# 字典转json dumps
json_data = json.dumps(dict_data, ensure_ascii=False)
print(json_data)

with open('temp1.json', 'w')as f:
    f.write(json_data)

f = open('temp1.json', 'r')
dict_data = json.load(f)
print('***')
print(dict_data)
print(type(dict_data))

ff = open('temp2.json', 'w')
json.dump(dict_data, ff, ensure_ascii=False)
# json.dump(dict_data, ff)
