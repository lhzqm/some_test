import requests
import re
import json


class Kr36(object):
    def __init__(self):
        self.url = 'http://36kr.com/'
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'
        }
        self.file = open('36kr.json', 'w', encoding='utf-8')
        self.base_url = 'http://36kr.com/api/search-column/mainsite?per_page=20&page={}'
        self.offset = 2

    def get_data(self, url):
        response = requests.get(url, headers=self.headers)
        return response.content.decode()

    def parse_data(self, data):
        # json_data = re.findall('<script>var props=({.*?})</script> ', data)[0]
        # <script>window.initialState=({.*?})</script>
        json_data = re.findall('<script>window.initialState=({.*?})</script>', data)[0]

        # print(json_data)

        json_data = json_data.split(',locationnal={')[0]
        with open('temp.json', 'w', encoding='utf-8')as f:
            f.write(json_data)

            # 将json数据转换成Python字典
            dict_data = json.loads(json_data)
            # print(dict_data['homeData']['data']['latestArticle']['data'][0]['post'])
            print(type(dict_data))

            # news_list = dict_data['feedPostsLatest|post']
            news_list = dict_data['homeData']['data']['latestArticle']['data'][0]['post']

            print(news_list)

            data_list = []
            # for news in news_list:
            #
            #     temp = dict()
            #     temp['title'] = news['title']
            #     temp['url'] = 'https://36kr.com/p/' + news['id']
            #     data_list.append(temp)
            #
            # print(data_list)

            return data_list

    def run(self):
        # 首页url
        # headers
        # 发送首页请求
        data = self.get_data(self.url)

        self.parse_data(data)


if __name__ == '__main__':
    kr36 = Kr36()
    kr36.run()
