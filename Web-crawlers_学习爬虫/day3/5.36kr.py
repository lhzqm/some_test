# coding:utf-8
import requests
import re
import json


class Kr36(object):
    def __init__(self):
        self.url = 'http://36kr.com/'
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'
        }
        self.file = open('36kr.json', 'w', encoding='utf-8')
        self.base_url = 'http://36kr.com/api/search-column/mainsite?per_page=20&page={}'
        self.offset = 2

    def get_data(self, url):
        response = requests.get(url, headers=self.headers)
        return response.content.decode()

    def parse_data(self, data):
        # json_data = re.findall('<script>var props=({.*?})</script> ', data)[0]
        # <script>window.initialState=({.*?})</script>
        json_data = re.findall('<script>window.initialState=({.*?})</script>', data)[0]

        print(json_data)

        # json_data = json_data.split(',locationnal={')[0]

        with open('temp.json', 'w', encoding='utf-8')as f:
            f.write(json_data)

        # 将json数据转换成Python字典
        dict_data = json.loads(json_data)

        # news_list = dict_data['feedPostsLatest|post']
        news_list = dict_data['homeData']['data']['latestArticle']['data']

        data_list = []
        for news in news_list:
            temp = dict()
            temp['title'] = news['title']
            temp['url'] = 'https://36kr.com/p/' + news['id']
            data_list.append(temp)

        print(data_list)
        return data_list

    def save_data(self, data_list):
        for data in data_list:
            json_data = json.dumps(data, ensure_ascii=False) + ',\n'
            self.file.write(json_data)

    def parse_ajax_data(self, data):
        dict_data = json.loads(data)

        news_list = dict_data['data']['items']

        data_list = []
        for news in news_list:
            temp = dict()
            temp['title'] = news['title']
            temp['url'] = news['cover']
            data_list.append(temp)

        return data_list

    def __del__(self):
        self.file.close()

    def run(self):
        # 首页url
        # headers
        # 发送首页请求
        data = self.get_data(self.url)
        # 正则解析首页
        data_list = self.parse_data(data)
        # 保存
        self.save_data(data_list)

        # 循环
        while True:
            # 构建ajaxurl
            url = self.base_url.format(self.offset)
            # 发送请求
            data = self.get_data(url)
            # 解析
            data_list1 = self.parse_ajax_data(data)
            # 保存
            self.save_data(data_list1)
            # 判断是否结尾
            if data_list1 == []:
                break
            # 模拟翻页
            self.offset += 1


if __name__ == '__main__':
    kr36 = Kr36()
    kr36.run()
