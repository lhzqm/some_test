# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TencentItem(scrapy.Item):
    # define the fields for your item here like:
    # 职位名(各个字段的注释在工作的时候应该详细)
    name = scrapy.Field()
    link = scrapy.Field()
    category = scrapy.Field()
    num = scrapy.Field()
    address = scrapy.Field()
    pub_time = scrapy.Field()


class TencentItemPlus(scrapy.Item):
    # define the fields for your item here like:
    # 职位名(各个字段的注释在工作的时候应该详细)
    name = scrapy.Field()
    link = scrapy.Field()
    category = scrapy.Field()
    num = scrapy.Field()
    address = scrapy.Field()
    pub_time = scrapy.Field()
    # 添加岗位需求和工作内容
    require = scrapy.Field()
    duty = scrapy.Field()
