# -*- coding: utf-8 -*-
import scrapy
from Tencent.items import TencentItemPlus


class TencentSpider(scrapy.Spider):
    name = 'tencentplus'
    # 检查允许的域
    allowed_domains = ['tencent.com']
    # 检查修改起始的url
    start_urls = ['https://hr.tencent.com/position.php']

    def parse(self, response):
        # 获取职位节点列表
        node_list = response.xpath("//tr[@class='even']|//tr[@class='odd']")
        # print('$$$$$',len(node_list))

        # 编列职位节点列表，从每一个职位节点中抽取数据
        for node in node_list:
            # 实例化一个item容器，用于存储数据
            item = TencentItemPlus()

            # 抽取数据，将数据存入item中
            item['name'] = node.xpath('./td[1]/a/text()').extract_first()
            item['link'] = 'https://hr.tencent.com/' + node.xpath('./td[1]/a/@href').extract_first()
            item['category'] = node.xpath('./td[2]/text()').extract_first()
            item['num'] = node.xpath('./td[3]/text()').extract_first()
            item['address'] = node.xpath('./td[4]/text()').extract_first()
            item['pub_time'] = node.xpath('./td[5]/text()').extract_first()

            # 停止yield数据，防止数据直接返回给引擎，然后无法操作
            # yield item
            # 构建详情页面的请求，并发送给引擎
            yield scrapy.Request(item['link'], callback=self.parse_detail, meta={"heima": item})

        # 提取下一页url
        next_url = response.xpath('//a[@id="next"]/@href').extract()[0]
        if next_url != 'javascript:;':
            # 构建下一页url
            next_url = 'https://hr.tencent.com/' + next_url

            # 创建成请求发送给引擎,创建请求的时候，callback执行该url对应的响应有谁来解析
            yield scrapy.Request(next_url, callback=self.parse)

    # 定义一个新的解析方法，用于解析详情页面响应
    def parse_detail(self, response):
        item = response.request.meta['heima']

        item['duty'] = ''.join(response.xpath('//tr[3]/td/ul/li/text()').extract())
        item['require'] = ''.join(response.xpath('//tr[4]/td/ul/li/text()').extract())

        yield item
