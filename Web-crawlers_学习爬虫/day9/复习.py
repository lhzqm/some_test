"""
反馈:

1.spider类不是很清楚

复习.py
1 scrapy框架结构

2 scrapy各模块功能
    引擎
        负责各模块之间通信和数据交互
    爬虫
        起始的请求
        解析响应返回数据和新的请求
    调度器
        put
        get
    下载器
        获取请求对饮的响应
    item管道
        数据保存

3 scrapy shell调试
    3.1 主要用来干什么
        测试站点
        测试xpath，css是否能够提取到数据
    3.2 如何加载请求头
        scrapy shell -s USER_AGENT=''

4 scrapy开发流程
    4.0 开发流程
        1.创建项目
            scrapy startproject projectname
        2.明确目标
            items.py建模
            name = scrapy.Filed()

        3.创建爬虫
            创建
                命令
                    scrapy genspider name domain

                手动 spiders
                    name
                    allowed_domains
                    start_urls

                    parse
            编写
                检查start_urls
                检查allowed_domains
                编写parse
        4.保存内容
            pipelines.py
                process_item
                    return item
            settings文件中注册管道类


    4.1 明确目标需要做什么，这么做有什么好处
        items.py建模
        好处:
            模型类实例存储数据更安全
            项目爬取那些数据更清洗

    4.2 创建爬虫需要用到的参数，需要关注的事情


    4.3 如何保存内容
"""
