# coding:utf-8
import requests

# url = 'https://www.baidu.com/s?wd=%E7%9F%A5%E4%B9%8E'
url = 'https://www.baidu.com/s'

# 构建请求头·
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36"
}

data = input('key=')
# 构建参数字典
params = {
    "wd": data
}

# response = requests.get(url, headers=headers)
response = requests.get(url, headers=headers, params=params)

# 验证
with open('zhihu1.html', 'w', encoding='utf-8')as f:
    f.write(response.content.decode('utf-8'))
