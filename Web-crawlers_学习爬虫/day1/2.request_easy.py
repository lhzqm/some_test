# coding:utf-8
import requests

# url必须带协议
url = 'http://www.baidu.com'

response = requests.get(url)


print(response)
# # 查看状态码
print(response.status_code)
# # # 查看响应对应的url
print(response.url)
# # # 响应头
print(response.headers)

# # # 响应对应的请求
print(response.request)
print(response.request.headers)


# print(response.text)
# print(response.encoding)
# response.encoding = 'utf-8'
# print(response.text)

# 建议使用content
# print(response.content.decode())
