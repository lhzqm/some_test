# coding:utf-8

import requests

# url = 'http://www.baidu.com'
url = 'https://www.zhihu.com/'

# 构建请求头
headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36"
}

response1 = requests.get(url)
response2 = requests.get(url, headers=headers)

# print(len(response1.content))
# print(len(response2.content))

print(response1.status_code)
print(response2.status_code)

