# coding:utf-8
"""
反馈
    需要复习的前面的知识
    贴吧列表页面要翻页，详情页面需不需要翻页？
    requests是否会获取失败，失败是否要处理？
    如何分析一个网站？
        浏览器  --- url  ---- 开发者工具  ---- network ---- 刷新 ---- 检查首页url中是否含有目标数据 ----- html --- ua，referer，cookies --- re/xpath
                                                                                |- 检查其他数据包  --- json、xml、html    json,xml2dict
复习.py
1 xpath

    1.0 什么是xpath？
        路径选择语法
    1.1 xpath语法
        1.nodename
            /html/head/title
        2.//和/
            //title
        3. .和..
            //title/.
        4.取数据
        <a>data</a>
            //a/text()   结果是:data

        <a href="http://www.baidu.com",id="baidu">data</a>
            //a/@href
            //a/@id
        5.[]是用来干什么的？
            修饰节点
        6.[]有哪些修饰节点的方法?
            1.通过索引修饰节点
                //ul/li[n]
                //ul/li[last()]
                //ul/li[last()-1]
            2.通过属性值修饰节点
                //div[@id="python38"]
            3.通过子节点的值修饰节点
                <span>
                    <i>33</i>
                </span>
                //span[i=33]
            4.包含修饰节点
                //a[text(),'下一页']
                //div[@id,'id_']
        7.通配符
            //*[]/nodename
        8.或
            xpath1|xpath2

    1.2 lxml库的使用
        html = etree.HTML(response.content)
        html.xpath(xpath)

2.queue与threading
    2.1 queue
        1.使用队列的原因?
            线程安全
        2.宣告对队列的操作结束?
            q.task_done()
        3.设置主线程等待队列操作结束再退出?
            q.join()
    2.2 thread
        1.设置守护线程
            t.setDemon(True)
        2.守护线程的特点
            跟随主线程的退出而退出
"""
