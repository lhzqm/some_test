# coding:utf-8
from selenium import webdriver

# 构建无头浏览器对象
# dr = webdriver.PhantomJS()
#
# url = 'http://www.baidu.com'
#
# dr.get(url)
# dr.save_screenshot('baidu.png')


# 部署爬虫，部署在服务器上，服务器的linux大部分是服务器班的操作系统，不支持有界面的浏览器，此时使用无头浏览器
# phantomjs树大招风


# chrome浏览器是最新版
# selenium比较新
# 浏览器驱动与浏览器匹配

# 创建浏览器配置对象
opt = webdriver.ChromeOptions()
# 对配置对象添加设置，设置为无头模式
opt.add_argument('headless')

# 创建浏览器对象
driver = webdriver.Chrome(chrome_options=opt)

url = 'http://www.baidu.com'

driver.get(url)
driver.save_screenshot('chrome_baidu.png')

# 渲染之后的源码，浏览器自动加载运行之后，所有的数据已经齐全
# print(driver.page_source)
# print(driver.title)
# print(driver.current_url)
# print(driver.get_cookies())
