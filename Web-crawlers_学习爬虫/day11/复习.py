# coding:utf-8
"""
反馈信息
1.发送post请求时, 怎么才能判断携带的post_data需要添加像有道词典案例那样的复杂的参数?

2.电影天堂提取电影下载地址时,下载地址提取不到

3.联系方式
  dark_sylphid

复习

0.mongodb在scrapy中的使用
  配置信息放在settings
  --1 from scrapy.conf import settings
  host = settings['MONGO_HOST']

1 反爬措施
    1.1 服务器端为什么要做反爬
      降低负载
      保护数据
    1.2 反爬的几个方向，举例
        1.识别用户身份
          ua
          cookies
          referer
          验证码
        2.分析用户行为
          ip cookies  并发量
          在线时间    定时休眠
          蜜罐
          基于机器学习
          mysql
        3.动态数据加载
          ajax，js

2 scrapy反反爬与下载器中间件
    2.1 设置user-agent的方式，优先级
        settings
          2个设置位置
        创建请求的时候
          scrapy.Request(url,callback, headers={})
        下载器中间件
          middlewares.py


    2.2 如何设置请求延迟,效果
      downloaddelay=3
    2.3 如何设置不带cookies请求
      cookies-enable=False
    2.4 ip代理池
      代理商
        免费
        付费
      自建
        云主机
        vps
        adsl拨号
      crawlera
        settings配置

    2.5 下载器中间的功能
        全局修改请求与响应
    2.6 下载器中间的编写方法
      middlewares.py

      class middlewares(object):
          def process_reqeust(self, request, spdier)

      settings注册

3 手机抓包
    pc 开wifi 查看IP地址 打开fiddler 设置之抓取远程机器的数据
    手机端 链接wifi  配置wifi 设置wifi代理 ip&port
    手机端操作app产生流量
"""
