# -*- coding: utf-8 -*-
import scrapy
from JD.items import JdItem
import json
# ----1 导入爬虫类
from scrapy_redis.spiders import RedisSpider


# ----2 修改类的继承
class BookSpider(RedisSpider):
    name = 'book'

    # ----3 注销起始的url和允许的域
    # allowed_domains = ['jd.com', '3.cn']
    # # 修改起始的url
    # start_urls = ['https://book.jd.com/booksort.html']

    # ----4 动态获取允许的域
    def __init__(self, *args, **kwargs):
        # Dynamically define the allowed domains list.
        domain = kwargs.pop('domain', '')
        # 需要强转成列表
        self.allowed_domains = list(filter(None, domain.split(',')))
        # 需要修改类
        super(BookSpider, self).__init__(*args, **kwargs)

    # ----5 设置redis_key
    redis_key = 'python27'

    def parse(self, response):

        # 获取大分类节点列表
        big_node_list = response.xpath('//*[@id="booksort"]/div[2]/dl/dt/a')
        # print(len(big_node_list))

        # 遍历大分类列表
        for big_node in big_node_list[0:1]:
            big_category = big_node.xpath('./text()').extract_first()
            big_category_link = 'https:' + big_node.xpath('./@href').extract_first()

            # 获取小分类节点列表
            small_node_list = big_node.xpath('../following-sibling::dd[1]/em/a')
            for small_node in small_node_list[0:1]:
                temp_data = dict()
                temp_data['small_category'] = small_node.xpath('./text()').extract_first()
                temp_data['small_category_link'] = 'https:' + small_node.xpath('./@href').extract_first()
                temp_data['big_category'] = big_category
                temp_data['big_category_link'] = big_category_link

                yield scrapy.Request(
                    url=temp_data['small_category_link'],
                    callback=self.parse_book_list,
                    meta={'meta_1': temp_data}
                )

    def parse_book_list(self, response):
        temp_data = response.request.meta['meta_1']

        # 获取图书节点列表
        book_list = response.xpath('//*[@id="plist"]/ul/li/div')

        # 遍历图书节点列表
        for book in book_list[0:1]:
            # 创建item实例
            item = JdItem()

            # 抽取数据，并保存
            item['big_category'] = temp_data['big_category']
            item['big_category_link'] = temp_data['big_category_link']
            item['small_category'] = temp_data['small_category']
            item['small_category_link'] = temp_data['small_category_link']

            item['book'] = book.xpath('./div[3]/a/em/text()').extract_first()
            item['author'] = book.xpath('./div[4]/span[1]/span/a/text()').extract_first()
            item['publisher'] = book.xpath('./div[4]/span[2]/a/text()').extract_first()
            item['pub_date'] = book.xpath('./div[4]/span[3]/text()').extract_first()
            item['cover'] = book.xpath('./div[1]/a/img/@src|./div[1]/a/img/@data-lazy-img').extract_first()
            try:
                item['detail_link'] = 'https:' + book.xpath('./div[1]/a/@href').extract_first()
            except:
                item['detail_link'] = None

            # 获取skuid
            skuid = book.xpath('./@data-sku').extract_first()
            if skuid != None:
                # 拼接价格url
                price_url = 'https://p.3.cn/prices/mgets?skuIds=J_' + str(skuid)
                yield scrapy.Request(
                    url=price_url,
                    callback=self.parse_price,
                    meta={'meta_2': item}
                )

    def parse_price(self, response):
        # 接收meta传参
        item = response.meta['meta_2']

        dict_data = json.loads(response.body)

        item['price'] = dict_data[0]['op']

        yield item
