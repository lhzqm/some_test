# coding:utf-8
import redis
from pymongo import MongoClient
import json

# 链接数据库
# cli = redis.Redis(host='47.92.159.134', port=6379, db=0)

mongo_cli = MongoClient('47.92.159.134', port=27017)
# print(mongo_cli)
db = mongo_cli['DMOZ']
print(db)
col = db['dmoz']
print(col)
# dict_data = json.loads({'123': 123})
col.insert({'123': 123})
# while True:
#     # 从redis中读取
#     source, data = cli.blpop(['dmoz:items'])
#     print(source, data)
#
#     # 写入到mongodb中
#     dict_data = json.loads(data)
#     col.insert(dict_data)
