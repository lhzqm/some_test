# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from Sun.items import SunItem

class DongguanSpider(CrawlSpider):
    name = 'dongguan'
    # 修改允许的域
    allowed_domains = ['sun0769.com']
    # 修改起始的url
    start_urls = ['http://wz.sun0769.com/index.php/question/questionType?type=4']

    # 编写规则
    rules = (
        # # 详情页面链接'html/question/201806/373992.shtml'
        Rule(LinkExtractor(allow=r'html/question/\d+/\d+.shtml'), callback='parse_item'),
        # 列表页面链接 'index.php/question/questionType?type=4&page=120'
        Rule(LinkExtractor(allow=r'index.php/question/questionType\?type=4&page=\d+'), follow=True),
    )

    def parse_item(self, response):
        # 构建item实例
        item = SunItem()

        # 抽取数据
        item['title'] = response.xpath('//title/text()').extract_first().split('_')[0]
        item['id'] = response.xpath('/html/body/div[6]/div/div[1]/div[1]/strong/text()').extract_first().split(':')[-1]
        item['url'] = response.url
        item['content'] = response.xpath('//meta[@name="description"]/@content').extract_first()[0]

        # 将数据返回
        yield item