# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ZhiyoujiItem(scrapy.Item):
    # define the fields for your item here like:
    # 记录数据来源
    data_source = scrapy.Field()
    # 记录数据采集时间
    timestamp = scrapy.Field()

    company_name = scrapy.Field()
    views = scrapy.Field()
    info = scrapy.Field()
    industry = scrapy.Field()
    desc = scrapy.Field()

    dianping = scrapy.Field()
    salary = scrapy.Field()
    news = scrapy.Field()
    address = scrapy.Field()
    contact = scrapy.Field()

    pass
