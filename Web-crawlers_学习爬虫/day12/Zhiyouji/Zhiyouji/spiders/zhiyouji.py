# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from Zhiyouji.items import ZhiyoujiItem
import time
# ----1 导入类
from scrapy_redis.spiders import RedisCrawlSpider


# 修改类的继承
class ZhiyoujiSpider(RedisCrawlSpider):

    name = 'zhiyouji'

    # 注销
    # # 检查修改允许的域
    allowed_domains = ['jobui.com']
    # # 检查修改起始的url
    # start_urls = ['http://www.jobui.com/cmp?area=%E5%85%A8%E5%9B%BD&keyword=']
    # # start_urls = ['http://www.jobui.com/cmp?area=%E5%85%A8%E5%9B%BD&keyword=%E6%8B%89%E6%89%8E%E6%96%AF%E7%BD%91%E7%BB%9C%E7%A7%91%E6%8A%80%28%E4%B8%8A%E6%B5%B7%29%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8']

    # 设置redis——key
    redis_key = 'zhiyouji'

    rules = (
        # /cmp?area=全国&n=3#listInter
        Rule(LinkExtractor(allow=r'/cmp\?area=%E5%85%A8%E5%9B%BD&n=\d+#listInter'), follow=True),
        # /company/10500000/
        Rule(LinkExtractor(allow=r'/company/\d+/$'), callback='parse_item'),
    )

    def parse_item(self, response):
        # print('--------',response.url)

        # 构建item实例
        item = ZhiyoujiItem()

        # 抽取数据
        item['timestamp'] = time.time()
        item['data_source'] = response.url

        item['company_name'] = response.xpath('//*[@id="companyH1"]/a/text()').extract_first()
        item['views'] = response.xpath('//div[@class="fl ele fs16 gray9 mr10"]/text()').extract_first().strip().split('人')[0]
        #
        item['info'] = response.xpath('//*[@id="cmp-intro"]/div/div/dl/dd[1]/text()').extract_first()
        item['industry'] = response.xpath('//*[@id="cmp-intro"]/div/div/dl/dd[2]/a/text()').extract()
        item['desc'] = ''.join(response.xpath('//*[@id="textShowMore"]/text()').extract())
        #
        item['dianping'] = response.xpath('//div[@class="swf-contA"]/div/h3/text()').extract_first()
        item['salary'] = response.xpath('//div[@class="swf-contB"]/div/h3/text()').extract_first()
        #
        node_list = response.xpath('//ul[@class="discuss-box"]/li')
        data_list = []
        for node in node_list:
            temp = {}
            temp['title'] = node.xpath('./h3/a/text()').extract_first()
            temp['content'] = node.xpath('./p/text()').extract_first()
            temp['source'] = node.xpath('./div/span/text()').extract_first()
            data_list.append(temp)

        item['news'] = data_list

        item['address'] = response.xpath('//dl[@class="dlli fs16"]/dd[1]/text()').extract_first()
        item['contact'] = response.xpath('//dl[@class="dlli fs16"]/div[1]/dd/text()').extract_first().replace('\xa0','')

        # print(item)
        # 返回
        yield item
