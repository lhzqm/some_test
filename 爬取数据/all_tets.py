import requests

url = 'https://www.ncbi.nlm.nih.gov/pathogens/ngram?'

# 构建请求头

headers = {
    "x-requested-with": "XMLHttpRequest",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36",
    "sec-fetch-site": "same-origin",
    "sec-fetch-mode": "cors",
    "referer": "https://www.ncbi.nlm.nih.gov/pathogens/isolates",
    "origin": "https://www.ncbi.nlm.nih.gov",
    "ncbi-phid": "322C34D37A4EBFC500004D577699F804.1.m_1.02",
    "cookie": "ncbi_sid=5AAB62E5DB82F161_0000SID; _ga=GA1.2.1451510357.1572351775; _ga=GA1.4.1451510357.1572351775; _gid=GA1.2.1891216322.1582620145; ncbi_pinger=N4IgDgTgpgbg+mAFgSwCYgFwgMwCZcDC2ALACLYDsAgsQKIBCAYgQKwAMHbhL2VAnOza0AbMIB0ARjEBbOBJABfIA===; _gat=1; _gat_dap=1",
    "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
    "content-length": "1058",
    "authority": "www.ncbi.nlm.nih.gov",
    "method": "POST",
    "path": "/pathogens/ngram?",
    "scheme": "https",
    "accept": "application/json, text/javascript, */*; q=0.01",
    "accept-encoding": "gzip, deflate, br",
    "accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
}
data = {
    "start": 0,
    "limit": 20,
    "q": '[display(),hist(geo_loc_name,isolation_source,epi_type,collected_by,host,property,target_creation_date)].from(pathogen).usingschema(/schema/pathogen).matching(kmer_group==["PDG000000065.8","PDG000000014.64","PDG000000064.15","PDG000000068.8","PDG000000043.60","PDG000000040.29","PDG000000067.10","PDG000000061.36","PDG000000059.26","PDG000000045.126","PDG000000032.150","PDG000000034.144","PDG000000010.359","PDG000000039.176","PDG000000030.167","PDG000000016.188","PDG000000058.43","PDG000000020.94","PDG000000042.132","PDG000000036.267","PDG000000026.92","PDG000000023.324","PDG000000055.97","PDG000000021.140","PDG000000028.276","PDG000000001.1644","PDG000000012.605","PDG000000004.1770","PDG000000003.1025","PDG000000002.1751"]).sort(target_creation_date,desc)',
    "_search": "false",
    "rows": 20,
    "page": 1,
    "sidx": "target_creation_date",
    "sord": "desc"
}

response = requests.post(url, data=data, headers=headers)

print(response.status_code)
# # # 响应头
print(response.headers)

# # # 响应对应的请求
print(response.request)
print(response.request.headers)
with open('all_test.html', 'w', encoding='utf-8')as f:
    f.write(response.content.decode('utf-8'))
