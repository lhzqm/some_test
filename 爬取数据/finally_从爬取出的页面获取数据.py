import os
import time
import json
import requests
import collections



class Test(object):
    def __init__(self, url):
        # 构建列表页面
        self.url = url
        # 构建headers
        self.headers = {
            "x-requested-with": "XMLHttpRequest",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36",
            "sec-fetch-site": "same-origin",
            "sec-fetch-mode": "cors",
            "referer": "https://www.ncbi.nlm.nih.gov/pathogens/isolates",
            "origin": "https://www.ncbi.nlm.nih.gov",
            "ncbi-phid": "322C34D37A4EBFC500004D577699F804.1.m_1.02",
            "cookie": "ncbi_sid=5AAB62E5DB82F161_0000SID; _ga=GA1.2.1451510357.1572351775; _ga=GA1.4.1451510357.1572351775; _gid=GA1.2.1891216322.1582620145; ncbi_pinger=N4IgDgTgpgbg+mAFgSwCYgFwgMwCZcDC2ALACLYDsAgsQKIBCAYgQKwAMHbhL2VAnOza0AbMIB0ARjEBbOBJABfIA===; _gat=1; _gat_dap=1",
            "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            "content-length": "1058",
            "authority": "www.ncbi.nlm.nih.gov",
            "method": "POST",
            "path": "/pathogens/ngram?",
            "scheme": "https",
            "accept": "application/json, text/javascript, */*; q=0.01",
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
        }
        self.file = open('finally_result.txt', 'w', encoding='utf-8')

    def get_data(self, url, start=0, page=1):
        data = {
            "start": start,  # 0
            "limit": 200,
            "q": '[display(),hist(geo_loc_name,isolation_source,epi_type,collected_by,host,property,target_creation_date)].from(pathogen).usingschema(/schema/pathogen).matching(kmer_group==["PDG000000065.8","PDG000000014.64","PDG000000064.15","PDG000000068.8","PDG000000043.60","PDG000000040.29","PDG000000067.10","PDG000000061.36","PDG000000059.26","PDG000000045.126","PDG000000032.150","PDG000000034.144","PDG000000010.359","PDG000000039.176","PDG000000030.167","PDG000000016.188","PDG000000058.43","PDG000000020.94","PDG000000042.132","PDG000000036.267","PDG000000026.92","PDG000000023.324","PDG000000055.97","PDG000000021.140","PDG000000028.276","PDG000000001.1644","PDG000000012.605","PDG000000004.1770","PDG000000003.1025","PDG000000002.1751"]).sort(target_creation_date,desc)',
            "_search": "false",
            "rows": 200,
            "page": page,  # 1
            "sidx": "target_creation_date",
            "sord": "desc"
        }
        response = requests.post(url, data=data, headers=self.headers)
        return response.content

    def parse_list_page(self, data):
        dict_data = json.loads(data)
        data_set = dict_data["ngout"]["data"]["content"]
        return data_set

    def __del__(self):
        self.file.close()

    def run(self):
        # 循环
        # 发起列表页面请求
        start = 0
        page = 1
        self.file.write(
            "Organism_Group" + "\t" +
            "Strain" + "\t" +
            "Serovar" + "\t" +
            "Isolate" + "\t" +
            "Create_Dat" + "\t" +
            "Location" + "\t" +
            "Isolation_So" + "\t" +
            "Isolation_type" + "\t" +
            "Host" + "\t" +
            "SNP_cluster" + "\t" +
            "Min_same" + "\t" +
            "Min_diff" + "\t" +
            "BioSample" + "\t" +
            "Assembly " + "\t" +
            "AMR_genotypes" + "\t" +
            "K-mer_group" + "\n"
        )
        while True:

            os.listdir("./")

            data = self.get_data(self.url, start, page)
            page = page + 1
            start = (page - 1) * 200
            if page == 2605:
                break

            print(page)

            with open('./page2/{}_page.html'.format(page), 'wb')as f:
                f.write(data)

            # quit()

            # finally_result
            data_set = self.parse_list_page(data)

            for data in data_set:
                self.file.write(
                    (data["taxgroup_name"] if data.get("taxgroup_name", []) else "null") + "\t" +
                    (data["strain"] if data.get("strain", []) else "null") + "\t" +
                    (data["serovar"] if data.get("serovar", []) else "null") + "\t" +
                    (data["target_acc"] if data.get("target_acc", []) else "null") + "\t" +
                    (data["creation_date"] if data.get("creation_date", []) else "null") + "\t" +
                    (data["geo_loc_name"] if data.get("geo_loc_name", []) else "null") + "\t" +
                    (data["isolation_source"] if data.get("isolation_source", []) else "null") + "\t" +
                    (data["epi_type"] if data.get("epi_type", []) else "null") + "\t" +
                    (data["host"] if data.get("host", []) else "null") + "\t" +
                    (data["erd_group"] if data.get("erd_group", []) else "null") + "\t" +
                    (str(data["minsame"]) if data.get("minsame", []) else "null") + "\t" +
                    (str(data["mindiff"]) if data.get("mindiff", []) else "null") + "\t" +
                    (data["biosample_acc"] if data.get("biosample_acc", []) else "null") + "\t" +
                    (data["asm_acc"] if data.get("asm_acc", []) else "null") + "\t" +
                    (",".join(data["AMR_genotypes"]) if data.get("AMR_genotypes", []) else "null") + "\t" +
                    (data["kmer_group"] if data.get("kmer_group", []) else "null") + "\n"
                )


if __name__ == '__main__':
    test = Test('https://www.ncbi.nlm.nih.gov/pathogens/ngram?')
    test.run()
