import time
import json
import requests
import socket

socket.setdefaulttimeout(20)  # 设置socket层的超时时间为20秒


class Test(object):
    def __init__(self, url):
        # 构建列表页面
        self.url = url
        # 构建headers
        self.headers = {
            "x-requested-with": "XMLHttpRequest",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36",
            "sec-fetch-site": "same-origin",
            "sec-fetch-mode": "cors",
            "referer": "https://www.ncbi.nlm.nih.gov/pathogens/isolates",
            "origin": "https://www.ncbi.nlm.nih.gov",
            "ncbi-phid": "322C34D37A4EBFC500004D577699F804.1.m_1.02",
            "cookie": "ncbi_sid=5AAB62E5DB82F161_0000SID; _ga=GA1.2.1451510357.1572351775; _ga=GA1.4.1451510357.1572351775; _gid=GA1.2.1891216322.1582620145; ncbi_pinger=N4IgDgTgpgbg+mAFgSwCYgFwgMwCZcDC2ALACLYDsAgsQKIBCAYgQKwAMHbhL2VAnOza0AbMIB0ARjEBbOBJABfIA===; _gat=1; _gat_dap=1",
            "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            "content-length": "1058",
            "authority": "www.ncbi.nlm.nih.gov",
            "method": "POST",
            "path": "/pathogens/ngram?",
            "scheme": "https",
            "accept": "application/json, text/javascript, */*; q=0.01",
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
        }
        self.file = open('results.txt', 'w', encoding='utf-8')

    def get_data(self, url, start=0, page=1):
        data = {
            "start": start,  # 0
            "limit": 200,
            "q": '[display(),hist(geo_loc_name,isolation_source,epi_type,collected_by,host,property,target_creation_date)].from(pathogen).usingschema(/schema/pathogen).matching(kmer_group==["PDG000000065.8","PDG000000014.64","PDG000000064.15","PDG000000068.8","PDG000000043.60","PDG000000040.29","PDG000000032.150","PDG000000034.144","PDG000000010.359","PDG000000058.43","PDG000000055.97","PDG000000045.127","PDG000000004.1774","PDG000000061.37","PDG000000067.11","PDG000000021.141","PDG000000042.133","PDG000000023.325","PDG000000026.93","PDG000000030.169","PDG000000020.96","PDG000000059.27","PDG000000016.189","PDG000000039.177","PDG000000072.2","PDG000000071.1","PDG000000002.1754","PDG000000036.268","PDG000000003.1030","PDG000000028.279","PDG000000012.609","PDG000000001.1652"]).sort(target_creation_date,desc)',
            "_search": "false",
            "rows": 200,
            "page": page,  # 1
            "sidx": "target_creation_date",
            "sord": "desc"
        }
        response = requests.post(url, data=data, headers=self.headers)
        response.close()
        time.sleep(1)
        return response.content

    def parse_list_page(self, data):
        dict_data = json.loads(data)
        data_set = dict_data["ngout"]["data"]["content"]
        return data_set

    def __del__(self):
        self.file.close()

    def run(self):
        # 循环
        # 发起列表页面请求
        self.file.write(
            "Organism_Group" + "\t" +
            "Strain" + "\t" +
            "Serovar" + "\t" +
            "Isolate" + "\t" +
            "Create_Dat" + "\t" +
            "Location" + "\t" +
            "Isolation_So" + "\t" +
            "Isolation_type" + "\t" +
            "Host" + "\t" +
            "SNP_cluster" + "\t" +
            "Min_same" + "\t" +
            "Min_diff" + "\t" +
            "BioSample" + "\t" +
            "Assembly " + "\t" +
            "AMR_genotypes" + "\t" +
            "K-mer_group" + "\t" +
            "AMRFinderPlus analysis type" + "\t" +
            "AMRFinderPlus version" + "\t" +
            "Level" + "\t" +
            "N50" + "\t" +
            "Length" + "\t" +
            "Contigs" + "\t" +
            "Method" + "\t" +
            "BioProject" + "\t" +
            "Collected by" + "\t" +
            "Collection Date" + "\t" +
            "Host Disease" + "\t" +
            "Lat/Lon" + "\t" +
            "Library Layout" + "\t" +
            "AST phenotypes" + "\t" +
            "Stress genotypes" + "\t" +
            "Virulence genotypes" + "\t" +
            "Outbreak" + "\t" +
            "PFGE Primary Enzyme Pattern" + "\t" +
            "PFGE Secondary Enzyme Pattern" + "\t" +
            "Platform" + "\t" +
            "PD Ref Gene Catalog version" + "\t" +
            "Run" + "\t" +
            "Scientific name" + "\t" +
            "Species TaxID" + "\t" +
            "SRA Center" + "\t" +
            "SRA Release Date" + "\t" +
            "TaxID" + "\t" +
            "WGS Prefix" + "\t" +
            "WGS Accession" + "\n"
        )

        page = 1
        start = 0
        while True:
            print(page)
            # time.sleep(1)
            if page == 2653:
                break
            data = self.get_data(self.url, start, page)

            with open('./page6/{}'.format(page), 'wb')as f:
                f.write(data)

            page = page + 1
            start = (page - 1) * 200

            # finally_result
            data_set = self.parse_list_page(data)

            for data in data_set:
                self.file.write(
                    (data["taxgroup_name"] if data.get("taxgroup_name", []) else "null") + "\t" +
                    (data["strain"] if data.get("strain", []) else "null") + "\t" +
                    (data["serovar"] if data.get("serovar", []) else "null") + "\t" +
                    (data["target_acc"] if data.get("target_acc", []) else "null") + "\t" +
                    (data["creation_date"] if data.get("creation_date", []) else "null") + "\t" +
                    (data["geo_loc_name"] if data.get("geo_loc_name", []) else "null") + "\t" +
                    (data["isolation_source"] if data.get("isolation_source", []) else "null") + "\t" +
                    (data["epi_type"] if data.get("epi_type", []) else "null") + "\t" +
                    (data["host"] if data.get("host", []) else "null") + "\t" +
                    (data["erd_group"] if data.get("erd_group", []) else "null") + "\t" +
                    (str(data["minsame"]) if data.get("minsame", []) else "null") + "\t" +
                    (str(data["mindiff"]) if data.get("mindiff", []) else "null") + "\t" +
                    (data["biosample_acc"] if data.get("biosample_acc", []) else "null") + "\t" +
                    (data["asm_acc"] if data.get("asm_acc", []) else "null") + "\t" +
                    (",".join(data["AMR_genotypes"]) if data.get("AMR_genotypes", []) else "null") + "\t" +
                    (data["kmer_group"] if data.get("kmer_group", []) else "null") + "\t" +
                    (data["amrfinderplus_analysis_type"] if data.get("amrfinderplus_analysis_type",
                                                                     []) else "null") + "\t" +
                    (str(data["amrfinderplus_version"]) if data.get("amrfinderplus_version", []) else "null") + "\t" +
                    (data["asm_level"] if data.get("asm_level", []) else "null") + "\t" +
                    (str(data["asm_stats_length_bp"]) if data.get("asm_stats_length_bp", []) else "null") + "\t" +
                    (str(data["asm_stats_contig_n50"]) if data.get("asm_stats_contig_n50", []) else "null") + "\t" +
                    (str(data["asm_stats_n_contig"]) if data.get("asm_stats_n_contig", []) else "null") + "\t" +
                    (data["assembly_method"] if data.get("assembly_method", []) else "null") + "\t" +
                    (data["bioproject_acc"] if data.get("bioproject_acc", []) else "null") + "\t" +
                    (data["collected_by"] if data.get("collected_by", []) else "null") + "\t" +
                    (data["collection_date"] if data.get("collection_date", []) else "null") + "\t" +
                    (data["host_disease"] if data.get("host_disease", []) else "null") + "\t" +
                    (data["LibraryLayout"] if data.get("LibraryLayout", []) else "null") + "\t" +
                    "null" + "\t" +  # todo没有 AST phenotypes
                    (",".join(data["stress_genotypes"]) if data.get("stress_genotypes", []) else "null") + "\t" +
                    (",".join(data["virulence_genotypes"]) if data.get("virulence_genotypes", []) else "null") + "\t" +
                    "null" + "\t" +  # Outbreak
                    "null" + "\t" +  # PFGE Primary Enzyme Pattern
                    "null" + "\t" +  # PFGE Secondary Enzyme Pattern
                    (data["Platform"] if data.get("Platform", []) else "null") + "\t" +
                    (data["refgene_db_version"] if data.get("refgene_db_version", []) else "null") + "\t" +
                    (data["Run"] if data.get("Run", []) else "null") + "\t" +
                    (data["scientific_name"] if data.get("scientific_name", []) else "null") + "\t" +
                    (str(data["species_taxid"]) if data.get("species_taxid", []) else "null") + "\t" +
                    (data["sra_center"] if data.get("sra_center", []) else "null") + "\t" +
                    (data["sra_release_date"] if data.get("sra_release_date", []) else "null") + "\t" +
                    (str(data["taxid"]) if data.get("taxid", []) else "null") + "\t" +
                    (data["wgs_acc_prefix"] if data.get("wgs_acc_prefix", []) else "null") + "\t" +
                    (data["wgs_master_acc"] if data.get("wgs_master_acc", []) else "null") + "\n"
                )


if __name__ == '__main__':
    test = Test('https://www.ncbi.nlm.nih.gov/pathogens/ngram?')
    test.run()
