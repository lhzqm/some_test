import json

data = open('all_test.html', 'r', encoding='utf-8')
data = data.read()
dict_data = json.loads(data)

data_set = dict_data["ngout"]["data"]["content"]

# print(data_set)
print(len(data_set))

with open('result.txt', 'w', encoding='utf-8')as f:
    f.write(
        "Organism_Group" + "\t" +
        "Strain" + "\t" +
        "Serovar" + "\t" +
        "Isolate" + "\t" +
        "Create_Dat" + "\t" +
        "Location" + "\t" +
        "Isolation_So" + "\t" +
        "Isolation_type" + "\t" +
        "Host" + "\t" +
        "SNP_cluster" + "\t" +
        "Min_same" + "\t" +
        "Min_diff" + "\t" +
        "BioSample" + "\t" +
        "Assembly " + "\t" +
        "AMR_genotypes" + "\t" +
        "K-mer_group" + "\n"
    )
    for data in data_set:
        f.write(
            (data["taxgroup_name"] if data.get("taxgroup_name", []) else "null") + "\t" +
            (data["strain"] if data.get("strain", []) else "null") + "\t" +
            (data["serovar"] if data.get("serovar", []) else "null") + "\t" +
            (data["target_acc"] if data.get("target_acc", []) else "null") + "\t" +
            (data["creation_date"] if data.get("creation_date", []) else "null") + "\t" +
            (data["geo_loc_name"] if data.get("geo_loc_name", []) else "null") + "\t" +
            (data["isolation_source"] if data.get("isolation_source", []) else "null") + "\t" +
            (data["epi_type"] if data.get("epi_type", []) else "null") + "\t" +
            (data["host"] if data.get("host", []) else "null") + "\t" +
            (data["erd_group"] if data.get("erd_group", []) else "null") + "\t" +
            (str(data["minsame"]) if data.get("minsame", []) else "null") + "\t" +
            (str(data["mindiff"]) if data.get("mindiff", []) else "null") + "\t" +
            (data["biosample_acc"] if data.get("biosample_acc", []) else "null") + "\t" +
            (data["asm_acc"] if data.get("asm_acc", []) else "null") + "\t" +
            (",".join(data["AMR_genotypes"]) if data.get("AMR_genotypes", []) else "null") + "\t" +
            (data["kmer_group"] if data.get("kmer_group", []) else "null") + "\n"
        )

# print(data_set)
# print(dict_data["ngout"]["data"]["content"])
#
# a = {'id': 'PDG000000002.1750_PDT000688270.1',
#      'LibraryLayout': 'PAIRED',
#      'Platform': 'ILLUMINA',
#      'Run': 'SRR11122776',
#      'asm_stats_contig_n50': 327498,
#      'asm_stats_length_bp': 4682040,
#      'asm_stats_n_contig': 35,
#      'attribute_package': 'Pathogen: environmental/food/other',
#      'bioproject_acc': 'PRJNA186035',
#      'bioproject_title': 'GenomeTrakr Project: US Food and Drug Administration',
#      'bioproject_center': 'Center for Food Safety and Applied Nutrition',
#      'biosample_acc': 'SAMN14145904',
#      'clustered': 1,
#      'collected_by': 'FDA',
#      'collection_date': '2018-07',
#      'epi_type': 'environmental/other',
#      'erd_group': 'PDS000026588.36',
#      'erd_group_acc': 'PDS000026588',
#      'geo_loc_name': 'USA:PA',
#      'isolation_source': 'environmental water',
#      'isolate_name_alias': 'CFSAN094939',
#      'kmer_group': 'PDG000000002.1750',
#      'kmer_group_acc': 'PDG000000002',
#      'major_contributor': 1, 'mindiff': 10,
#      'minsame': 0,
#      'new': 1,
#      'parent_group': 'PDS000026588.36',
#      'scientific_name': 'Salmonella enterica subsp. enterica',
#      'sort_order': 1,
#      'taxid': 59201, 'species_taxid': 28901,
#      'sra_center': 'CFSAN',
#      'sra_release_date': '2020-02-20T18:45:20Z',
#      'strain': 'CFSAN094939',
#      'sub_species': 'enterica',
#      'target_acc': 'PDT000688270.1',
#      'target_creation_date': '2020-02-20T19:10:39Z',
#      'creation_date': '2020-02-20T19:10:39Z',
#      'taxgroup_name': 'Salmonella enterica',
#      'taxgroup_name_short': 'Salmonella',
#      'number_drugs_resistant': 0,
#      'number_drugs_intermediate': 0,
#      'number_drugs_susceptible': 0,
#      'number_drugs_tested': 0,
#      'number_amr_genes': 0,
#      'number_stress_genes': 0,
#      'number_virulence_genes': 0,
#      'cluster_size': 74, 'amrfinderplus_applied': 0,
#      'updatedate': '2020-02-23T11:40:04.725Z',
#      'tree_node_ids': [79, 80, 81, 91, 92, 93, 95],
#      '_version_': 1659328922650673200}
#
# b = a.values()
#
# print(a.values())
#
#
# print("\t".join(a.values()))
