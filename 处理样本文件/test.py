import pandas as pd
import numpy as np

data = pd.read_csv("test_sample.txt", sep="\t", index_col=None, header=0)
data = data[["Species", "Assay type", "Cell type", "Sample"]]
# summary = data.value_counts(normalize=True)
# summary = data.apply(pd.value_counts)
# print(summary)


a = data.apply(pd.value_counts).fillna(0, downcast='infer').to_dict()
b = {key: {subKey: subValue for subKey, subValue in valueD.items() if subValue > 0} for key, valueD in a.items()}
print(b)
# DE_Group = data.groupby("Species")["Sample"].agg(value=list)
# dict_data = DE_Group.to_dict()
# print(dict_data)
# print(data)
# print(data.groupby(["Species"]).size())
# print(data.groupby(["Cell type"]).size())
# print(data.groupby(["Species"]).size().to_dict())

# df = pd.DataFrame({'A' : ['foo', 'bar', 'foo', 'bar','foo', 'bar', 'foo', 'foo'],
#                    'B' : ['one', 'one', 'two', 'three', 'two', 'two', 'one', 'three'],
#                    'C' : np.random.randn(8),
#                    'D' : np.random.randn(8)})
# print(df)
# print('------')
#
# print(df.groupby('A'), type(df.groupby('A')))
# print('------')
# # 直接分组得到一个groupby对象，是一个中间数据，没有进行计算
#
# a = df.groupby('A').mean()
# b = df.groupby(['A','B']).mean()
# c = df.groupby(['A'])['D'].mean()  # 以A分组，算D的平均值
# print("-----------------")
# print(a,type(a),'\n',a.columns)
# print()
# print(b,type(b),'\n',b.columns)
# print()
# print(c,type(c))
# # 通过分组后的计算，得到一个新的dataframe
# # 默认axis = 0，以行来分组
# # 可单个或多个（[]）列分组
