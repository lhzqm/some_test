import json
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

match_all = {
    "query": {
        "match_all": {}
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}
match = {
    "query": {
        "match": {
            "cid": "CMNPD1"
        }
    }
}

wildcard = {
    "query": {
        "wildcard": {
            "cid": "cmnpd1*"
        }
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}
search_text = "Chromista"
query_dsl = {
    "query": {
        "bool": {
            "should": [
                {"wildcard": {"oid": f"{search_text}*"}},
                {"wildcard": {"o_name": f"{search_text}*"}},
            ],
        }
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}
count_dsl = {
    "query": {
        "bool": {
            "should": [
                {"wildcard": {"oid": f"{search_text}*"}},
                {"wildcard": {"o_name": f"{search_text}*"}},
            ],
        }
    }
}

index = "0508_taxonomy"
result = es.search(index=index, body=query_dsl, filter_path=['hits.hits._score', "hits.hits._source"])
count = es.count(index=index, body=count_dsl)

print(json.dumps(result, indent=2, ensure_ascii=False))
print(count["count"])
# print(es.count(index="0508_compound_names"))
