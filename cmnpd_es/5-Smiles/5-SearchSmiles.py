import json
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

match_all = {
    "query": {
        "match_all": {}
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}
match = {
    "query": {
        "match": {
            "cid": "CMNPD1"
        }
    }
}

wildcard = {
    "query": {
        "wildcard": {
            "cid": "cmnpd1*"
        }
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}


def needs_escaping(character):
    """
    特殊字符字典 Special character dictionary
    :param character:
    :return:
    """
    escape_chars = {
        '\\': True, '+': True, '-': True, '!': True,
        '(': True, ')': True, ':': True, '^': True,
        '[': True, ']': True, '\"': True, '{': True,
        '}': True, '~': True, '*': True, '?': True,
        '|': True, '&': True, '/': True
    }
    return escape_chars.get(character, False)


def deal_with_search_text(search_text):
    """
    处理特殊字符 Handling special characters
    :param search_text:
    :return:
    """
    sanitized = ''
    for character in search_text:
        if needs_escaping(character):
            sanitized += '\\{}'.format(character)
        else:
            sanitized += character
    return sanitized


search_text = "C(/Cl)=C(/C(Cl)Cl)\C=C\[C@@H]([C@](Cl)(C=C)C)Cl"
search_text = deal_with_search_text(search_text)
print(search_text)
query_dsl = {
    "query": {
        "bool": {
            "should": [
                {"wildcard": {"cid": f"{search_text}*"}},
                {"wildcard": {"smiles": f"{search_text}*"}},
            ],
        }
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}
count_dsl = {
    "query": {
        "bool": {
            "should": [
                {"wildcard": {"cid": f"{search_text}*"}},
                {"wildcard": {"smiles": f"{search_text}*"}},
            ],
        }
    },
}

index = "0508_smiles"
result = es.search(index=index, body=query_dsl, filter_path=['hits.hits._score', "hits.hits._source"])
count = es.count(index=index, body=count_dsl)

print(json.dumps(result, indent=2, ensure_ascii=False))
print(count["count"])
# print(es.count(index="0508_compound_names"))
