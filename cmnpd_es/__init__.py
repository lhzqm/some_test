import json
import pandas as pd
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

index_list = [
    "docs",
    "new_cas",
    "new_compound_names",
    "compound_properties",
    "compound_names_cid",
    "chembl_targets",
    "taxonomy"
]

match_all = {
    "query": {
        "match_all": {}
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}

result = es.search(index="compound_properties", body=match_all, filter_path=['hits.hits._score', "hits.hits._source"])
# result = es.search(index=index_name, body=match, _source=['compound_names_cid', 'mol_name'])

print(json.dumps(result, indent=2, ensure_ascii=False))
