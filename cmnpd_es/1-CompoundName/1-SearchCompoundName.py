import json
from elasticsearch import Elasticsearch

es = Elasticsearch("47.92.159.134:18129")

match_all = {
    "query": {
        "match_all": {}
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}
count = {
    "query": {
        "wildcard": {
            "cid": "cmnpd1*"
        }
    }
}

wildcard = {
    "query": {
        "wildcard": {
            "cid": "cmnpd1*"
        }
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}
search_text = "Ircinin 3"
query_dsl = {
    "query": {
        "bool": {
            "should": [
                {"wildcard": {"cid": f"{search_text}*"}},
                {"wildcard": {"mol_name": f"{search_text}*"}},
            ],
        }
    },
    "sort": {
        "_id": {  # 根据age字段升序排序
            "order": "asc"  # asc升序，desc降序
        }
    }
}

match = {
    "query": {
        "match": {
            "cid": "CMNPD1"
        }
    }
}

result = es.search(index="0508_compound_names", body=query_dsl, filter_path=['hits.hits._score', "hits.hits._source"])
count = es.count(index="0508_compound_names", body=count)

print(json.dumps(result, indent=2, ensure_ascii=False))
# print(json.dumps(count, indent=2, ensure_ascii=False))
# print(es.count(index="0508_compound_names"))
